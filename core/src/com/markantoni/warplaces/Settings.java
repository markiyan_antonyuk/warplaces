package com.markantoni.warplaces;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 10.02.2016.
 */
public class Settings {
    public static final int DEFAULT_WIDTH = 800;
    public static final int DEFAULT_HEIGHT = 600;

    private static final Preferences preferences = Gdx.app.getPreferences(Strings.get("pref_name"));

    public static void init() {
        setFullscreen(isFullscreen());
    }

    public static boolean isDebug() {
        return preferences.getBoolean(Strings.get("pref_debug"), false);
    }

    public static void setDebug(boolean debug) {
        preferences.putBoolean(Strings.get("pref_debug"), debug);
        preferences.flush();
    }

    public static void setTouchmode(boolean touchmode) {
        preferences.putBoolean(Strings.get("pref_touchmode"), touchmode);
        preferences.flush();
    }

    public static boolean isInTouchmode() {
        return preferences.getBoolean(Strings.get("pref_touchmode"), !WarPlaces.isDesktop());
    }

    public static void setFullscreen(boolean fullscreen) {
        if (WarPlaces.isDesktop()) {
            preferences.putBoolean(Strings.get("pref_fullscreen"), fullscreen);
            preferences.flush();
            if (fullscreen) {
                Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
            } else {
                Gdx.graphics.setWindowedMode(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            }
        }
    }

    public static boolean isFullscreen() {
        return preferences.getBoolean(Strings.get("pref_fullscreen"), false);
    }
}
