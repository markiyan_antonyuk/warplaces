package com.markantoni.warplaces;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.VisUI;
import com.markantoni.warplaces.screens.Screen;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.screens.menu.MainMenuScreen;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Assets;

import java.io.File;

public class WarPlaces implements ApplicationListener {
    private static WarPlaces sInstance;
    private Rect mSize = Rect.empty();
    private Renderer mRenderer;

    @Override
    public void create() {
        sInstance = this;
        SizeUnit.init();
        VisUI.load(VisUI.SkinScale.X1);
        VisUI.setDefaultTitleAlign(Align.center);
        mSize.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mRenderer = new Renderer(new SpriteBatch(), new ShapeRenderer(), mSize);

        ScreenStack.open(MainMenuScreen.class);
        Settings.init();
    }

    @Override
    public void resize(int width, int height) {
        mSize.set(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mRenderer.resize(mSize);
        ScreenStack.resize(width, height);
    }

    @Override
    public void render() {
        ScreenStack.process();
        Screen screen = ScreenStack.getTop();

        if (screen != null) {
            screen.update(Gdx.graphics.getDeltaTime());

            mRenderer.begin();
            screen.render(mRenderer);
            mRenderer.end();
        }
    }

    @Override
    public void pause() {
        ScreenStack.pause();
    }

    @Override
    public void resume() {
        ScreenStack.resume();
    }

    @Override
    public void dispose() {
        mRenderer.dispose();
        VisUI.dispose();
        Assets.instance().dispose();
        ScreenStack.dispose();
    }

    public static File getGameDir() {
        File gameDir = new File(Gdx.files.getLocalStoragePath() + "WarPlaces" + File.separator);
        if (!gameDir.exists()) {
            gameDir.mkdirs();
        }
        return gameDir;
    }

    public static Rect getSize() {
        return sInstance.mSize;
    }

    public static boolean isDesktop() {
        return Gdx.app.getType() == Application.ApplicationType.Desktop;
    }

    public static void exitGame() {
        Gdx.app.exit();
    }
}
