package com.markantoni.warplaces.game;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.markantoni.warplaces.WarPlaces;
import com.markantoni.warplaces.game.camera.Camera;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.game.terrain.TerrainSize;
import com.markantoni.warplaces.screens.Screen;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.UiUtils;
import com.markantoni.warplaces.utils.Utils;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by mark on 30.12.15.
 */
public abstract class AbstractGameScreen extends Screen {
    public static final Rect DEFAULT_TILE_SIZE = new Rect(SizeUnit.x(1f), SizeUnit.y(1f));
    public static final String SIZE = "SIZE";
    public static final String FILE = "FILE";
    private Terrain mTerrain;
    private Camera mCamera;

    @Override
    protected void create() {
        initUI(getStage());
        addInputSource(mInputAdapter);
        addInputSource(new GestureDetector(mGestureAdapter));

        File file;
        if ((file = getBundle().get(FILE, null)) != null) {
            JSONObject json = Utils.readJson(file);
            mTerrain = new Terrain(json.optJSONObject(Terrain.JSON.TERRAIN));
        } else {
            mTerrain = new Terrain(getBundle().get(SIZE, TerrainSize.SMALL));
        }

        mCamera = new Camera(WarPlaces.getSize(), mTerrain.getRealSize());
        mTerrain.setCamera(mCamera);
    }

    @Override
    public void resume() {
        super.resume();
        Renderer.setCamera(mCamera);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        mCamera.updateViewport(width, height);
    }

    public final boolean isUiEvent(float x, float y) {
        for (Actor actor : getStage().getActors()) {
            if (isUiEvent(actor, x, y)) {
                mCamera.getCameraController().stop();
                return true;
            }
        }
        return false;
    }

    private boolean isUiEvent(Actor actor, float x, float y) {
        if (actor instanceof Table
                && actor.getWidth() == getStage().getWidth()
                && actor.getHeight() == getStage().getHeight()) {
            Table table = (Table) actor;
            for (Cell cell : table.getCells()) {
                if (isUiEvent(cell.getActor(), x, y)) {
                    return true;
                }
            }
        } else {
            if (UiUtils.contains(actor, x, y)) {
                return true;
            }
        }
        return false;
    }

    public Terrain getTerrain() {
        return mTerrain;
    }

    public Camera getCamera() {
        return mCamera;
    }

    protected boolean shouldTranslate(float x, float y) {
        return !isUiEvent(x, y);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        mTerrain.update(deltaTime);
        mCamera.update(deltaTime);
    }

    protected abstract void renderGame(Renderer renderer);

    @Override
    public void render(Renderer renderer) {
        mTerrain.render(renderer);
        renderGame(renderer);
        renderer.end();
        super.render(renderer);
    }

    @Override
    public void dispose() {
        super.dispose();
        mTerrain.dispose();
    }

    private final InputAdapter mInputAdapter = new InputAdapter() {

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return isUiEvent(screenX, Utils.flipY(screenY));
        }

        @Override
        public boolean scrolled(int amount) {
            mCamera.getCameraController().scrolled(amount);
            return false;
        }

        @Override
        public boolean mouseMoved(int x, int y) {
            y = Utils.flipY(y);
            if (shouldTranslate(x, y)) {
                mCamera.getCameraController().mouseMoved(x, y);
            } else {
                mCamera.getCameraController().stop();
            }
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            screenY = Utils.flipY(screenY);
            if (!shouldTranslate(screenX, screenY)) {
                mCamera.getCameraController().stop();
            }

            return isUiEvent(screenX, screenY);
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            mCamera.getCameraController().stop();
            return false;
        }
    };

    private final GestureDetector.GestureAdapter mGestureAdapter = new GestureDetector.GestureAdapter() {

        @Override
        public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
            float x1 = pointer1.x;
            float x2 = pointer2.x;
            float y1 = Utils.flipY(pointer1.y);
            float y2 = Utils.flipY(pointer2.y);

            mCamera.getCameraController().pinch(x1, y1, x2, y2, isUiEvent(x1, y1) || isUiEvent(x2, y2));
            return true;
        }
    };
}
