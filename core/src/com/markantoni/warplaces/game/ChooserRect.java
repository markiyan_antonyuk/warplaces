package com.markantoni.warplaces.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderable;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 27.01.16.
 */
public class ChooserRect implements Renderable {
    private final GestureDetector mGestureDetector;
    private final Rect mRect = Rect.empty();
    private final Rect mNormalizedRect = Rect.empty();
    private Callback mCallback;
    private boolean mIsActive;
    private boolean mCanStart = true;

    public ChooserRect() {
        mGestureDetector = new GestureDetector(mGestureAdapter);
    }

    public boolean isActive() {
        return mIsActive;
    }

    @Override
    public void render(Renderer renderer) {
        if (mIsActive) {
            renderer.renderColor(Color.WHITE, mRect);
        }
    }

    public InputProcessor getInputSource() {
        return mGestureDetector;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void chosen(Rect bounds);
    }

    private final GestureDetector.GestureAdapter mGestureAdapter = new GestureDetector.GestureAdapter() {
        @Override
        public boolean touchDown(float x, float y, int pointer, int button) {
            if (Utils.fingersTouched(2)) {
                mCanStart = false;
            }

            if (button == GameScreen.ACTION_BUTTON) {
                return panStop(x, y, pointer, button);
            }

            return super.touchDown(x, y, pointer, button);
        }

        @Override
        public boolean panStop(float x, float y, int pointer, int button) {
            mCanStart = true;
            if (!mIsActive) {
                return false;
            }

            if (mCallback != null) {
                mCallback.chosen(((Rect) mNormalizedRect.set(mRect)).normalize());
            }
            mRect.setEmpty();
            mIsActive = false;
            return true;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            if (Utils.fingersTouched(2) || !mCanStart) {
                mIsActive = false;
                return false;
            }

            if (!Gdx.input.isButtonPressed(GameScreen.SELECTION_BUTTON)) {
                return false;
            }

            mIsActive = true;
            Vector2 coordinates = Renderer.unproject(x, y);
            x = coordinates.x;
            y = coordinates.y;
            if (mRect.isEmpty()) {
                mRect.set(x, y);
            } else {
                mRect.setSize(x - mRect.x, y - mRect.y);
            }
            return true;
        }
    };
}
