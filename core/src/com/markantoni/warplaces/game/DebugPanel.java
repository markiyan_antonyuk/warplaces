package com.markantoni.warplaces.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.camera.Camera;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.utils.SizeUnit;

/**
 * Created by mark on 15.03.16.
 */
public class DebugPanel extends UiPanel {
    private VisLabel mFps;
    private VisLabel mMemory;
    private VisLabel mSize;
    private VisLabel mSizeUnit;
    private VisLabel mUiSizeUnit;
    private VisLabel mTouchMode;
    private VisLabel mCameraPosition;
    private VisLabel mCameraBounds;
    private VisLabel mCameraZoom;
    private VisLabel mGameObjectsCount;
    private VisLabel mNationsCount;
    private VisLabel mUnitsCount;
    private VisLabel mBuildingsCount;
    private VisLabel mItemsCount;
    private VisLabel mResourcesCount;

    public DebugPanel(GameScreen gameScreen) {
        super(gameScreen, DebugPanel.class.getName(), false, false, 0, false, false);
    }

    @Override
    protected Table create() {
        setTitle("DEBUG");
        Table table = new Table();
        table.defaults().prefSize(SizeUnit.uiX(1), SizeUnit.uiY(.5f)).pad(SizeUnit.uiUnit(.1f)).fillX();

        mFps = new VisLabel("", Align.right);
        mMemory = new VisLabel("", Align.right);
        mSize = new VisLabel("", Align.right);
        mSizeUnit = new VisLabel("", Align.right);
        mUiSizeUnit = new VisLabel("", Align.right);
        mTouchMode = new VisLabel("", Align.right);
        mCameraPosition = new VisLabel("", Align.right);
        mCameraBounds = new VisLabel("", Align.right);
        mCameraZoom = new VisLabel("", Align.right);
        mGameObjectsCount = new VisLabel("", Align.right);
        mNationsCount = new VisLabel("", Align.right);
        mUnitsCount = new VisLabel("", Align.right);
        mBuildingsCount = new VisLabel("", Align.right);
        mItemsCount = new VisLabel("", Align.right);
        mResourcesCount = new VisLabel("", Align.right);

        table.add(createLabel("FPS"), mFps);
        table.row();
        table.add(createLabel("Memory"), mMemory);
        table.row();
        table.add(createLabel("Size WxH"), mSize);
        table.row();
        table.add(createLabel("Size unit"), mSizeUnit);
        table.row();
        table.add(createLabel("Ui unit"), mUiSizeUnit);
        table.row();
        table.add(createLabel("Touch mode"), mTouchMode);
        table.row();
        table.add(createLabel("Camera position"), mCameraPosition);
        table.row();
        table.add(createLabel("Camera bounds"), mCameraBounds);
        table.row();
        table.add(createLabel("Camera zoom"), mCameraZoom);
        table.row();
        table.add(createLabel("Game Objects"), mGameObjectsCount);
        table.row();
        table.add(createLabel("Nations"), mNationsCount);
        table.row();
        table.add(createLabel("Units"), mUnitsCount);
        table.row();
        table.add(createLabel("Buildings"), mBuildingsCount);
        table.row();
        table.add(createLabel("Items"), mItemsCount);
        table.row();
        table.add(createLabel("Resources"), mResourcesCount);
        table.row();

        return table;
    }

    private VisLabel createLabel(String text) {
        return new VisLabel(text + ":", Align.left);
    }

    public void update(GameScreen gameScreen) {
        if (!Settings.isDebug()) {
            setVisible(false);
            return;
        }
        setVisible(true);
        Camera camera = gameScreen.getCamera();
        int units = 0;
        int buildings = 0;
        int items = gameScreen.getItemsManager().getAll().size();
        int resources = gameScreen.getResourcesManager().getAll().size();
        for (Nation nation : gameScreen.getAllNations()) {
            units += nation.getUnitManager().getAll().size();
            buildings += nation.getBuildingsManager().getAll().size();
        }

        mFps.setText(Gdx.graphics.getFramesPerSecond() + "");
        mMemory.setText(Gdx.app.getJavaHeap() / 1024 / 1024 + " mb");
        mSize.setText(Gdx.graphics.getWidth() + "x" + Gdx.graphics.getHeight());
        mSizeUnit.setText("x:" + SizeUnit.x(1) + " y:" + SizeUnit.y(1) + " unit:" + SizeUnit.unit(1));
        mUiSizeUnit.setText("x:" + SizeUnit.uiX(1) + " y:" + SizeUnit.uiY(1) + " unit:" + SizeUnit.uiUnit(1));
        mTouchMode.setText(Settings.isInTouchmode() + "");
        mCameraPosition.setText("x:" + (int) camera.getVisibleBounds().x + " y:" + (int) camera.getVisibleBounds().y);
        mCameraBounds.setText((int) camera.getVisibleBounds().width + "x" + (int) camera.getVisibleBounds().height);
        mCameraZoom.setText(camera.getZoom() + "");
        mGameObjectsCount.setText(units + buildings + items + resources + "");
        mNationsCount.setText(gameScreen.getAllNations().size() + "");
        mUnitsCount.setText(units + "");
        mBuildingsCount.setText(buildings + "");
        mItemsCount.setText(items + "");
        mResourcesCount.setText(resources + "");

        getUi().pack();
    }
}
