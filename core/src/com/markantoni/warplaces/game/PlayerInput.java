package com.markantoni.warplaces.game;

import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 10.02.16.
 */
public interface PlayerInput {
    boolean handleTap(float x, float y, int button);

    void handleLongPress();

    boolean handleRect(Rect rect);
}
