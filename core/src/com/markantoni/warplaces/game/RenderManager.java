package com.markantoni.warplaces.game;

import com.badlogic.gdx.math.Vector2;
import com.markantoni.warplaces.game.buildings.Building;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.utils.RenderableSelected;
import com.markantoni.warplaces.utils.Renderer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by mark on 04.02.16.
 */
public class RenderManager implements RenderableSelected {
    private final SortedMap<Vector2, GameObject> mMap = new TreeMap<Vector2, GameObject>(mComparator);
    private final List<GameObject> mDrawFirst = new ArrayList<GameObject>();

    public final void clear() {
        mMap.clear();
        mDrawFirst.clear();
    }

    public final void put(Building building) {
        put((GameObject) building);
    }

    public final void put(Unit unit) {
        put((GameObject) unit);
    }

    private void put(GameObject gameObject) {
        Vector2 isometric = gameObject.getIsometricPosition();
        if (isometric.isZero()) {
            mDrawFirst.add(gameObject);
        } else {
            mMap.put(isometric, gameObject);
        }
    }

    @Override
    public void render(Renderer renderer) {
        for (GameObject gameObject : mDrawFirst) {
            gameObject.render(renderer);
        }

        for (Vector2 vector2 : mMap.keySet()) {
            GameObject gameObject = mMap.get(vector2);
            gameObject.render(renderer);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        for (GameObject gameObject : mDrawFirst) {
            if (gameObject.isSelected() || gameObject.isMainSelected()) {
                gameObject.renderSelected(renderer);
            }
        }

        for (Vector2 vector2 : mMap.keySet()) {
            GameObject gameObject = mMap.get(vector2);
            if (gameObject.isSelected() || gameObject.isMainSelected()) {
                gameObject.renderSelected(renderer);
            }
        }
    }

    private static final Comparator<Vector2> mComparator = new Comparator<Vector2>() {
        @Override
        public int compare(Vector2 o1, Vector2 o2) {
            float result = o2.y - o1.y;
            if (result == 0) {
                result = o2.x - o1.x;
            }
            return (int) (result * 1000);
        }
    };
}
