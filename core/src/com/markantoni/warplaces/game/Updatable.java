package com.markantoni.warplaces.game;

import com.markantoni.warplaces.game.objects.GameObjectFinder;

public interface Updatable {
    void update(float deltaTime, RenderManager renderManager);
    void updateFinder(GameObjectFinder finder);
}