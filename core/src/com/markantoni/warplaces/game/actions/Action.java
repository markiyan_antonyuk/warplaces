package com.markantoni.warplaces.game.actions;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by mark on 29.02.16.
 */
public abstract class Action {
    private final boolean mIsDnD;

    public Action(boolean isDnD) {
        mIsDnD = isDnD;
    }

    public void perform(ActionsPerformer actionsPerformer) {
    }

    public boolean performDnd(ActionsPerformer actionsPerformer, float x, float y) {
        return false;
    }

    public boolean isDragAndDrop() {
        return mIsDnD;
    }

    public abstract Sprite getPreview();
}
