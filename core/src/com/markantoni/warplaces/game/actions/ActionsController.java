package com.markantoni.warplaces.game.actions;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.utils.Assets;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mark on 29.02.16.
 */
public class ActionsController {
    public static final int MAX_ACTIONS = 11;

    private final Map<Integer, Action> mActions = new HashMap<Integer, Action>();
    private ActionsPerformer mPerformer;

    public void setPerformer(ActionsPerformer performer) {
        mPerformer = performer;
        mActions.clear();
        for (int i = 0; i < MAX_ACTIONS; i++) {
            Action action = performer.getAction(i);
            if (action != null) {
                mActions.put(i, action);
            }
        }

        mActions.put(MAX_ACTIONS, mDestroyAction);
    }

    public ActionsPerformer getPerformer() {
        return mPerformer;
    }

    private final Action mDestroyAction = new Action(false) {
        @Override
        public void perform(ActionsPerformer actionsPerformer) {
            actionsPerformer.getHolder().destroy();
        }

        @Override
        public Sprite getPreview() {
            return Assets.Actions.DESTROY.SPRITE;
        }
    };

    public Map<Integer, Action> getActions() {
        return mActions;
    }
}
