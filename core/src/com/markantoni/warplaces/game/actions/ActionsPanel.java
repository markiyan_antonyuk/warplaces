package com.markantoni.warplaces.game.actions;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.layout.DragPane;
import com.kotcrab.vis.ui.widget.Draggable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.camera.Camera;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 29.02.16.
 */
public class ActionsPanel extends UiPanel {
    private static final int ROW_SIZE = 3;
    private static final int ROWS = 4;

    private final ActionsController mActionsController = new ActionsController();
    private List<VisTextButton> mButtons;
    private Table mTable;

    public ActionsPanel(GameScreen gameScreen) {
        super(gameScreen, ActionsPanel.class.getName(), false, true, 0, false, true);
    }

    @Override
    protected Table create() {
        setTitle(Strings.get("actions"));
        mTable = new Table();
        mButtons = new ArrayList<VisTextButton>();
        mTable.defaults().size(SizeUnit.uiX(1), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));

        for (int row = 0; row < ROWS; row++) {
            for (int column = 0; column < ROW_SIZE; column++) {
                VisTextButton button = new VisTextButton("");
                button.setFocusBorderEnabled(false);

                button.setVisible(false);
                mButtons.add(button);
                mTable.add(button);
            }
            mTable.row();
        }

        return mTable;
    }

    public void setActionsPerformer(ActionsPerformer actionsPerformer) {
        setVisible(true);
        mActionsController.setPerformer(actionsPerformer);
        rebuild();
    }

    private void rebuild() {
        //remove all previous click and d&d listeners from button
        List<EventListener> listenersToRemove = new ArrayList<EventListener>();
        for (VisTextButton button : mButtons) {
            listenersToRemove.clear();
            button.setVisible(false);

            for (EventListener listener : button.getListeners()) {
                if (listener instanceof ClickListener || listener instanceof Draggable) {
                    listenersToRemove.add(listener);
                }
            }

            for (EventListener listener : listenersToRemove) {
                button.removeListener(listener);
            }
        }

        Map<Integer, Action> actions = mActionsController.getActions();
        for (int index : actions.keySet()) {
            final Action action = actions.get(index);
            final VisTextButton button = mButtons.get(index);

            button.setVisible(true);
            updateButtonDrawable(button, action.getPreview());

            if (action.isDragAndDrop()) {
                Draggable draggable = new Draggable(new DragPane.DefaultDragListener() {
                    @Override
                    public boolean onEnd(Actor actor, float stageX, float stageY) {
                        AbstractGameScreen gameScreen = mActionsController.getPerformer().getHolder().getManager().getGameScreen();
                        Camera camera = gameScreen.getCamera();
                        Vector2 vector2 = camera.unproject(stageX, Utils.flipY(stageY));
                        if (gameScreen.isUiEvent(stageX, stageY)) {
                            return false;
                        }
                        return action.performDnd(mActionsController.getPerformer(), vector2.x, vector2.y);
                    }
                });
                draggable.setBlockInput(true);
                draggable.attachTo(button);
            } else {
                button.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        action.perform(mActionsController.getPerformer());
                        updateButtonDrawable(button, action.getPreview());
                    }
                });
            }
        }
    }
}
