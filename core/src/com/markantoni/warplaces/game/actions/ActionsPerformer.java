package com.markantoni.warplaces.game.actions;

import com.markantoni.warplaces.game.objects.GameObject;

/**
 * Created by mark on 29.02.16.
 */
public interface ActionsPerformer {
    GameObject getHolder();
    Action getAction(int actionIndex);
}
