package com.markantoni.warplaces.game.attributes;


/**
 * Created by mark on 19.02.16.
 */
public interface Attributes {
    String type();

    String health();

    String attack();

    String defence();

    String speed();

    String weight();

    String inventory();

    String storage();

    String capacity();
}
