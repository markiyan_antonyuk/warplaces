package com.markantoni.warplaces.game.attributes;

import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 18.02.16.
 */
public class GameObjectAttributes {
    private List<Property> mList = new ArrayList<Property>();

    public GameObjectAttributes() {
    }

    public void parse(Attributes attributes) {
        mList.clear();
        if (attributes.type() != null) {
            mList.add(new Property(Strings.get("type"), attributes.type()));
        }

        if (attributes.health() != null) {
            mList.add(new Property(Strings.get("health"), attributes.health()));
        }

        if (attributes.attack() != null) {
            mList.add(new Property(Strings.get("attack"), attributes.attack()));
        }

        if (attributes.defence() != null) {
            mList.add(new Property(Strings.get("defence"), attributes.defence()));
        }

        if (attributes.speed() != null) {
            mList.add(new Property(Strings.get("speed"), attributes.speed()));
        }

        if (attributes.weight() != null) {
            mList.add(new Property(Strings.get("weight"), attributes.weight()));
        }

        if (attributes.inventory() != null) {
            mList.add(new Property(Strings.get("inventory"), attributes.inventory()));
        }

        if (attributes.storage() != null) {
            mList.add(new Property(Strings.get("storage"), attributes.storage()));
        }

        if (attributes.capacity() != null) {
            mList.add(new Property(Strings.get("capacity"), attributes.capacity()));
        }
    }

    private void addAttribute(String key, Object value) {
        mList.add(new Property(key, value));
    }

    public List<Property> getAttributes() {
        return mList;
    }

    public static final class Property {
        private final String mKey;
        private final Object mValue;

        protected Property(String key, Object value) {
            mKey = key;
            mValue = value;
        }

        public String getKey() {
            return mKey;
        }

        public Object getValue() {
            return mValue;
        }
    }
}
