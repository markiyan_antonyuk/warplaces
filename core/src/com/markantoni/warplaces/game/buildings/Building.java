package com.markantoni.warplaces.game.buildings;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.resources.ResourceSet;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 03.02.16.
 */
public abstract class Building extends GameObject implements ActionsPerformer {
    private final Sprite mSprite;
    private final TextureRegion[] mConstructionTextures;
    private final String mName;
    private Construction mConstruction;

    public Building(Nation nation, String name, Sprite defaultSprite, TextureRegion[] costructionTextures, World world, float x, float y, float width, float height) {
        super(nation.getGameScreen(), nation);
        mName = name;
        setPhysicObject(new BuildingObject(world, x, y, generatePhysicBodyVertices(width, height)));
        mBounds.set(x, y, width, height);
        mSprite = defaultSprite;
        mConstructionTextures = costructionTextures;
    }

    void setConstruction(Construction construction) {
        mConstruction = construction;
    }

    public boolean isUnderConstruction() {
        return mConstruction != null;
    }

    public Construction getConstruction() {
        return mConstruction;
    }

    @Override
    public Color getColor() {
        return getBuildingsManager().getNation().getColor();
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public GameObjectManager getManager() {
        return getBuildingsManager();
    }

    protected abstract Vector2[] generatePhysicBodyVertices(float width, float height);

    public abstract ResourceSet getCost();

    @Override
    public void update(float deltaTime) {
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }

    @Override
    public Rect getBounds() {
        return mBounds;
    }

    @Override
    public void render(Renderer renderer) {
        if (renderer.needToRender(mBounds)) {
            renderer.renderSprite(mSprite, mBounds);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        if (renderer.needToRender(mBounds)) {
            renderer.renderEllipse(mIsMainSelected ? Color.GOLD : Color.RED, mBounds.x, mBounds.y, mBounds.width, mBounds.height);
        }
    }

    public TextureRegion[] getConstructionTextures() {
        return mConstructionTextures;
    }

    @Override
    public String type() {
        return Strings.get("building");
    }

    @Override
    public GameObject getHolder() {
        return this;
    }

    @Override
    public Action getAction(int actionIndex) {
        return null;
    }
}
