package com.markantoni.warplaces.game.buildings;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.objects.PhysicObject;
import com.markantoni.warplaces.utils.PhysicUtils;

/**
 * Created by mark on 03.02.16.
 */
public class BuildingObject extends PhysicObject {
    BuildingObject(World world, float x, float y, Vector2[] vertices) {
        if (vertices != null) {
            mBody = PhysicUtils.createStaticBody(world, x, y, vertices);
        }
    }
}
