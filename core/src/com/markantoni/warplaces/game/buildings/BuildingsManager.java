package com.markantoni.warplaces.game.buildings;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.Updatable;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.resources.objects.Resource;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.utils.Log;
import com.markantoni.warplaces.utils.Point;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 03.02.16.
 */
public class BuildingsManager extends GameObjectManager<Building> implements Updatable {
    private final World mWorld;

    public BuildingsManager(Nation nation, World world) {
        super(nation.getGameScreen(), nation);
        mWorld = world;
    }

    public Construction createConstruction(Class<? extends Building> type, float x, float y) {
        return createConstruction(type, (int) (x / AbstractGameScreen.DEFAULT_TILE_SIZE.width), (int) (y / AbstractGameScreen.DEFAULT_TILE_SIZE.height));
    }

    public Construction createConstruction(Class<? extends Building> type, int tileX, int tileY) {
        try {
            Building building = type
                    .getConstructor(Nation.class, World.class, float.class, float.class)
                    .newInstance(getNation(), mWorld, tileX * AbstractGameScreen.DEFAULT_TILE_SIZE.width, tileY * AbstractGameScreen.DEFAULT_TILE_SIZE.height);

            if (!canBePlaced(building) || !getNation().getResources().isEnough(building.getCost())) {
                building.freePhysicObject(mWorld);
                return null;
            }

            Construction construction = new Construction(getNation(), mWorld, building);
            add(construction);
            return construction;
        } catch (Exception e) {
            Log.d(BuildingsManager.class, "cant create construction of type: " + type + " " + e.getCause().toString());
        }
        return null;
    }

    public Building createBuilding(Class<? extends Building> type, float x, float y) {
        return createBuilding(type, (int) (x / AbstractGameScreen.DEFAULT_TILE_SIZE.width), (int) (y / AbstractGameScreen.DEFAULT_TILE_SIZE.height));
    }

    public Building createBuilding(Class<? extends Building> type, int tileX, int tileY) {
        try {
            Building building = type
                    .getConstructor(Nation.class, World.class, float.class, float.class)
                    .newInstance(getNation(), mWorld, tileX * AbstractGameScreen.DEFAULT_TILE_SIZE.width, tileY * AbstractGameScreen.DEFAULT_TILE_SIZE.height);

            if (!canBePlaced(building)) {
                building.freePhysicObject(mWorld);
                return null;
            }

            add(building);
            return building;
        } catch (Exception e) {
            Log.d(BuildingsManager.class, "cant create building " + type + " " + e.getCause().toString());
        }
        return null;
    }

    @Override
    public void add(Building building) {
        super.add(building);
    }

    private boolean canBePlaced(Building building) {
        Rect rect = building.getBounds();
        Terrain terrain = getGameScreen().getTerrain();

        int bottomCell = (int) (rect.y / AbstractGameScreen.DEFAULT_TILE_SIZE.height);
        int topCell = (int) (bottomCell + rect.height / AbstractGameScreen.DEFAULT_TILE_SIZE.height);
        int leftCell = (int) (rect.x / AbstractGameScreen.DEFAULT_TILE_SIZE.width);
        int rightCell = (int) (leftCell + rect.width / AbstractGameScreen.DEFAULT_TILE_SIZE.width);

        for (Terrain.Tile tile : terrain.getTileMap().keySet()) {
            if (tile.isPassable()) {
                continue;
            }

            for (int y = bottomCell; y < topCell; y++) {
                for (int x = leftCell; x < rightCell; x++) {
                    if (terrain.getTileMap().get(tile).containsKey(new Point(x, y))) {
                        return false;
                    }
                }
            }
        }

        for (Resource resource : getGameScreen().getResourcesManager().getAll()) {
            if (resource.getBounds().overlaps(rect)) {
                return false;
            }
        }

        for (Nation nation : getGameScreen().getAllNations()) {
            for (Building b : nation.getBuildingsManager().getAll()) {
                if (b.getBounds().overlaps(rect)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        super.update(deltaTime, renderManager);
        for (int i = mAll.size() - 1; i >= 0; i--){
            Building building = mAll.get(i);
            renderManager.put(building);
            building.update(deltaTime);
        }
    }

    @Override
    protected boolean handleAction(float x, float y) {
        return false;
    }
}
