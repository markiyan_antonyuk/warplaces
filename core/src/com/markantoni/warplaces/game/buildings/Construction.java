package com.markantoni.warplaces.game.buildings;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.PhysicObject;
import com.markantoni.warplaces.game.resources.ResourceSet;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 01.03.2016.
 */
public class Construction extends Building {
    private final Building mBuilding;
    private final ResourceSet mResources = new ResourceSet();
    private final ResourceSet mNeededResources = new ResourceSet();

    public Construction(Nation nation, World world, Building building) {
        super(nation, Strings.get("construction"), Assets.Buildings.CONSTRUCTION.SPRITE, null, world,
                building.getBounds().x, building.getBounds().y, building.getBounds().width, building.getBounds().height);
        mBuilding = building;
        mBuilding.setConstruction(this);
        mNeededResources.add(building.getCost());
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (isFinished()) {
            mBuilding.setConstruction(null);
            destroy();
            getBuildingsManager().add(mBuilding);
        }
    }

    @Override
    public void render(Renderer renderer) {
        super.render(renderer);
        int index = MathUtils.clamp((int) ((float) getConstructedPercent() / 100 * mBuilding.getConstructionTextures().length),
                0, mBuilding.getConstructionTextures().length - 1);
        renderer.renderTextureRegion(mBuilding.getConstructionTextures()[index], mBounds);
    }

    @Override
    public void destroy() {
        super.destroy();
        if (mBuilding.getConstruction() != null) {
            mBuilding.destroy();
        }
    }

    public boolean isFinished() {
        return getConstructedPercent() >= 100;
    }

    @Override
    public Vector2 getIsometricPosition() {
        return Vector2.Zero;
    }

    public Building getBuilding() {
        return mBuilding;
    }

    @Override
    public ResourceSet getCost() {
        return null;
    }

    @Override
    protected void setPhysicObject(PhysicObject physicObject) {
        //don't set any, building has set
    }

    @Override
    protected Vector2[] generatePhysicBodyVertices(float width, float height) {
        return null; //stub, must be empty, building has all
    }

    @Override
    public void freePhysicObject(World world) {
        //stub
    }

    public ResourceSet getNeededResources() {
        return mNeededResources;
    }

    public boolean addResource(Item resource) {
        mNeededResources.remove(resource.getClass());
        mResources.add(resource.getClass());
        return mNeededResources.get(resource.getClass()) > 0;
    }

    private int getConstructedPercent() {
        return mResources.containsPercents(mBuilding.getCost());
    }

    @Override
    public String capacity() {
        return getConstructedPercent() + "";
    }
}
