package com.markantoni.warplaces.game.buildings;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.resources.StoneItem;
import com.markantoni.warplaces.game.items.resources.WoodItem;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.resources.ResourceSet;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Pair;
import com.markantoni.warplaces.utils.PhysicUtils;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 03.02.16.
 */
public class Warehouse extends StorageBuilding {
    private static final int STORAGE_SIZE = 12;
    private static final ResourceSet sCost = new ResourceSet(
            new Pair<Class<? extends Item>, Integer>(WoodItem.class, 15),
            new Pair<Class<? extends Item>, Integer>(StoneItem.class, 5));

    public Warehouse(Nation nation, World world, float x, float y) {
        super(nation, Strings.get("warehouse"), Assets.Buildings.WAREHOUSE.SPRITE, Assets.Buildings.WAREHOUSE_CONSTRUCTION.TEXTURE_REGION[0],
                world, x, y, AbstractGameScreen.DEFAULT_TILE_SIZE.width, AbstractGameScreen.DEFAULT_TILE_SIZE.height, STORAGE_SIZE);
        mPosition.add(x, y);
    }

    @Override
    public Vector2 getIsometricPosition() {
        return mPosition;
    }

    @Override
    protected Vector2[] generatePhysicBodyVertices(float width, float height) {
        Vector2[] vector2 = PhysicUtils.rectWithoutEdges(width / 5 * 3, height / 5 * 3, 4, width / 5, height / 5);
        mPosition.set(vector2[0]);
        return vector2;
    }

    @Override
    public ResourceSet getCost() {
        return sCost;
    }
}
