package com.markantoni.warplaces.game.buildings.storage;

import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.items.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 15.02.16.
 */
public class Storage extends Container<Integer> {
    private static final int MAX_CAPACITY_PER_SLOT = 99;

    private final Map<Integer, List<Item>> mContainer = new HashMap<Integer, List<Item>>();
    private final int mSize;
    private final StorageBuilding mStorageBuilding;

    public Storage(StorageBuilding holder, int size) {
        mStorageBuilding = holder;
        mSize = size;
    }

    public boolean isFull() {
        return mContainer.size() >= mSize;
    }

    public boolean isEmpty() {
        return mContainer.size() == 0;
    }

    public boolean contains(Class<? extends Item> item) {
        return getFirst(item) != null;
    }

    public int getSize() {
        return mSize;
    }

    public int getCurrentSize() {
        return mContainer.size();
    }

    @Override
    public Item getFirst(Class<? extends Item> type) {
        for (int i = 0; i < mSize; i++) {
            Item item = getItem(i);
            if (item != null && item.getClass().equals(type)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public boolean addItem(Item item, Integer slot) {
        if (slot >= mSize) {
            return false;
        }

        List<Item> items = mContainer.get(slot);
        if (items == null) {
            mContainer.put(slot, new ArrayList<Item>(Arrays.asList(item)));
            notifyChanged(item, null, slot);
            notifyObserver();
            return true;
        }

        if (item.isStackable() && items.get(0).getClass() == item.getClass() &&
                items.size() + 1 <= MAX_CAPACITY_PER_SLOT) {
            items.add(item);
            notifyChanged(item, null, slot);
            notifyObserver();
            return true;
        }

        return false;
    }

    @Override
    public boolean addItem(Item item) {
        for (int i = 0; i < mSize; i++) {
            if (addItem(item, i)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean canBeAdded(Item item) {
        for (int i = 0; i < mSize; i++) {
            List<Item> items = mContainer.get(i);
            if (items == null) {
                return true;
            }

            if (item.isStackable()
                    && items.get(0).getClass() == item.getClass()
                    && items.size() + 1 <= MAX_CAPACITY_PER_SLOT) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Item dropItem(Integer slot) {
        List<Item> items = mContainer.get(slot);
        if (items != null) {
            Item item = items.remove(0);
            item.setContainer(null);

            if (items.isEmpty()) {
                mContainer.remove(slot);
            }

            notifyObserver();
            notifyChanged(item, slot, null);
            mStorageBuilding.dropItem(item);

            return item;
        }
        return null;
    }

    @Override
    public void dropAll() {
        for (int i = 0; i < getSize(); i++) {
            while (dropItem(i) != null) ;
        }
    }

    @Override
    public boolean canRemoveItem(Item item) {
        return mContainer.get(getSlot(item)) != null;
    }

    @Override
    public boolean removeItem(Item item) {
        Integer slot = getSlot(item);
        List<Item> items = mContainer.get(slot);

        if (items != null) {
            items.remove(item);
            item.setContainer(null);

            if (items.isEmpty()) {
                mContainer.remove(slot);
                notifyObserver();
                return true;
            }

            notifyObserver();
            return false;
        }
        return true;
    }

    @Override
    public Item getItem(Integer slot) {
        List<Item> items = mContainer.get(slot);
        if (items != null) {
            return items.get(0);
        }
        return null;
    }

    public List<Item> getItems(Integer slot) {
        return mContainer.get(slot);
    }

    @Override
    public Integer getSlot(Item item) {
        for (Map.Entry<Integer, List<Item>> entry : mContainer.entrySet()) {
            for (Item i : entry.getValue()) {
                if (i == item) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    @Override
    public boolean canBePlacedInSlot(Item item, Integer slot) {
        return true;
    }

    public void switchSlots(Integer slot1, Integer slot2) {
        if (mergeSlots(slot1, slot2)) {
            return;
        }

        List<Item> item1 = mContainer.remove(slot1);
        List<Item> item2 = mContainer.remove(slot2);

        if (item2 != null) {
            mContainer.put(slot1, item2);
            notifyChanged(null/*item2*/, slot2, slot1);
        }

        if (item1 != null) {
            mContainer.put(slot2, item1);
            notifyChanged(null/*item1*/, slot1, slot2);
        }

        notifyObserver();
    }

    private boolean mergeSlots(Integer slot1, Integer slot2) {
        Item item1 = getItem(slot1);
        Item item2 = getItem(slot2);

        if (item1 == null || item2 == null) {
            return false;
        }

        if (!item1.getClass().equals(item2.getClass())) {
            return false;
        }

        List<Item> items = getItems(slot1);
        List<Item> copy = new ArrayList<Item>(items);

        for (Item item : copy) {
            if (addItem(item, slot2)) {
                items.remove(item);
            } else {
                break;
            }
        }

        if (items.isEmpty()) {
            mContainer.remove(slot1);
        }

        notifyChanged(null, slot2, slot1);
        notifyChanged(null, slot1, slot2);
        notifyObserver();

        return true;
    }
}
