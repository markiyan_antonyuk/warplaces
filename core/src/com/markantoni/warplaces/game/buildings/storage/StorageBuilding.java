package com.markantoni.warplaces.game.buildings.storage;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.buildings.Building;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 15.02.16.
 */
public abstract class StorageBuilding extends Building {
    private final Storage mStorage;

    public StorageBuilding(Nation nation, String name, Sprite defaultSprite, TextureRegion[] constructionTextures, World world, float x, float y, float width, float height, int containerSize) {
        super(nation, name, defaultSprite, constructionTextures, world, x, y, width, height);
        mStorage = new Storage(this, containerSize);
    }

    @Override
    public void freePhysicObject(World world) {
        mStorage.dropAll();
        super.freePhysicObject(world);
    }

    public Storage getStorage() {
        return mStorage;
    }

    public void dropItem(Item item) {
        getItemsManager().createItem(item, Utils.randomRange(getBounds().x, getBounds().width),
                Utils.randomRange(getBounds().y, getBounds().height / 4));
    }

    @Override
    public String type() {
        return Strings.get("storage");
    }

    @Override
    public String storage() {
        return mStorage.getCurrentSize() + "/" + mStorage.getSize();
    }
}
