package com.markantoni.warplaces.game.buildings.storage;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.Draggable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.container.ContainerPanel;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.inventory.Inventory;
import com.markantoni.warplaces.game.units.inventory.InventorySlot;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 15.02.16.
 */
public class StoragePanel extends ContainerPanel<Storage, StorageBuilding> {
    private static final int ROW_SIZE = 4;
    private static final int WIDTH = SizeUnit.uiX(1);
    private static final int HEIGHT = SizeUnit.uiY(1);
    private static final int PADDING = SizeUnit.uiUnit(.1f);

    private Table mTable;
    private List<VisTextButton> mButtons;

    public StoragePanel(GameScreen gameScreen) {
        super(gameScreen, StoragePanel.class.getName(), 1, false, false);
    }

    @Override
    protected Table createTable() {
        setTitle(Strings.get("storage"));
        getUi().setPosition(SizeUnit.max(), 0);
        mButtons = new ArrayList<VisTextButton>();

        mTable = new Table();
        mTable.defaults().size(WIDTH, HEIGHT).pad(PADDING);
        initButtons(ROW_SIZE);

        return mTable;
    }

    @Override
    public void showContainer(StorageBuilding holder) {
        setVisible(true);
        setContainer((holder).getStorage());
    }

    @Override
    protected void handleContainer(Storage container) {
        if (container == null) {
            return;
        }

        if (mButtons.size() != container.getSize()) {
            initButtons(container.getSize());
        }

        for (int i = 0; i < container.getSize(); i++) {
            List<Item> items = container.getItems(i);
            if (items != null) {
                updateButtonDrawable(mButtons.get(i), items.get(0).getPreviewSprite());
                mButtons.get(i).setText(items.size() > 1 ? items.size() + "" : "");
            } else {
                updateButtonDrawable(mButtons.get(i), null);
                mButtons.get(i).setText("");
            }
        }
    }

    private void initButtons(int size) {
        mTable.clearChildren();
        mButtons.clear();

        for (int i = 0; i < size; i++) {
            if (i > 0 && i % ROW_SIZE == 0) {
                mTable.row();
            }

            VisTextButton button = new VisTextButton("");
            button.setUserObject(i);
            button.setFocusBorderEnabled(false);
            Draggable draggable = new Draggable(this);
            draggable.attachTo(button);
            draggable.setBlockInput(true);
            button.getLabel().setFontScale(2);

            mTable.add(button);
            mButtons.add(button);
        }
    }

    @Override
    protected boolean drag(Object object) {
        return mContainer.getItem((Integer) object) != null;
    }


    @Override
    protected boolean drop(Object srcSlot, Object destSlot, Container container) {
        if (container instanceof Inventory) {
            Inventory inventory = (Inventory) container;
            Item src = inventory.getItem((InventorySlot) srcSlot);
            Item dst = mContainer.getItem((Integer) destSlot);

            //remove item from inventory
            inventory.removeItem(src);

            if (dst == null) { //if slot in storage is empty
                //add item from inventory into slot
                if (!mContainer.addItem(src, (Integer) destSlot)) {
                    //or if it's taken, add to first free slot
                    if (!mContainer.addItem(src)) {
                        //if there is no free slots, return item back to inventory
                        inventory.addItem(src, (InventorySlot) srcSlot);
                        return false;
                    }
                }
                return true;
            } else if (inventory.canBePlacedInSlot(dst, (InventorySlot) srcSlot)) { //check if can place item from storage to inventory
                //check if can merge item into storage slot
                if (mContainer.addItem(src, (Integer) destSlot)) {
                    return true;
                }

                //remove item from container
                mContainer.removeItem(dst);
                //check if can add item to container
                if (mContainer.addItem(src, (Integer) destSlot) || mContainer.addItem(src)) {
                    //and add it to inventory
                    inventory.addItem(dst, (InventorySlot) srcSlot);
                    return true;
                } else {
                    //return back item to container
                    mContainer.addItem(dst, (Integer) destSlot);
                    //if not, return back item to inventory
                    inventory.addItem(src, (InventorySlot) srcSlot);
                    return false;
                }
            } else {
                //return back item to inventory
                inventory.addItem(src, (InventorySlot) srcSlot);
            }

        } else {
            mContainer.switchSlots((Integer) srcSlot, (Integer) destSlot);
            return true;
        }
        return false;
    }
}
