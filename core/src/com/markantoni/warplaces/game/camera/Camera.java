package com.markantoni.warplaces.game.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 01.02.16.
 */
public class Camera {
    private final OrthographicCamera mCamera;
    private final Vector2 mTempVector2 = new Vector2();
    private final Vector3 mTempVector3 = new Vector3();
    private final Rect mTempRect = Rect.empty();
    private final CameraController mCameraController;

    public Camera(Rect bounds, Rect gameSize) {
        mCameraController = new CameraController(gameSize);
        mCamera = new OrthographicCamera(bounds.width, bounds.height);
        reset();
    }

    public void setTranslation(float x, float y) {
        mCamera.position.set(x, y, 0);
        mCameraController.mTranslationController.normalizeTranslation(mCamera.position, getVisibleBounds());
    }

    public void update(float deltaTime) {
        mCameraController.update(mCamera, deltaTime, getVisibleBounds());
    }

    public CameraController getCameraController() {
        return mCameraController;
    }

    public boolean isPerformingAction() {
        return mCameraController.isPerformingAction();
    }

    public Rect getVisibleBounds() {
        float zoom = mCamera.zoom;
        mTempRect.setSize(mCamera.viewportWidth * zoom, mCamera.viewportHeight * zoom);
        mTempRect.set(mCamera.position.x - mTempRect.width / 2, mCamera.position.y - mTempRect.height / 2);
        return mTempRect.cpy();
    }

    public Vector2 unproject(float x, float y) {
        return unproject(mTempVector2.set(x, y));
    }

    public Vector2 unproject(Vector2 vector2) {
        mTempVector3.set(vector2, 0);
        mCamera.unproject(mTempVector3);
        return mTempVector2.set(mTempVector3.x, mTempVector3.y).cpy();
    }

    public void setMargins(float top, float bottom) {
        mCameraController.mTranslationController.setMargins(top * mCamera.zoom, bottom * mCamera.zoom);
    }

    public void reset() {
        mCameraController.reset(mCamera);
    }

    public void updateViewport(float width, float height) {
        mCamera.translate((int) (mCamera.viewportWidth - width), (int) (mCamera.viewportHeight - height));
        mCamera.viewportWidth = width;
        mCamera.viewportHeight = height;
        mCamera.update();
    }

    public Matrix4 matrix() {
        return mCamera.combined;
    }

    public float getZoom() {
        return mCamera.zoom;
    }

}
