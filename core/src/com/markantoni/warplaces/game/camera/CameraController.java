package com.markantoni.warplaces.game.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.SizeUnit;

/**
 * Created by mark on 02.02.16.
 */
public class CameraController {
    private enum State {
        NONE, TRANSLATE, ZOOM
    }

    private final float START_ZOOM_DISTANCE = SizeUnit.uiUnit(.5f);
    private final int CHECKS_BEFORE_CHOICE = 10;

    private final Vector2 mTouchPinch1 = new Vector2();
    private final Vector2 mTouchPinch2 = new Vector2();
    private int mCheckCount = 0;
    private float mInitialPinchDistance;
    private float mLastPinchDistance;
    private State mState;
    final ZoomController mZoomController;
    final TranslationController mTranslationController;

    CameraController(Rect realSize) {
        mZoomController = new ZoomController();
        mTranslationController = new TranslationController(realSize);
    }

    boolean isPerformingAction() {
        return mZoomController.isActive() || mTranslationController.isActive();
    }

    void update(OrthographicCamera camera, float deltaTime, Rect visibleBounds) {
        camera.zoom = mZoomController.getNextZoom(camera.zoom, deltaTime);
        camera.translate(mTranslationController.getNewTranslation(deltaTime).scl(camera.zoom));
        mTranslationController.normalizeTranslation(camera.position, visibleBounds);
        camera.update();
    }

    void reset(OrthographicCamera camera) {
        mZoomController.reset();
        mTranslationController.reset();
        camera.zoom = 1;
        camera.setToOrtho(false, camera.viewportWidth, camera.viewportHeight);
        camera.update();
        stop();
    }

    public void scrolled(int amount) {
        if (Settings.isInTouchmode()) {
            return;
        }

        if (amount > 0) {
            mZoomController.zoomOut();
        } else {
            mZoomController.zoomIn();
        }
    }

    public void stop() {
        mZoomController.stopZoom();
        mTranslationController.stopTranslation();
        mState = State.NONE;
        mLastPinchDistance = mInitialPinchDistance = 0;
        mCheckCount = 0;
        mTouchPinch1.setZero();
        mTouchPinch2.setZero();
    }

    public void pinch(float x1, float y1, float x2, float y2, boolean isUiEvent) {
        if (!Settings.isInTouchmode() || isUiEvent) {
            return;
        }
        float distance = Vector2.dst(x1, y1, x2, y2);

        //check if it is first pinch frame
        if (mInitialPinchDistance == 0) {
            mTouchPinch1.set(x1, y1);
            mTouchPinch2.set(x2, y2);
            mInitialPinchDistance = distance;
            mLastPinchDistance = distance;
        }

        //made few pinch checks to decide what to choose - translation or zoom
        if (mCheckCount < CHECKS_BEFORE_CHOICE) {
            mCheckCount++;
            return;
        }

        //if checks count is enough, setup state
        if (mState == State.NONE) {
            //state is setting based on first pinch and current pinch, whether it on the same direction
            mState = new Vector2(x1, y1).sub(mTouchPinch1).hasSameDirection(new Vector2(x2, y2).sub(mTouchPinch2)) ? State.TRANSLATE : State.ZOOM;
            return;
        }

        if (mState == State.TRANSLATE) {
            mTranslationController.translate(x1, y1, x2, y2);
        }

        if (mState == State.ZOOM && Math.abs(mLastPinchDistance - mInitialPinchDistance) > START_ZOOM_DISTANCE) {
            mZoomController.performZoom(distance);
            mTranslationController.stopTranslation();
        }

        mLastPinchDistance = distance;
    }

    public void mouseMoved(int x, int y) {
        if (!Settings.isInTouchmode()) {
            mTranslationController.translate(x, y);
        }
    }
}
