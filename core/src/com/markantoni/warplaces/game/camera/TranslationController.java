package com.markantoni.warplaces.game.camera;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.WarPlaces;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.SizeUnit;

/**
 * Created by mark on 02.02.16.
 */
class TranslationController {
    private final float TRANSLATION_SPEED = Settings.isInTouchmode() ? SizeUnit.unit(3) : SizeUnit.unit(10);
    private final float TRANSLATION_DELTA = SizeUnit.uiUnit(.5f);

    private final Rect mRealSize;
    private final Vector2 mTranslationVector = new Vector2();
    private final Vector2 mLastTouch1 = new Vector2();
    private final Vector2 mLastTouch2 = new Vector2();
    private final Vector2 mTempVector = new Vector2();
    private boolean mIsActive;
    private float mTopMargin, mCurrentTopMargin;
    private float mBottomMargin, mCurrentBottomMargin;

    TranslationController(Rect realSize) {
        stopTranslation();
        mRealSize = realSize;
    }

    Vector3 normalizeTranslation(Vector3 translation, Rect visibleBounds) {
        translation.x = MathUtils.clamp(translation.x, visibleBounds.width / 2, mRealSize.width - visibleBounds.width / 2);
        translation.y = MathUtils.clamp(translation.y, visibleBounds.height / 2 - mCurrentBottomMargin, mRealSize.height - visibleBounds.height / 2 + mCurrentTopMargin);
        return translation;
    }

    void setMargins(float top, float bottom) {
        if (bottom > mBottomMargin) {
            mBottomMargin = mCurrentBottomMargin = bottom;
        } else {
            mBottomMargin = bottom;
        }

        if (top > mTopMargin) {
            mTopMargin = mCurrentTopMargin = top;
        } else {
            mTopMargin = top;
        }
    }

    void reset() {
        mTopMargin = mBottomMargin = mCurrentTopMargin = mCurrentBottomMargin = 0;
    }

    boolean isActive() {
        return mIsActive;
    }

    Vector2 getNewTranslation(float deltaTime) {
        if (mCurrentBottomMargin > mBottomMargin) {
            mCurrentBottomMargin = Math.max(mCurrentBottomMargin - deltaTime * TRANSLATION_SPEED, 0);
        }
        if (mCurrentTopMargin > mTopMargin) {
            mCurrentTopMargin = Math.max(mCurrentTopMargin - deltaTime * TRANSLATION_SPEED, 0);
        }
        return mTempVector.set(mTranslationVector.x * deltaTime, mTranslationVector.y * deltaTime);
    }

    void stopTranslation() {
        mTranslationVector.setZero();
        mLastTouch1.setZero();
        mLastTouch2.setZero();
        mIsActive = false;
    }

    void translate(float x1, float y1, float x2, float y2) {
        mIsActive = true;

        if (mLastTouch1.isZero() || mLastTouch2.isZero()) {
            mLastTouch1.set(x1, y1);
            mLastTouch2.set(x2, y2);
        }

        float newX = ((x1 - mLastTouch1.x) + (x2 - mLastTouch2.x)) / 2 * TRANSLATION_SPEED;
        float newY = ((y1 - mLastTouch1.y) + (y2 - mLastTouch2.y)) / 2 * TRANSLATION_SPEED;

        if (Math.abs(newX) > TRANSLATION_SPEED / 2) {
            mTranslationVector.x = -newX;
        } else {
            mTranslationVector.x = 0;
        }

        if (Math.abs(newY) > TRANSLATION_SPEED / 2) {
            mTranslationVector.y = -newY;
        } else {
            mTranslationVector.y = 0;
        }

        mLastTouch1.set(x1, y1);
        mLastTouch2.set(x2, y2);
    }

    void translate(int x, int y) {
        boolean result = false;

        if (x < TRANSLATION_DELTA) {
            mTranslationVector.x = -TRANSLATION_SPEED;
            result = true;
        } else if (x > WarPlaces.getSize().width - TRANSLATION_DELTA) {
            mTranslationVector.x = TRANSLATION_SPEED;
            result = true;
        } else {
            mTranslationVector.x = 0;
        }

        if (y < TRANSLATION_DELTA) {
            mTranslationVector.y = -TRANSLATION_SPEED;
            result = true;
        } else if (y > WarPlaces.getSize().height - TRANSLATION_DELTA) {
            mTranslationVector.y = TRANSLATION_SPEED;
            result = true;
        } else {
            mTranslationVector.y = 0;
        }

        mIsActive = result;
    }
}
