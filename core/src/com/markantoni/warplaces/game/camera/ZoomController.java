package com.markantoni.warplaces.game.camera;

import com.markantoni.warplaces.utils.SizeUnit;

/**
 * Created by mark on 02.02.16.
 */
class ZoomController {
    //// TODO: 01.02.16 smooth zoom based on coordinates
    private final float MIN_ZOOM = .25f;
    private final float MAX_ZOOM = 2f;
    private final float MIN_ZOOM_DISTANCE = SizeUnit.unit(0.25f);
    private final float ZOOM_DELTA = 1f;

    private float mZoomDirection;
    private float mLastZoomDistance;
    private float mZoomAmount;
    private boolean mIsActive;

    boolean isActive() {
        return mIsActive;
    }

    void reset() {
        mZoomDirection = 0;
    }

    float getNextZoom(float currentZoom, float deltaTime) {
        if (mZoomDirection == 0) {
            mIsActive = false;
            return currentZoom;
        }

        if (mZoomAmount == 0 || (mZoomAmount < ZOOM_DELTA && mZoomAmount > -ZOOM_DELTA)) {
            mZoomAmount = 0;
            mIsActive = false;
            return currentZoom;
        }

        float multiplier = ZOOM_DELTA * mZoomDirection;
        float deltaZoom = multiplier * deltaTime;
        float nextZoom = currentZoom + deltaZoom;
        mZoomAmount -= multiplier;


        if ((nextZoom > MAX_ZOOM && mZoomDirection > 0) || (nextZoom < MIN_ZOOM && mZoomDirection < 0)) {
            mIsActive = false;
            return currentZoom;
        }

        mIsActive = true;
        return nextZoom;
    }

    private void zoom(float amount) {
        mZoomAmount += amount;
        mZoomDirection = amount;
    }

    void zoomIn() {
        zoom(-ZOOM_DELTA);
    }

    void zoomOut() {
        zoom(ZOOM_DELTA);
    }

    void stopZoom() {
        mLastZoomDistance = 0;
        mZoomDirection = 0;
    }

    void performZoom(float distance) {
        if (mLastZoomDistance == 0) {
            mLastZoomDistance = distance;
            return;
        }

        if (Math.abs(distance - mLastZoomDistance) < MIN_ZOOM_DISTANCE) {
            return;
        }

        if (distance > mLastZoomDistance) {
            zoomIn();
        } else {
            zoomOut();
        }

        mLastZoomDistance = distance;
    }

}
