package com.markantoni.warplaces.game.container;

import com.markantoni.warplaces.game.items.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 15.02.16.
 */
public abstract class Container<SlotType> {
    private final List<Observer> mObservers = new ArrayList<Observer>();
    private ChangedCallback mChangedCallback;

    public Container() {
    }

    /**
     * adds item to first free slot in container
     */
    public abstract Item getFirst(Class<? extends Item> type);

    public abstract boolean addItem(Item item);

    public abstract boolean canBePlacedInSlot(Item item, SlotType slot);

    public abstract boolean canBeAdded(Item item);

    public abstract boolean addItem(Item item, SlotType slot);

    public abstract Item dropItem(SlotType slot);

    public abstract void dropAll();

    public abstract boolean canRemoveItem(Item item);

    /**
     * @return true if slot is empty
     */
    public abstract boolean removeItem(Item item);

    public abstract Item getItem(SlotType slot);

    public abstract SlotType getSlot(Item item);

    public void setChangedCallback(ChangedCallback<SlotType> callback) {
        mChangedCallback = callback;
    }

    public void addObserver(Observer observer) {
        mObservers.add(observer);
    }

    public void removeObserver(Observer observer) {
        mObservers.remove(observer);
    }

    protected void notifyChanged(Item item, SlotType oldSlot, SlotType newSlot) {
        if (mChangedCallback != null) {
            mChangedCallback.changed(item, oldSlot, newSlot);
        }
    }

    protected void notifyObserver() {
        for (Observer observer : mObservers) {
            observer.changed(this);
        }
    }

    public interface Observer {
        void changed(Container container);
    }

    public interface ChangedCallback<SlotType> {
        void changed(Item item, SlotType oldSlot, SlotType newSlot);
    }
}
