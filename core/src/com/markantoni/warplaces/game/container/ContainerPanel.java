package com.markantoni.warplaces.game.container;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.Draggable;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 15.02.16.
 */
public abstract class ContainerPanel<ContainerType extends Container, HolderType extends GameObject> extends UiPanel implements Draggable.DragListener {
    protected final List<ContainerPanel> mPanels = new ArrayList<ContainerPanel>();
    protected ContainerType mContainer;

    public ContainerPanel(GameScreen gameScreen, String name, int priority, boolean top, boolean left) {
        super(gameScreen, name, true, true, priority, top, left);
    }

    public static void registerPanels(ContainerPanel... panels) {
        for (ContainerPanel panel : panels) {
            for (ContainerPanel containerPanel : panels) {
                if (!panel.mPanels.contains(containerPanel)) {
                    panel.mPanels.add(containerPanel);
                }
            }
        }
    }

    @Override
    protected final Table create() {
        Table container = new Table();
        Table table = createTable();

        table.pack();
        container.add(table).row();

        return container;
    }

    protected abstract Table createTable();

    public abstract void showContainer(HolderType holder);

    protected abstract void handleContainer(ContainerType container);

    protected void setContainer(ContainerType container) {
        if (container != null && container == mContainer) {
            return;
        }

        if (mContainer != null) {
            mContainer.removeObserver(mObserver);
        }

        mContainer = container;
        mContainer.addObserver(mObserver);
        handleContainer(mContainer);
    }

    private final Container.Observer mObserver = new Container.Observer() {
        @Override
        public void changed(Container container) {
            handleContainer((ContainerType) container);
        }
    };

    protected Object getUserObject(float x, float y) {
        return getUi().getStage().hit(x, y, false).getParent().getUserObject();
    }

    protected abstract boolean drag(Object slot);

    protected abstract boolean drop(Object srcSlot, Object destSlot, Container container);

    @Override
    public final boolean onStart(Actor actor, float stageX, float stageY) {
        return drag(actor.getUserObject());
    }

    @Override
    public final void onDrag(Actor actor, float stageX, float stageY) {
    }

    @Override
    public final boolean onEnd(Actor actor, float stageX, float stageY) {
        Object srcSlot = actor.getUserObject();
        Item item = mContainer.getItem(srcSlot);
        if (item == null) {
            return false;
        }

        ContainerPanel droppedOn = null;
        for (ContainerPanel panel : mPanels) {
            if (panel.isFullyVisible() && UiUtils.contains(panel.getUi(), stageX, stageY)) {
                droppedOn = panel;
                break;
            }
        }

        if (droppedOn == null) {
            //drop all items from container to world
            while (mContainer.dropItem(srcSlot) != null) ;
            return true;
        }

        Object destSlot = droppedOn.getUserObject(stageX, stageY);
        if (destSlot == null) {
            return false;
        }

        if (droppedOn == this) {
            //drop on self
            return drop(srcSlot, destSlot, mContainer);
        } else {
            //drop on other panel
            return droppedOn.drop(srcSlot, destSlot, mContainer);
        }
    }
}
