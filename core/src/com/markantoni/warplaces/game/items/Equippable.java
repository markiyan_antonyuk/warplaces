package com.markantoni.warplaces.game.items;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Mark on 05.02.2016.
 */
public interface Equippable {
    TextureRegion[][] getEquippedTexture();
}
