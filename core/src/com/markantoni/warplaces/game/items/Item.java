package com.markantoni.warplaces.game.items;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderable;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.Random;

/**
 * Created by mark on 27.01.16.
 */
public abstract class Item extends GameObject implements Renderable {
    private final String mName;
    private final Texture mDroppedTexture;
    private final Sprite mPreviewSprite;
    private int mAngle;
    private Container mContainer;
    //TODO add textures for equipped when standing, moving, working

    protected Item(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, null);
        mName = name;
        mBounds.width = width;
        mBounds.height = height;
        mDroppedTexture = droppedTexture;
        mPreviewSprite = previewSprite;
    }

    @Override
    public Color getColor() {
        return Color.BLACK;
    }

    @Override
    public GameObjectManager getManager() {
        return getItemsManager();
    }

    @Override
    public void render(Renderer renderer) {
        if (getPhysicObject() != null && renderer.needToRender(getPhysicObject().getBounds())) {
            mBounds.set(getPhysicObject().getBounds());
            renderer.renderTexture(mDroppedTexture, mBounds, mAngle);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        if (getPhysicObject() != null && renderer.needToRender(getPhysicObject().getBounds())) {
            mBounds.set(getPhysicObject().getBounds());
            int divider = SizeUnit.unit(.1f);
            renderer.renderEllipse(mIsMainSelected ? Color.GOLD : Color.RED,
                    mBounds.x - mBounds.width / divider,
                    mBounds.y - mBounds.height / divider,
                    mBounds.width + mBounds.width / divider * 2,
                    mBounds.height + mBounds.height / divider * 2);
        }
    }

    @Override
    public void update(float deltaTime) {
        if (getPhysicObject() != null) {
            getPhysicObject().getBounds().getPosition(mPosition);
        }
    }

    @Override
    public Rect getBounds() {
        return mBounds.set(mPosition);
    }

    @Override
    public Vector2 getIsometricPosition() {
        return mPosition;
    }

    public void generateItemObject(World world, float x, float y) {
        setPhysicObject(new ItemObject(world, x, y, mBounds.width, mBounds.height));
        mAngle = generateAngle();
    }

    protected int generateAngle() {
        return new Random().nextInt(360);
    }

    public void setContainer(Container container) {
        if (mContainer != null && container != null) {
            new IllegalStateException("can't set container to item which has container").printStackTrace();
        }
        mContainer = container;
    }

    public Container getContainer() {
        return mContainer;
    }

    @Override
    public Sprite getPreviewSprite() {
        return mPreviewSprite;
    }

    public Texture getDroppedTexture() {
        return mDroppedTexture;
    }

    @Override
    public final String getName() {
        return mName;
    }

    @Override
    public String type() {
        return Strings.get("item");
    }

    public boolean isStackable() {
        return false;
    }

    @Override
    protected ItemObject getPhysicObject() {
        return (ItemObject) super.getPhysicObject();
    }
}
