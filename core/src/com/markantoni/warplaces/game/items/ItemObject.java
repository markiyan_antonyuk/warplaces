package com.markantoni.warplaces.game.items;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.objects.PhysicObject;
import com.markantoni.warplaces.utils.PhysicUtils;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by Mark on 29.01.2016.
 */
public class ItemObject extends PhysicObject {
    private Rect mBounds = Rect.empty();

    ItemObject(World world, float x, float y, float width, float height) {
        mBody = PhysicUtils.createBody(world, x, y, width, height);
        mBounds.setSize(width, height);
    }

    Rect getBounds() {
        mBounds.set(mBody.getPosition());
        return mBounds;
    }
}
