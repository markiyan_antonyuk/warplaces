package com.markantoni.warplaces.game.items;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Log;
import com.markantoni.warplaces.utils.RenderableSelected;
import com.markantoni.warplaces.utils.Renderer;

/**
 * Created by Mark on 29.01.2016.
 */
public class ItemsManager extends GameObjectManager<Item> implements RenderableSelected {
    private World mWorld;

    public ItemsManager(GameScreen gameScreen, World world) {
        super(gameScreen, null);
        mWorld = world;
    }

    public void createItem(Class<? extends Item> type, float x, float y) {
        createItem(createItem(type), x, y);
    }

    public void createItem(Item item, float x, float y) {
        add(item);
        item.generateItemObject(mWorld, x, y);
    }

    public Item createItem(Class<? extends Item> type) {
        try {
            return type.getConstructor(GameScreen.class).newInstance(getGameScreen());
        } catch (Exception e) {
            Log.d(ItemsManager.class, "cant create item " + type + " " + e.getCause().toString());
        }
        return null;
    }

    @Override
    public void render(Renderer renderer) {
        for (Item item : mAll) {
            item.render(renderer);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        for (Item item : mSelected) {
            item.renderSelected(renderer);
        }
    }

    public void update(float deltaTime, /*Unused*/RenderManager renderManager) {
        super.update(deltaTime, renderManager);
        for (Item item : mAll) {
            item.update(deltaTime);
        }
    }

    @Override
    protected boolean handleAction(float x, float y) {
        return false;
    }
}
