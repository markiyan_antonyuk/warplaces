package com.markantoni.warplaces.game.items.armor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.Equippable;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;

/**
 * Created by mark on 29.01.16.
 */
public abstract class Armor extends Item implements Equippable {
    //todo needed for armor, provide textures on player
    public Armor(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }
}
