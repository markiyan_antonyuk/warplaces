package com.markantoni.warplaces.game.items.helmets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;

/**
 * Created by Mark on 29.01.2016.
 */
public abstract class Helmet extends Item {
    public Helmet(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }
}
