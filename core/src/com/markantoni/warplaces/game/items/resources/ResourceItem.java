package com.markantoni.warplaces.game.items.resources;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 29.01.16.
 */
public abstract class ResourceItem extends Item {
    // TODO: 29.01.16 resources have capacity, weight, and other items, types

    protected ResourceItem(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }

    @Override
    protected int generateAngle() {
        return 0;
    }

    @Override
    public String type() {
        return Strings.get("resource_item");
    }

    @Override
    public boolean isStackable() {
        return true;
    }
}
