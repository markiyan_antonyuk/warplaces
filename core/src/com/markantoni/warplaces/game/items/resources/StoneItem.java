package com.markantoni.warplaces.game.items.resources;

import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 24.02.16.
 */
public class StoneItem extends ResourceItem {
    public StoneItem(GameScreen gameScreen) {
        super(gameScreen, Strings.get("stone"), SizeUnit.unit(.2f), SizeUnit.unit(.2f), Assets.Items.STONE.TEXTURE, Assets.Items.STONE.SPRITE);
    }
}