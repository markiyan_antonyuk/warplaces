package com.markantoni.warplaces.game.items.resources;

import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 05.02.2016.
 */
public class WheatItem extends ResourceItem {
    public WheatItem(GameScreen gameScreen) {
        super(gameScreen, Strings.get("wheat"), SizeUnit.unit(.2f), SizeUnit.unit(.2f), Assets.Items.WHEAT.TEXTURE, Assets.Items.WHEAT.SPRITE);
    }
}
