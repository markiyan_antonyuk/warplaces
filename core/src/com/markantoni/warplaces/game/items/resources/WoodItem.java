package com.markantoni.warplaces.game.items.resources;

import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 19.02.16.
 */
public class WoodItem extends ResourceItem {
    public WoodItem(GameScreen gameScreen) {
        super(gameScreen, Strings.get("wood"), SizeUnit.unit(.2f), SizeUnit.unit(.2f), Assets.Items.WOOD.TEXTURE, Assets.Items.WOOD.SPRITE);
    }
}
