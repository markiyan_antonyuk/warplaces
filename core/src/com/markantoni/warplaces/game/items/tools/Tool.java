package com.markantoni.warplaces.game.items.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.Equippable;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;

/**
 * Created by mark on 29.01.16.
 */
public abstract class Tool extends Item implements Equippable {
    //todo add attack power, speed and so on
    //TODO handle textures when unit moves, works, stands
    protected Tool(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }
}
