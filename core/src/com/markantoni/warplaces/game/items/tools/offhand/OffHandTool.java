package com.markantoni.warplaces.game.items.tools.offhand;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 05.02.2016.
 */
public abstract class OffHandTool extends Tool {
    protected OffHandTool(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }

    @Override
    public String type() {
        return Strings.get("off_hand_tool");
    }
}
