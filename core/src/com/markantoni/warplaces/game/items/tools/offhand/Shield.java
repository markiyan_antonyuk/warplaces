package com.markantoni.warplaces.game.items.tools.offhand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 08.02.16.
 */
public class Shield extends OffHandTool {
    public Shield(GameScreen gameScreen) {
        super(gameScreen, Strings.get("shield"), SizeUnit.unit(.2f), SizeUnit.unit(.2f), Assets.Items.SHIELD.TEXTURE, Assets.Items.SHIELD.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        return Assets.Units.SHIELD_EQUIPPED.TEXTURE_REGION;
    }
}
