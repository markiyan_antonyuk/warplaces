package com.markantoni.warplaces.game.items.tools.onehand;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 08.02.16.
 */
public abstract class OneHandTool extends Tool {
    protected OneHandTool(GameScreen gameScreen, String name, float width, float height, Texture droppedTexture, Sprite previewSprite) {
        super(gameScreen, name, width, height, droppedTexture, previewSprite);
    }

    @Override
    public String type() {
        return Strings.get("one_hand_tool");
    }
}
