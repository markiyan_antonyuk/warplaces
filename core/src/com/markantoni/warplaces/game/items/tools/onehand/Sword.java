package com.markantoni.warplaces.game.items.tools.onehand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.inventory.InventorySlot;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 08.02.16.
 */
public class Sword extends OneHandTool {
    public Sword(GameScreen gameScreen) {
        super(gameScreen, Strings.get("sword"), SizeUnit.unit(.2f), SizeUnit.unit(.2f), Assets.Items.SWORD.TEXTURE, Assets.Items.SWORD.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        if (getContainer().getSlot(this) == InventorySlot.OFFHAND_TOOL) {
            return Assets.Units.SWORD_EQUIPPED_OFF_HAND.TEXTURE_REGION;
        }
        return Assets.Units.SWORD_EQUIPPED_MAIN_HAND.TEXTURE_REGION;
    }
}
