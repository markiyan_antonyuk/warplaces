package com.markantoni.warplaces.game.items.tools.twohand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 22.02.16.
 */
public class Axe extends TwoHandTool {
    public Axe(GameScreen gameScreen) {
        super(gameScreen, Strings.get("axe"), SizeUnit.unit(.3f), SizeUnit.unit(.3f), Assets.Items.AXE.TEXTURE, Assets.Items.AXE.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        return Assets.Units.AXE_EQUIPPED.TEXTURE_REGION;
    }
}
