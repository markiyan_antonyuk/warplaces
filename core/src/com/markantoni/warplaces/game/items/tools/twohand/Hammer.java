package com.markantoni.warplaces.game.items.tools.twohand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 26.02.2016.
 */
public class Hammer extends TwoHandTool {
    public Hammer(GameScreen gameScreen) {
        super(gameScreen, Strings.get("hammer"), SizeUnit.unit(.3f), SizeUnit.unit(.3f), Assets.Items.HAMMER.TEXTURE, Assets.Items.HAMMER.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        return Assets.Units.HAMMER_EQUIPPED.TEXTURE_REGION;
    }
}
