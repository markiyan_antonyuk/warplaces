package com.markantoni.warplaces.game.items.tools.twohand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 24.02.16.
 */
public class Pickaxe extends TwoHandTool {
    public Pickaxe(GameScreen gameScreen) {
        super(gameScreen, Strings.get("pickaxe"), SizeUnit.unit(.3f), SizeUnit.unit(.3f), Assets.Items.PICKAXE.TEXTURE, Assets.Items.PICKAXE.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        return Assets.Units.PICKAXE_EQUIPPED.TEXTURE_REGION;
    }
}