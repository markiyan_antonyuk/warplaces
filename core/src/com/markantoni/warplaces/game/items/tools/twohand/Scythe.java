package com.markantoni.warplaces.game.items.tools.twohand;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 05.02.2016.
 */
public class Scythe extends TwoHandTool {
    public Scythe(GameScreen gameScreen) {
        super(gameScreen, Strings.get("scythe"), SizeUnit.unit(.3f), SizeUnit.unit(.3f), Assets.Items.SCYTHE.TEXTURE, Assets.Items.SCYTHE.SPRITE);
    }

    @Override
    public TextureRegion[][] getEquippedTexture() {
        return Assets.Units.SCYTHE_EQUIPPED.TEXTURE_REGION;
    }
}
