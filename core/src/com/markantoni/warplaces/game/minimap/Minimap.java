package com.markantoni.warplaces.game.minimap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.markantoni.warplaces.game.camera.Camera;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 09.03.16.
 */
public class Minimap {
    private final GameScreen mGameScreen;
    private final MinimapPanel mMinimapPanel;
    private final Rect mTempRect = Rect.empty();
    private Pixmap mPixmap;

    public Minimap(GameScreen gameScreen) {
        mGameScreen = gameScreen;
        mMinimapPanel = new MinimapPanel(gameScreen, this);
    }

    public void update() {
        TextureData textureData = mGameScreen.getTerrain().getTerrainPreview().getTextureData();
        if (!textureData.isPrepared()) {
            textureData.prepare();
        }
        Pixmap terrain = textureData.consumePixmap();
        if (mPixmap != null) {
            mPixmap.dispose();
        }

        mPixmap = new Pixmap(terrain.getWidth(), terrain.getHeight(), terrain.getFormat());
        mPixmap.drawPixmap(terrain, 0, 0);
        Rect realSize = mGameScreen.getTerrain().getRealSize();

        drawGameObjects(mPixmap, realSize);
        drawCameraRect(mGameScreen.getCamera(), mPixmap, realSize);
        mMinimapPanel.setPreview(new Texture(mPixmap));
    }

    private void drawGameObjects(Pixmap pixmap, Rect terrainSize) {
        for (GameObject gameObject : mGameScreen.getResourcesManager().getAll()) {
            drawGameObject(gameObject, pixmap, terrainSize);
        }

        for (GameObject gameObject : mGameScreen.getItemsManager().getAll()) {
            drawGameObject(gameObject, pixmap, terrainSize);
        }

        for (Nation nation : mGameScreen.getAllNations()) {
            for (GameObject gameObject : nation.getUnitManager().getAll()) {
                drawGameObject(gameObject, pixmap, terrainSize);
            }

            for (GameObject gameObject : nation.getBuildingsManager().getAll()) {
                drawGameObject(gameObject, pixmap, terrainSize);
            }
        }
    }

    private void drawCameraRect(Camera camera, Pixmap pixmap, Rect terrainSize) {
        pixmap.setColor(Color.WHITE);
        Rect rect = sizeRectToPixmap(pixmap, terrainSize, camera.getVisibleBounds());
        pixmapDrawRect(rect, pixmap, false);
    }

    private void drawGameObject(GameObject gameObject, Pixmap pixmap, Rect terrainSize) {
        pixmap.setColor(gameObject.getColor());
        Rect rect = sizeRectToPixmap(pixmap, terrainSize, gameObject.getBounds());
        pixmapDrawRect(rect, pixmap, true);
    }

    private Rect sizeRectToPixmap(Pixmap pixmap, Rect maxSize, Rect initialRect) {
        mTempRect.set(
                (initialRect.x * pixmap.getWidth() / maxSize.width),
                pixmap.getHeight() - (initialRect.y * pixmap.getHeight() / maxSize.height),
                (initialRect.width * pixmap.getWidth() / maxSize.width),
                -(initialRect.height * pixmap.getHeight() / maxSize.height));
        return mTempRect;
    }

    private void pixmapDrawRect(Rect rect, Pixmap pixmap, boolean fill) {
        if (fill) {
            pixmap.drawRectangle((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        } else {
            pixmap.drawRectangle((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        }
    }

    void moveCameraFromPreview(float x, float y, float width, float height) {
        Camera camera = mGameScreen.getCamera();
        Rect terrainSize = mGameScreen.getTerrain().getRealSize();
        camera.setTranslation(x * terrainSize.width / width, y * terrainSize.height / height);
    }
}
