package com.markantoni.warplaces.game.minimap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisImage;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 09.03.16.
 */
public class MinimapPanel extends UiPanel {
    private final Minimap mMinimap;
    private Table mTable;
    private VisImage mPreview;

    public MinimapPanel(GameScreen gameScreen, Minimap minimap) {
        super(gameScreen, MinimapPanel.class.getName(), true, true, 0, true, false);
        mMinimap = minimap;
    }

    @Override
    protected Table create() {
        setTitle(Strings.get("minimap"));
        mPreview = new VisImage();
        mTable = new Table();
        mTable.add(mPreview).size(SizeUnit.uiX(5), SizeUnit.uiY(5));
        mTable.pack();
        mPreview.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                mMinimap.moveCameraFromPreview(x, y, mPreview.getWidth(), mPreview.getHeight());
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                mMinimap.moveCameraFromPreview(x, y, mPreview.getWidth(), mPreview.getHeight());
            }
        });
        return mTable;
    }

    void setPreview(Texture terrainPreview) {
        terrainPreview.getTextureObjectHandle();
        mPreview.setDrawable(terrainPreview);
    }
}
