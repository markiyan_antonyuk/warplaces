package com.markantoni.warplaces.game.nations;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.objects.GameObjectFinder;
import com.markantoni.warplaces.game.Updatable;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.buildings.BuildingsManager;
import com.markantoni.warplaces.game.items.ItemsManager;
import com.markantoni.warplaces.game.resources.Resources;
import com.markantoni.warplaces.game.resources.ResourcesManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.UnitsManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mark on 29.01.16.
 */
public abstract class Nation implements Updatable {
    private final UnitsManager mUnitsManager;
    private final BuildingsManager mBuildingsManager;
    private final List<GameObjectManager> mManagers;
    private final GameScreen mGameScreen;
    private final Resources mResources;
    private final Color mColor;

    private boolean mIsPlayerNation;

    public Nation(GameScreen gameScreen, World world, Color color) { //todo sandboxgamescreen -> gamescreen
        mGameScreen = gameScreen;
        mUnitsManager = new UnitsManager(this, world);
        mBuildingsManager = new BuildingsManager(this, world);
        mManagers = new ArrayList<GameObjectManager>(Arrays.asList(
                mUnitsManager, mBuildingsManager, gameScreen.getItemsManager(), gameScreen.getResourcesManager()
        ));
        mResources = new Resources(this);
        mColor = color;
    }

    public Color getColor() {
        return mColor;
    }

    protected void setPlayerNation() {
        mIsPlayerNation = true;
    }

    public boolean isPlayerNation() {
        return mIsPlayerNation;
    }

    public GameScreen getGameScreen() {
        return mGameScreen;
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        mUnitsManager.update(deltaTime, renderManager);
        mBuildingsManager.update(deltaTime, renderManager);
    }

    @Override
    public void updateFinder(GameObjectFinder finder) {
        mUnitsManager.updateFinder(finder);
        mBuildingsManager.updateFinder(finder);
    }

    protected List<GameObjectManager> getAllManagers() {
        return mManagers;
    }

    public UnitsManager getUnitManager() {
        return mUnitsManager;
    }

    public ItemsManager getItemManager() {
        return mGameScreen.getItemsManager();
    }

    public BuildingsManager getBuildingsManager() {
        return mBuildingsManager;
    }

    public ResourcesManager getResourcesManager() {
        return mGameScreen.getResourcesManager();
    }

    public Resources getResources() {
        return mResources;
    }
}
