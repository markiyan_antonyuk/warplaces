package com.markantoni.warplaces.game.nations;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.ChooserRect;
import com.markantoni.warplaces.game.items.resources.StoneItem;
import com.markantoni.warplaces.game.items.resources.WoodItem;
import com.markantoni.warplaces.game.items.tools.twohand.Hammer;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.PlayerInput;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.buildings.Warehouse;
import com.markantoni.warplaces.game.items.resources.WheatItem;
import com.markantoni.warplaces.game.items.tools.offhand.Shield;
import com.markantoni.warplaces.game.items.tools.onehand.Sword;
import com.markantoni.warplaces.game.items.tools.twohand.Scythe;
import com.markantoni.warplaces.game.objects.GameObjectPanel;
import com.markantoni.warplaces.game.resources.ResourcesPanel;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.inventory.InventorySlot;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderable;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 29.01.16.
 */
public class PlayerNation extends Nation implements PlayerInput, Renderable {
    private ChooserRect mChooserRect;
    private GameObjectPanel mGameObjectPanel;
    private ResourcesPanel mResourcesPanel;

    public PlayerNation(GameScreen gameScreen, World world, Color color) {
        super(gameScreen, world, color);
        mChooserRect = new ChooserRect();
        mChooserRect.setCallback(mChooserRectCallback);
        mGameObjectPanel = new GameObjectPanel(gameScreen);
        mResourcesPanel = new ResourcesPanel(gameScreen, this);

        /********** REMOVE *********/
        getUnitManager().createUnit(SizeUnit.x(1), SizeUnit.y(0));
        getUnitManager().createUnit(SizeUnit.x(2), SizeUnit.y(0)).getInventory().addItem(new Hammer(getGameScreen()));
        /**** loaded ***/
        /*
        for (int i = 0; i < 200; i++) {
            for (int j = 0; j < 250; j++) {
                getUnitManager().createUnit(i * 25, j * 25, itemsManager);
            }
        }
        /**/
        Unit unit = getUnitManager().createUnit(SizeUnit.x(0), SizeUnit.y(0));
        unit.getInventory().addItem(getItemManager().createItem(WheatItem.class), InventorySlot.BACKPACK_1);
        unit.getInventory().addItem(new Shield(gameScreen), InventorySlot.BACKPACK_2);
        unit.getInventory().addItem(new Sword(gameScreen), InventorySlot.BACKPACK_3);
        unit.getInventory().addItem(new Sword(gameScreen), InventorySlot.BACKPACK_4);
        unit.getInventory().addItem(new Scythe(gameScreen));

        Warehouse warehouse = (Warehouse) getBuildingsManager().createBuilding(Warehouse.class, 4, 2);
        getBuildingsManager().createBuilding(Warehouse.class, 6, 2);
        warehouse.getStorage().addItem(new WheatItem(gameScreen), 3);
        warehouse.getStorage().addItem(new Scythe(gameScreen));
        warehouse.getStorage().addItem(new WheatItem(gameScreen), 5);
        warehouse.getStorage().addItem(new Scythe(gameScreen));
        warehouse.getStorage().addItem(new Scythe(gameScreen));
        warehouse.getStorage().addItem(new WheatItem(gameScreen));
        warehouse.getStorage().addItem(new Scythe(gameScreen));
        for (int i = 0; i < 99; i++) {
            warehouse.getStorage().addItem(new WheatItem(gameScreen));
            warehouse.getStorage().addItem(new StoneItem(gameScreen));
            warehouse.getStorage().addItem(new WoodItem(gameScreen));
        }
        /*******************/
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        super.update(deltaTime, renderManager);

        for (GameObjectManager manager : getAllManagers()) {
            if (!manager.getSelection().isEmpty()) {
                mGameObjectPanel.setManager(manager);
                break;
            }
        }

        mGameObjectPanel.update();
        mResourcesPanel.update();
    }

    @Override
    public void render(Renderer renderer) {
        mChooserRect.render(renderer);
    }

    @Override
    public boolean handleTap(float x, float y, int button) {
        if (!Settings.isInTouchmode() && button == GameScreen.SELECTION_BUTTON) {
            Utils.loop(getAllManagers(), new Utils.LoopAction<GameObjectManager>() {
                @Override
                public void action(GameObjectManager gameObjectManager) {
                    gameObjectManager.clearSelection();
                }
            });
        }

        if (getUnitManager().handleTap(x, y, button)) {
            clearTouchSelectionExcept(getUnitManager());
            return true;
        }

        if (getItemManager().handleTap(x, y, button)) {
            clearTouchSelectionExcept(getItemManager());
            return true;
        }

        if (getBuildingsManager().handleTap(x, y, button)) {
            clearTouchSelectionExcept(getBuildingsManager());
            return true;
        }

        if (getResourcesManager().handleTap(x, y, button)) {
            clearTouchSelectionExcept(getResourcesManager());
            return true;
        }

        return false;
    }

    private void clearTouchSelectionExcept(final GameObjectManager except) {
        if (!Settings.isInTouchmode()) {
            return;
        }

        Utils.loop(getAllManagers(), new Utils.LoopAction<GameObjectManager>() {
            @Override
            public void action(GameObjectManager gameObjectManager) {
                if (except != gameObjectManager) {
                    gameObjectManager.clearSelection();
                }
            }
        });
    }

    @Override
    public void handleLongPress() {
        Utils.loop(getAllManagers(), new Utils.LoopAction<GameObjectManager>() {
            @Override
            public void action(GameObjectManager gameObjectManager) {
                gameObjectManager.clearSelection();
            }
        });
    }

    @Override
    public boolean handleRect(Rect rect) {
        Utils.loop(getAllManagers(), new Utils.LoopAction<GameObjectManager>() {
            @Override
            public void action(GameObjectManager gameObjectManager) {
                gameObjectManager.clearSelection();
            }
        });

        if (getUnitManager().handleRect(rect)) {
            return true;
        }

        if (getItemManager().handleRect(rect)) {
            return true;
        }

        if (getBuildingsManager().handleRect(rect)) {
            return true;
        }

        return getResourcesManager().handleRect(rect);
    }

    public ChooserRect getChooserRect() {
        return mChooserRect;
    }

    private final ChooserRect.Callback mChooserRectCallback = new ChooserRect.Callback() {
        @Override
        public void chosen(Rect bounds) {
            handleRect(bounds);
        }
    };
}
