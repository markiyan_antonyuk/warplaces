package com.markantoni.warplaces.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.UnitsManager;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 11.02.16.
 */
public class ChooserPanel extends UiPanel {
    private static final int WIDTH = SizeUnit.uiX(1);
    private static final int HEIGHT = SizeUnit.uiY(1);
    private static final int PADDING = SizeUnit.uiUnit(.1f);
    private static final int MAX_BUTTONS = 9;
    private static final int BUTTONS_IN_ROW = 3;

    private List<VisTextButton> mButtons;
    private GameObjectManager<? extends GameObject> mManager;
    private Map<Class<? extends GameObject>, Integer> mObjects = new HashMap<Class<? extends GameObject>, Integer>();
    private Map<Class<? extends GameObject>, Sprite> mSprites = new HashMap<Class<? extends GameObject>, Sprite>();

    public ChooserPanel(GameScreen gameScreen) {
        super(gameScreen, ChooserPanel.class.getName(), true, true, 1, true, true);
    }

    @Override
    protected Table create() {
        setTitle(Strings.get("selection"));
        Table table = new Table();
        table.defaults().size(WIDTH, HEIGHT).pad(PADDING);

        mButtons = new ArrayList<VisTextButton>(MAX_BUTTONS);
        for (int i = 0; i < MAX_BUTTONS; i++) {
            if (i > 0 && i % BUTTONS_IN_ROW == 0) {
                table.row();
            }

            final VisTextButton button = new VisTextButton("");
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    mManager.cutSelectionTo((Class<? extends GameObject>) button.getUserObject());
                }
            });
            button.setFocusBorderEnabled(false);
            button.setVisible(false);
            table.add(button);
            mButtons.add(button);
        }
        return table;
    }

    public void setManager(GameObjectManager<? extends GameObject> gameObjectManager) {
        mManager = gameObjectManager;
        mObjects.clear();
        mSprites.clear();

        if (gameObjectManager instanceof UnitsManager) {
            for (GameObject object : mManager.getSelection()) {
                Unit unit = (Unit) object;
                if (!mSprites.containsKey(unit.getType())) {
                    mSprites.put(unit.getType(), unit.getPreviewSprite());
                    mObjects.put(unit.getType(), 1);
                } else {
                    mObjects.put(unit.getType(), mObjects.get(unit.getType()) + 1);
                }
            }
        } else {
            for (GameObject object : mManager.getSelection()) {
                if (!mSprites.containsKey(object.getClass())) {
                    mSprites.put(object.getClass(), object.getPreviewSprite());
                    mObjects.put(object.getClass(), 1);
                } else {
                    mObjects.put(object.getClass(), mObjects.get(object.getClass()) + 1);
                }
            }
        }


        int counter = 0;
        for (Class<? extends GameObject> objectClass : mObjects.keySet()) {
            VisTextButton button = mButtons.get(counter);

            button.setText(mObjects.get(objectClass) + "");
            updateButton(button, mSprites.get(objectClass));
            button.setUserObject(objectClass);

            counter++;
        }

        for (; counter < MAX_BUTTONS; counter++) {
            updateButton(mButtons.get(counter), null);
        }
    }

    private void updateButton(VisTextButton button, Sprite sprite) {
        if (sprite == null) {
            button.setVisible(false);
        } else {
            SpriteDrawable drawable = new SpriteDrawable(sprite);
            VisTextButton.VisTextButtonStyle style = new VisImageTextButton.VisImageTextButtonStyle(drawable, drawable.tint(Color.CYAN), drawable, button.getStyle().font);
            button.getLabel().setFontScale(2);
            button.setStyle(style);
            button.setVisible(true);
        }
    }
}
