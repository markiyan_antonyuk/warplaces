package com.markantoni.warplaces.game.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.buildings.BuildingsManager;
import com.markantoni.warplaces.game.items.ItemsManager;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.resources.ResourcesManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.attributes.Attributes;
import com.markantoni.warplaces.game.units.UnitsManager;
import com.markantoni.warplaces.utils.Point;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.RenderableSelected;
import com.markantoni.warplaces.utils.Renderer;

/**
 * Created by mark on 04.02.16.
 */
public abstract class GameObject implements RenderableSelected, Attributes {
    protected final Vector2 mPosition = new Vector2();
    protected final Vector2 mCenter = new Vector2();
    protected final Point mTiledPosition = new Point();
    protected final Rect mBounds = Rect.empty();
    private final Nation mNation;
    private final GameScreen mGameScreen;
    protected boolean mIsSelected;
    protected boolean mIsMainSelected;
    private PhysicObject mPhysicObject;

    public GameObject(GameScreen gameScreen, Nation nation) {
        mGameScreen = gameScreen;
        mNation = nation;
    }

    public abstract Color getColor();

    public abstract String getName();

    public abstract GameObjectManager getManager();

    public void destroy() {
        getManager().remove(this);
    }

    public ItemsManager getItemsManager() {
        return mGameScreen.getItemsManager();
    }

    public ResourcesManager getResourcesManager() {
        return mGameScreen.getResourcesManager();
    }

    public GameObjectFinder getGameObjectFinder() {
        return mGameScreen.getGameObjectFinder();
    }

    public UnitsManager getUnitsManager() {
        return mNation.getUnitManager();
    }

    public BuildingsManager getBuildingsManager() {
        return mNation.getBuildingsManager();
    }

    protected void setPhysicObject(PhysicObject physicObject) {
        mPhysicObject = physicObject;
        physicObject.getBody().getFixtureList().first().setUserData(this);
    }

    protected PhysicObject getPhysicObject() {
        return mPhysicObject;
    }

    public void freePhysicObject(World world) {
        if (mPhysicObject != null) {
            mPhysicObject.destroy(world);
        }
        mPhysicObject = null;
    }

    public void setMainSelected(boolean value) {
        mIsMainSelected = value;
    }

    public boolean isMainSelected() {
        return mIsMainSelected;
    }

    public final Point getTilePosition() {
        return mTiledPosition.set((int) (getCenter().x / AbstractGameScreen.DEFAULT_TILE_SIZE.width), (int) (getCenter().y / AbstractGameScreen.DEFAULT_TILE_SIZE.height));
    }

    public abstract Sprite getPreviewSprite();

    public void setSelected(boolean select) {
        mIsSelected = select;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    @Override
    public abstract void renderSelected(Renderer renderer);

    public Vector2 getCenter() {
        return getBounds().getCenter(mCenter);
    }

    public abstract Vector2 getIsometricPosition();

    public abstract Rect getBounds();

    public abstract void update(float deltaTime);


    @Override
    public String type() {
        return null;
    }

    @Override
    public String health() {
        return null;
    }

    @Override
    public String speed() {
        return null;
    }

    @Override
    public String defence() {
        return null;
    }

    @Override
    public String attack() {
        return null;
    }

    @Override
    public String weight() {
        return null;
    }

    @Override
    public String capacity() {
        return null;
    }

    @Override
    public String inventory() {
        return null;
    }

    @Override
    public String storage() {
        return null;
    }
}