package com.markantoni.warplaces.game.objects;

import com.markantoni.warplaces.utils.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by mark on 04.02.16.
 */
public class GameObjectFinder {
    private final Set<GameObject> mSet = new TreeSet<GameObject>(mComparator);
    private Map<Point, List<GameObject>> mMap = new HashMap<Point, List<GameObject>>();

    public void clear() {
        mMap.clear();
    }

    public void put(GameObject gameObject) {
        Point point = gameObject.getTilePosition();
        if (mMap.containsKey(point)) {
            mMap.get(point).add(gameObject);
        } else {
            mMap.put(point, new ArrayList<GameObject>(Arrays.asList(gameObject)));
        }
    }

    public Set<GameObject> findAll(GameObject caller, int radius, Filter... filters) {
        //fixme make nice optimization or what
        int x = caller.getTilePosition().x;
        int y = caller.getTilePosition().y;
        mSet.clear();

        for (int delta = 0; delta < radius; delta++) {
            for (int deltaX = -delta; deltaX <= delta; deltaX++) {
                int xPoint = x + deltaX;
                for (int deltaY = -delta; deltaY <= delta; deltaY++) {
                    int yPoint = y + deltaY;

                    Point point = new Point(xPoint, yPoint);
                    List<GameObject> objects = mMap.get(point);

                    if (objects != null) {
                        for (GameObject gameObject : objects) {
                            for (Filter filter : filters) {
                                if (gameObject != caller
                                        && filter.mType.isAssignableFrom(gameObject.getClass())
                                        && filter.apply(gameObject)) {
                                    mSet.add(gameObject);
                                }
                            }
                        }
                    }
                }
            }
        }

        return mSet;
    }

    //todo merge with findall
    public GameObject find(GameObject caller, int radius, Filter... filters) {
        //fixme make nice optimization or what
        int x = caller.getTilePosition().x;
        int y = caller.getTilePosition().y;

        GameObject closest = null;
        float minDist = Float.MAX_VALUE;

        for (int delta = 0; delta < radius; delta++) {
            for (int deltaX = -delta; deltaX <= delta; deltaX++) {
                int xPoint = x + deltaX;
                for (int deltaY = -delta; deltaY <= delta; deltaY++) {
                    int yPoint = y + deltaY;

                    Point point = new Point(xPoint, yPoint);
                    List<GameObject> objects = mMap.get(point);

                    if (objects != null) {
                        for (GameObject gameObject : objects) {
                            for (Filter filter : filters) {
                                if (gameObject != caller
                                        && filter.mType.isAssignableFrom(gameObject.getClass())
                                        && filter.apply(gameObject)) {
                                    float distance = gameObject.getCenter().dst(caller.getCenter());
                                    if (distance < minDist) {
                                        minDist = distance;
                                        closest = gameObject;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (closest != null) {
                return closest;
            }
        }

        return null;
    }

    private static final Comparator<GameObject> mComparator = new Comparator<GameObject>() {
        @Override
        public int compare(GameObject o1, GameObject o2) {
            return (int) o1.getTilePosition().dst(o2.getTilePosition());
        }
    };

    public static class Filter {
        private Class<? extends GameObject> mType;

        public Filter(Class<? extends GameObject> type) {
            mType = type;
        }

        public boolean apply(GameObject object) {
            return true;
        }
    }
}
