package com.markantoni.warplaces.game.objects;

import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.*;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Rect;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by mark on 10.02.16.
 */
public abstract class GameObjectManager<T extends GameObject> implements PlayerInput, Updatable {
    protected final SortedSet<T> mSelected = new TreeSet<T>(mComparator);
    protected final List<T> mAll = new ArrayList<T>();
    private final List<T> mObjectsToRemove = new ArrayList<T>();
    private final Nation mNation;
    private final GameScreen mGameScreen;
    private final List<Listener> mListeners = new ArrayList<Listener>();
    protected T mMainSelected;

    public GameObjectManager(GameScreen gameScreen, Nation nation) {
        mGameScreen = gameScreen;
        mNation = nation;
    }

    public final GameScreen getGameScreen() {
        return mGameScreen;
    }

    public final Nation getNation() {
        return mNation;
    }

    public void setMainSelected(T t) {
        for (T type : mAll) {
            type.setMainSelected(false);
        }
        if (t != null) {
            t.setMainSelected(true);
        }
        mMainSelected = t;
    }

    public T getMainSelected() {
        return mMainSelected;
    }

    protected void add(T t) {
        mAll.add(t);
        for (Listener listener : mListeners) {
            listener.added(this, t);
        }
    }

    protected final void select(T t) {
        t.setSelected(true);
        mSelected.add(t);

        if (mMainSelected == null) {
            mMainSelected = t;
            mMainSelected.setMainSelected(true);
        }
    }

    public void remove(T t) {
        t.setSelected(false);
        t.setMainSelected(false);
        mAll.remove(t);
        mSelected.remove(t);
        mObjectsToRemove.add(t);
        for (Listener listener : mListeners) {
            listener.removed(this, t);
        }
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        for (int i = mObjectsToRemove.size() - 1; i >= 0; i--) {
            if (!mGameScreen.getWorld().isLocked()) {
                mObjectsToRemove.get(i).freePhysicObject(mGameScreen.getWorld());
                mObjectsToRemove.remove(i);
            }
        }
    }

    @Override
    public void updateFinder(GameObjectFinder finder) {
        for (T t : mAll) {
            finder.put(t);
        }
    }

    protected void cutSelectionTo(Class<? extends GameObject> type) {
        List<T> objects = new ArrayList<T>();
        for (T t : mSelected) {
            if (t.getClass().equals(type)) {
                objects.add(t);
            }
        }

        clearSelection();
        for (T t : objects) {
            select(t);
        }
    }

    public final List<T> getSelection() {
        //todo optimize, don't create the list every time
        return new ArrayList<T>(mSelected);
    }

    public final List<T> getAll() {
        return mAll;
    }

    public final void clearSelection() {
        for (T t : mSelected) {
            t.setSelected(false);
            t.setMainSelected(false);
        }
        mSelected.clear();
        mMainSelected = null;
    }

    protected abstract boolean handleAction(float x, float y);

    private boolean handleSelection(float x, float y) {
        for (T t : mAll) {
            if (t.getBounds().contains(x, y)) {
                clearSelection();
                select(t);
                return true;
            }
        }
        return false;
    }

    @Override
    public final boolean handleTap(float x, float y, int button) {
        boolean result = false;

        if (!Settings.isInTouchmode()) {
            if (button == GameScreen.SELECTION_BUTTON) {
                if (handleSelection(x, y)) {
                    result = true;
                }
            }

            if (button == GameScreen.ACTION_BUTTON) {
                result = handleAction(x, y);
            }
        } else {
            if (handleSelection(x, y)) {
                result = true;
            } else {
                result = handleAction(x, y);

            }
        }
        return result;
    }

    @Override
    public final void handleLongPress() {
        if (Settings.isInTouchmode()) {
            clearSelection();
        }
    }

    @Override
    public final boolean handleRect(Rect rect) {
        boolean result = false;

        for (T t : mAll) {
            if (rect.overlaps(t.getBounds())) {
                select(t);
                result = true;
            }
        }

        return result;
    }

    private static final Comparator<GameObject> mComparator = new Comparator<GameObject>() {
        @Override
        public int compare(GameObject o1, GameObject o2) {
            int result = (int) (o1.getIsometricPosition().dst(0, 0) - o2.getIsometricPosition().dst(0, 0)) * 1000;
            if (result == 0) {
                result = o1.hashCode() - o2.hashCode();
            }
            return result;
        }
    };

    public void addListener(Listener listener) {
        mListeners.add(listener);
    }

    public void removeListener(Listener listener) {
        mListeners.remove(listener);
    }

    public interface Listener<T extends GameObject> {
        void added(GameObjectManager manager, T gameObject);

        void removed(GameObjectManager manager, T gameObject);
    }
}
