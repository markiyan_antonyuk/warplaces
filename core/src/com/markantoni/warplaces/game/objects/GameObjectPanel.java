package com.markantoni.warplaces.game.objects;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.actions.ActionsPanel;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.buildings.storage.StoragePanel;
import com.markantoni.warplaces.game.container.ContainerPanel;
import com.markantoni.warplaces.game.attributes.GameObjectAttributes;
import com.markantoni.warplaces.game.attributes.Attributes;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.inventory.InventoryPanel;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.List;

/**
 * Created by mark on 18.02.16.
 */
public class GameObjectPanel extends UiPanel {
    private static final int MAX_STATISTICS = 6;

    private Table mTable;
    private Table mStatisticsTable;
    private Table mNavigationTable;
    private VisLabel mStatisticLabels[][];
    private VisTextButton mPreview;
    private VisTextButton mNext;
    private VisTextButton mPrevious;

    private final InventoryPanel mInventoryPanel;
    private final StoragePanel mStoragePanel;
    private final ChooserPanel mChooserPanel;
    private final ActionsPanel mActionsPanel;
    private final GameObjectAttributes mGameObjectAttributes;
    private GameObjectManager mManager;
    private int mIndex;

    public GameObjectPanel(GameScreen gameScreen) {
        super(gameScreen, GameObjectPanel.class.getName(), false, true, 1, false, true);
        mGameObjectAttributes = new GameObjectAttributes();
        mInventoryPanel = new InventoryPanel(gameScreen);
        mStoragePanel = new StoragePanel(gameScreen);
        mChooserPanel = new ChooserPanel(gameScreen);
        mActionsPanel = new ActionsPanel(gameScreen);

        ContainerPanel.registerPanels(mInventoryPanel, mStoragePanel);
    }

    @Override
    protected Table create() {
        mTable = new Table();
        mNavigationTable = new Table();
        mPreview = new VisTextButton("");
        mPreview.setFocusBorderEnabled(false);

        float width = SizeUnit.uiX(1);
        float height = SizeUnit.uiY(1);
        float padding = SizeUnit.uiUnit(.1f);

        mTable.defaults().pad(padding);
        mTable.add(mPreview).size(width, height);
        mTable.pack();

        mNavigationTable.defaults().minSize(width, height)
                .prefSize(mTable.getWidth() / 2, height)
                .pad(padding);

        mNext = new VisTextButton(Strings.get("next"));
        mPrevious = new VisTextButton(Strings.get("previous"));
        mNavigationTable.add(mPrevious, mNext);

        mNext.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                chooseNext(true);
            }
        });

        mPrevious.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                chooseNext(false);
            }
        });

        mStatisticLabels = new VisLabel[MAX_STATISTICS][2];
        mStatisticsTable = new Table();
        mStatisticsTable.defaults().prefWidth(mTable.getWidth() / 2).pad(1).fillX();
        for (int i = 0; i < mStatisticLabels.length; i++) {
            mStatisticLabels[i] = new VisLabel[]{new VisLabel("", Align.left), new VisLabel("", Align.right)};
        }

        Table table = new Table();
        table.add(mTable).row();
        table.add(mStatisticsTable).row();
        table.add(mNavigationTable);
        return table;
    }

    public void setManager(GameObjectManager manager) {
        if (mManager != manager) {
            mManager = manager;
            mIndex = 0;
        }
    }

    public void update() {
        if (mManager == null) {
            setVisible(false);
            return;
        }

        if (mManager.getSelection().isEmpty()) {
            setVisible(false);
            return;
        }

        boolean moreThanOneSelected = mManager.getSelection().size() > 1;
        mChooserPanel.setManager(mManager);
        mChooserPanel.setVisible(moreThanOneSelected);
        showNavigation(moreThanOneSelected);

        updateIndex();
        handleSelected((GameObject) mManager.getSelection().get(mIndex));
    }

    private void handleSelected(GameObject selected) {
        mManager.setMainSelected(selected);
        setVisible(true);
        setTitle(selected.getName());
        updatePreview(selected);

        mStoragePanel.setVisible(false);
        mInventoryPanel.setVisible(false);

        setStatistics(selected);
        if (selected instanceof ActionsPerformer) {
            mActionsPanel.setActionsPerformer((ActionsPerformer) selected);
        } else {
            mActionsPanel.setVisible(false);
        }

        if (selected instanceof Unit) {
            Unit unit = (Unit) selected;
            mInventoryPanel.showContainer(unit);

            //todo add arrows to show storage for several collided buildings
            StorageBuilding storageBuilding = unit.getCollidedStorage();
            selected.getBuildingsManager().setMainSelected(storageBuilding);

            if (storageBuilding != null && !storageBuilding.isUnderConstruction()) {
                mStoragePanel.showContainer(storageBuilding);
            }
        }

        if (selected instanceof StorageBuilding) {
            StorageBuilding storageBuilding = (StorageBuilding) selected;
            mStoragePanel.showContainer(storageBuilding);
        }
    }

    private void updateIndex() {
        if (mIndex >= mManager.getSelection().size()) {
            mIndex = 0;
        }

        if (mIndex < 0) {
            mIndex = mManager.getSelection().size() - 1;
        }
    }

    private void chooseNext(boolean next) {
        mIndex += next ? 1 : -1;
        updateIndex();
    }

    private void updatePreview(GameObject gameObject) {
        SpriteDrawable drawable = new SpriteDrawable(gameObject.getPreviewSprite());
        VisTextButton.VisTextButtonStyle style =
                new VisImageTextButton.VisImageTextButtonStyle(drawable, drawable, drawable, mPreview.getStyle().font);
        mPreview.setStyle(style);
    }

    private void setStatistics(Attributes attributes) {
        mStatisticsTable.clearChildren();

        mGameObjectAttributes.parse(attributes);
        List<GameObjectAttributes.Property> list = mGameObjectAttributes.getAttributes();
        for (int i = 0; i < MAX_STATISTICS; i++) {
            if (i >= list.size()) {
                break;
            } else {
                GameObjectAttributes.Property property = list.get(i);
                mStatisticLabels[i][0].setText(property.getKey() + ":");
                mStatisticLabels[i][1].setText(property.getValue() + "");
                mStatisticsTable.add(mStatisticLabels[i]);
                mStatisticsTable.row();
            }
        }

        getUi().pack();
    }

    private void showNavigation(boolean show) {
        mNavigationTable.setVisible(show);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (!visible) {
            mInventoryPanel.setVisible(false);
            mStoragePanel.setVisible(false);
            mChooserPanel.setVisible(false);
            mActionsPanel.setVisible(false);
        }
    }
}
