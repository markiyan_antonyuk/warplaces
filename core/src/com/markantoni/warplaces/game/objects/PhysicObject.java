package com.markantoni.warplaces.game.objects;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by mark on 26.02.16.
 */
public abstract class PhysicObject {
    protected Body mBody;

    public Body getBody() {
        return mBody;
    }

    protected void destroy(World world) {
        world.destroyBody(mBody);
    }
}
