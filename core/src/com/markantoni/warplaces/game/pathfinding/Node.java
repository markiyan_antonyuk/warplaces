package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.ReflectionPool;
import com.markantoni.warplaces.utils.Point;

/**
 * Created by mark on 25.02.16.
 */
class Node implements IndexedNode<Node>, Pool.Poolable {
    private static final Pool<Node> sNodePool = new ReflectionPool<Node>(Node.class);
    private final Array<Connection<Node>> mConnections = new Array<Connection<Node>>();
    private final Point mPoint = new Point();
    private NodeGraph mNodeGraph;
    private float mCost;
    private int mIndex;

    static Node obtain(NodeGraph nodeGraph, int cellX, int cellY) {
        return obtain(nodeGraph, cellX, cellY, 1);
    }

    static Node obtain(NodeGraph nodeGraph, int cellX, int cellY, float cost) {
        Node node = sNodePool.obtain();
        node.setup(nodeGraph, cellX, cellY, cost);
        return node;
    }

    void free() {
        sNodePool.free(this);
    }

    private void setup(NodeGraph nodeGraph, int cellX, int cellY, float cost) {
        mPoint.set(cellX, cellY);
        mNodeGraph = nodeGraph;
        mCost = cost;
    }

    @Override
    public void reset() {
        mPoint.set(0, 0);
        mCost = 0;
    }

    void setIndex(int index) {
        mIndex = index;
    }

    float getCost() {
        return mCost;
    }

    Point getPosition() {
        return mPoint;
    }

    @Override
    public int getIndex() {
        return mIndex;
    }

    @Override
    public Array<Connection<Node>> getConnections() {
        for (Connection<Node> connection : mConnections) {
            ((NodeConnection) connection).free();
        }
        mConnections.clear();
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, 1, 0));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, 1, 1));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, 0, 1));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, -1, 1));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, -1, 0));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, -1, -1));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, 0, -1));
        mConnections.add(NodeConnection.obtain(mNodeGraph, this, 1, -1));

        for (int i = mConnections.size - 1; i >= 0; i--) {
            if (mConnections.get(i).getToNode() == null) {
                mConnections.removeIndex(i);
            }
        }

        return mConnections;
    }

    @Override
    public String toString() {
        return "Node index: " + mIndex + " position: " + mPoint + " cost: " + mCost;
    }
}
