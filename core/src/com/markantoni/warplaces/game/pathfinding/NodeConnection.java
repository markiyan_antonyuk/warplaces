package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.ReflectionPool;
import com.markantoni.warplaces.utils.Point;

/**
 * Created by mark on 25.02.16.
 */
class NodeConnection implements Connection<Node>, Pool.Poolable {
    private static final Pool<NodeConnection> sNodeConnectionPool = new ReflectionPool<NodeConnection>(NodeConnection.class);
    private final Point mPoint = new Point();
    private NodeGraph mNodeGraph;
    private Node mNode;

    static NodeConnection obtain(NodeGraph nodeGraph, Node node, int nextX, int nextY) {
        NodeConnection connection = sNodeConnectionPool.obtain();
        connection.setup(nodeGraph, node, nextX, nextY);
        return connection;
    }

    void free() {
        sNodeConnectionPool.free(this);
    }

    private void setup(NodeGraph nodeGraph, Node node, int nextX, int nextY) {
        mNodeGraph = nodeGraph;
        mNode = node;
        mPoint.set(node.getPosition().x + nextX, node.getPosition().y + nextY);
    }

    @Override
    public void reset() {
        mNode = null;
        mPoint.set(0, 0);
    }

    @Override
    public float getCost() {
        return getToNode().getCost() * mNode.getCost();
    }

    @Override
    public Node getFromNode() {
        return mNode;
    }

    @Override
    public Node getToNode() {
        return mNodeGraph.getNode(mPoint);
    }
}
