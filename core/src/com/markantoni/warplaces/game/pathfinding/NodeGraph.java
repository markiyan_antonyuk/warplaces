package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;
import com.markantoni.warplaces.utils.Point;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mark on 25.02.16.
 */
class NodeGraph implements IndexedGraph<Node> {
    private final Point mTempPoint = new Point();
    private final Map<Point, Node> mNodes = Collections.synchronizedMap(new HashMap<Point, Node>());
    private boolean mIsChanged;

    Node getNode(Point p) {
        return mNodes.get(p);
    }

    Node getNode(int x, int y) {
        mTempPoint.set(x, y);
        return getNode(mTempPoint);
    }

    void addNode(Node node) {
        if (mNodes.containsKey(node.getPosition())) {
            removeNode(node);
        }
        mNodes.put(node.getPosition(), node);
        mIsChanged = true;
    }

    void removeNode(Node node) {
        removeNode(node.getPosition());
    }

    void removeNode(Point point) {
        mIsChanged = true;
        if (mNodes.containsKey(point)) {
            mNodes.remove(point).free();
        }
    }

    void removeNode(int x, int y) {
        mTempPoint.set(x, y);
        removeNode(mTempPoint);
    }

    void removeNodeCell(Point point) {
        int x = point.x * 2;
        int y = point.y * 2;
        removeNode(x, y);
        removeNode(x + 1, y);
        removeNode(x, y + 1);
        removeNode(x + 1, y + 1);
    }

    private void updateIndexes() {
        int index = 0;
        for (Node node : mNodes.values()) {
            node.setIndex(index++);
        }
    }

    void resetChanged() {
        updateIndexes();
        mIsChanged = false;
    }

    boolean isChanged() {
        return mIsChanged;
    }

    @Override
    public int getNodeCount() {
        return mNodes.size();
    }

    @Override
    public Array<Connection<Node>> getConnections(Node fromNode) {
        return mNodes.get(fromNode.getPosition()).getConnections();
    }
}
