package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.math.Vector2;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.buildings.Building;
import com.markantoni.warplaces.game.buildings.BuildingsManager;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.resources.objects.Resource;
import com.markantoni.warplaces.game.resources.ResourcesManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.utils.Point;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mark on 25.02.16.
 */
public class Pathfinder {
    //listener on building and resources
    private static final int DEFAULT_CELL_SIZE = (int) (AbstractGameScreen.DEFAULT_TILE_SIZE.width / 2);
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private final GameScreen mGameScreen;
    private final NodeGraph mNodeGraph;
    private final Pathsmoother mPathsmoother;
    private IndexedAStarPathFinder<Node> mPathFinder;

    public Pathfinder(GameScreen gameScreen) {
        mGameScreen = gameScreen;
        mNodeGraph = new NodeGraph();
        mPathsmoother = new Pathsmoother(mNodeGraph);
        handleTerrain();
        handleGameObjects();
    }

    private void handleGameObjects() {
        for (Nation nation : mGameScreen.getAllNations()) {
            BuildingsManager buildingsManager = nation.getBuildingsManager();
            buildingsManager.addListener(mBuildingsListener);
            for (Building building : buildingsManager.getAll()) {
                mBuildingsListener.added(buildingsManager, building);
            }
        }

        ResourcesManager resourcesManager = mGameScreen.getResourcesManager();
        resourcesManager.addListener(mResourcesListener);
        for (Resource resource : resourcesManager.getAll()) {
            mResourcesListener.added(resourcesManager, resource);
        }
    }

    private void handleTerrain() {
        Terrain terrain = mGameScreen.getTerrain();
        Map<Terrain.Tile, Map<Point, Point>> tileMap = terrain.getTileMap();
        for (Terrain.Tile tile : tileMap.keySet()) {
            if (tile.isPassable()) {
                for (int y = 0; y < terrain.getSize() * 2; y++) {
                    for (int x = 0; x < terrain.getSize() * 2; x++) {
                        mNodeGraph.addNode(Node.obtain(mNodeGraph, x, y));
                    }
                }
            } else {
                for (Point point : tileMap.get(tile).keySet()) {
                    mNodeGraph.removeNodeCell(point);
                }
            }
        }
    }

    public GraphPath<Node> search(Point from, Point to) {
        if (mNodeGraph.isChanged()) {
            mNodeGraph.resetChanged();
            mPathFinder = new IndexedAStarPathFinder<Node>(mNodeGraph);
        }

        Node start = findClosestNode(from, from);
        Node finish = mNodeGraph.getNode(to);

        if (finish == null) {
            finish = findClosestNode(to, from);
        }

        if (finish == null || start == null) {
            return null;
        }

        SmoothablePath graphPath = new SmoothablePath();
        mPathFinder.searchNodePath(start, finish, mHeuristic, graphPath);
        mPathsmoother.smooth(graphPath);
        return graphPath;
    }

    public void findPathAsync(final Vector2 position, final Vector2 destination, final LinkedList<Vector2> list, final FinderCallback callback) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                Point start = new Point((int) position.x / DEFAULT_CELL_SIZE, (int) position.y / DEFAULT_CELL_SIZE);
                Point finish = new Point((int) destination.x / DEFAULT_CELL_SIZE, (int) destination.y / DEFAULT_CELL_SIZE);
                final GraphPath<Node> graphPath = search(start, finish);

                Utils.post(new Runnable() {
                    @Override
                    public void run() {
                        list.clear();
                        if (graphPath == null) {
                            return;
                        }

                        for (Node node : graphPath) {
                            Point point = node.getPosition();
                            list.add(new Vector2(point.x * DEFAULT_CELL_SIZE + DEFAULT_CELL_SIZE / 2, point.y * DEFAULT_CELL_SIZE + DEFAULT_CELL_SIZE / 2)); //centering
                        }
                        callback.pathFound(list);
                    }
                });
            }
        });
    }

    private Node findClosestNode(Point point, Point closestTo) {
        Node closest = mNodeGraph.getNode(point);
        if (closest != null) {
            return closest;
        }

        int terrainSize = mGameScreen.getTerrain().getSize();
        Point p = new Point();
        List<Node> closestNodes = new ArrayList<Node>();

        for (int delta = 0; delta < terrainSize; delta++) {
            for (int deltaX = -delta; deltaX <= delta; deltaX++) {
                p.x = point.x + deltaX;
                for (int deltaY = -delta; deltaY <= delta; deltaY++) {
                    p.y = point.y + deltaY;

                    if (!point.equals(p)) {
                        if (mNodeGraph.getNode(p) != null) {
                            closestNodes.add(mNodeGraph.getNode(p));
                        }
                    }
                }
            }

            float MIN_DIST = Float.MAX_VALUE;
            for (Node node : closestNodes) {
                float distance = node.getPosition().dst(closestTo);
                if (distance < MIN_DIST) {
                    MIN_DIST = distance;
                    closest = node;
                }
            }

            if (closest != null) {
                return closest;
            }
        }
        return null;
    }

    private void clearNodesForGameObject(GameObject gameObject) {
        Rect bounds = gameObject.getBounds();
        for (int y = (int) (bounds.y / DEFAULT_CELL_SIZE); y < bounds.y / DEFAULT_CELL_SIZE + bounds.height / DEFAULT_CELL_SIZE; y++) {
            for (int x = (int) (bounds.x / DEFAULT_CELL_SIZE); x < bounds.x / DEFAULT_CELL_SIZE + bounds.width / DEFAULT_CELL_SIZE; x++) {
                mNodeGraph.addNode(Node.obtain(mNodeGraph, x, y));
            }
        }
    }

    private final GameObjectManager.Listener<Building> mBuildingsListener = new GameObjectManager.Listener<Building>() {
        @Override
        public void added(GameObjectManager manager, Building gameObject) {
            Rect bounds = gameObject.getBounds();
            for (int y = (int) (bounds.y / DEFAULT_CELL_SIZE); y < bounds.y / DEFAULT_CELL_SIZE + bounds.height / DEFAULT_CELL_SIZE; y++) {
                for (int x = (int) (bounds.x / DEFAULT_CELL_SIZE); x < bounds.x / DEFAULT_CELL_SIZE + bounds.width / DEFAULT_CELL_SIZE; x++) {
                    mNodeGraph.removeNode(x, y);
                }
            }
        }

        @Override
        public void removed(GameObjectManager manager, Building gameObject) {
            clearNodesForGameObject(gameObject);
        }
    };

    private final GameObjectManager.Listener<Resource> mResourcesListener = new GameObjectManager.Listener<Resource>() {
        @Override
        public void added(GameObjectManager manager, Resource gameObject) {
            Rect bounds = gameObject.getBounds();
            for (int y = (int) (bounds.y / DEFAULT_CELL_SIZE); y < bounds.y / DEFAULT_CELL_SIZE + bounds.height / DEFAULT_CELL_SIZE; y++) {
                for (int x = (int) (bounds.x / DEFAULT_CELL_SIZE); x < bounds.x / DEFAULT_CELL_SIZE + bounds.width / DEFAULT_CELL_SIZE; x++) {
                    mNodeGraph.addNode(Node.obtain(mNodeGraph, x, y, 10 - gameObject.getPassingSpeedMultiplier() * 10));
                }
            }
        }

        @Override
        public void removed(GameObjectManager manager, Resource gameObject) {
            clearNodesForGameObject(gameObject);
        }
    };

    private final Heuristic<Node> mHeuristic = new Heuristic<Node>() {
        @Override
        public float estimate(Node node, Node endNode) {
            return Math.abs((node.getPosition().x - endNode.getPosition().x) * (node.getPosition().y - endNode.getPosition().y));
        }
    };

    public interface FinderCallback {
        void pathFound(LinkedList<Vector2> path);
    }
}
