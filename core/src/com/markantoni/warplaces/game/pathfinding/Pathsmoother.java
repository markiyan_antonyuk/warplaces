package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.PathSmoother;
import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.markantoni.warplaces.utils.Point;

/**
 * Created by mark on 04.03.16.
 */
class Pathsmoother {
    private final NodeGraph mNodeGraph;
    private final PathSmoother<Node, Point> mPathSmoother;

    Pathsmoother(NodeGraph nodeGraph) {
        mNodeGraph = nodeGraph;
        mPathSmoother = new PathSmoother<Node, Point>(mRaycastCollisionDetector);
    }

    void smooth(SmoothablePath smoothablePath) {
        mPathSmoother.smoothPath(smoothablePath);
    }

    private final RaycastCollisionDetector<Point> mRaycastCollisionDetector = new RaycastCollisionDetector<Point>() {
        /*
         * copy paste from
         * https://github.com/libgdx/gdx-ai/blob/30dd388b8b94cd99f1ade12533177b0ea7bb07b8/tests/src/com/badlogic/gdx/ai/tests/pfa/tests/tiled/TiledRaycastCollisionDetector.java
         */
        @Override
        public boolean collides(Ray<Point> ray) {
            int x0 = ray.start.x;
            int y0 = ray.start.y;
            int x1 = ray.end.x;
            int y1 = ray.end.y;

            int tmp;
            boolean steep = Math.abs(y1 - y0) > Math.abs(x1 - x0);
            if (steep) {
                // Swap x0 and y0
                tmp = x0;
                x0 = y0;
                y0 = tmp;
                // Swap x1 and y1
                tmp = x1;
                x1 = y1;
                y1 = tmp;
            }
            if (x0 > x1) {
                // Swap x0 and x1
                tmp = x0;
                x0 = x1;
                x1 = tmp;
                // Swap y0 and y1
                tmp = y0;
                y0 = y1;
                y1 = tmp;
            }

            int deltax = x1 - x0;
            int deltay = Math.abs(y1 - y0);
            int error = 0;
            int y = y0;
            int ystep = (y0 < y1 ? 1 : -1);
            for (int x = x0; x <= x1; x++) {
                Node node = steep ? mNodeGraph.getNode(y, x) : mNodeGraph.getNode(x, y);
                if (node != null) {
                    return true;
                }
                error += deltay;
                if (error + error >= deltax) {
                    y += ystep;
                    error -= deltax;
                }
            }

            return false;
        }

        @Override
        public boolean findCollision(Collision outputCollision, Ray inputRay) {
            throw new UnsupportedOperationException();
        }
    };
}
