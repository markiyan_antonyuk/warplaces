package com.markantoni.warplaces.game.pathfinding;

import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.SmoothableGraphPath;
import com.markantoni.warplaces.utils.Point;

/**
 * Created by mark on 04.03.16.
 */
class SmoothablePath extends DefaultGraphPath<Node> implements SmoothableGraphPath<Node, Point> {
    @Override
    public Point getNodePosition(int index) {
        return nodes.get(index).getPosition();
    }

    @Override
    public void swapNodes(int index1, int index2) {
        Node node1 = nodes.get(index1);
        Node node2 = nodes.get(index2);
        nodes.set(index1, node2);
        nodes.set(index2, node1);
    }

    @Override
    public void truncatePath(int newLength) {
        nodes.truncate(newLength);
    }
}
