package com.markantoni.warplaces.game.redactor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.WarPlaces;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.screens.menu.MenuScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

import java.io.File;

/**
 * Created by mark on 10.02.16.
 */
public class RedactorChooserScreen extends MenuScreen {
    private ButtonGroup<VisTextButton> mButtonGroup = new ButtonGroup<VisTextButton>();
    private Table mTerrainsTable;
    private Table mPreviewTable;

    @Override
    protected void initUI(Stage stage) {
        setTitle(Strings.get("select_terrain"));
        mPreviewTable = generateTable();
        stage.addActor(mPreviewTable);

        Table table = generateTable();
        getStage().addActor(table);

        mTerrainsTable = new Table();
        mTerrainsTable.defaults().prefSize(SizeUnit.uiX(4), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f)).align(Align.center);
        VisScrollPane scrollPane = new VisScrollPane(mTerrainsTable);
        table.center().add(scrollPane).pad(SizeUnit.uiUnit(1));

        Table buttonsTable = generateTable();
        VisTextButton open = new VisTextButton(Strings.get("open"));
        VisTextButton delete = new VisTextButton(Strings.get("delete"));
        VisTextButton back = new VisTextButton(Strings.get("back"));

        buttonsTable.bottom();
        buttonsTable.defaults().prefSize(SizeUnit.uiX(2), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));
        buttonsTable.add(open, delete, back);

        getStage().addActor(buttonsTable);

        generateContent();

        open.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (mButtonGroup.getChecked() != null) {
                    ScreenStack.open(RedactorScreen.class, new Bundle().put(AbstractGameScreen.FILE,
                            new File(WarPlaces.getGameDir(), mButtonGroup.getChecked().getText() + Assets.Extensions.TERRAIN)));
                }
            }
        });

        delete.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (mButtonGroup.getChecked() != null) {
                    VisTextButton button = mButtonGroup.getChecked();
                    String name = button.getText().toString();
                    new File(WarPlaces.getGameDir(), name).delete();
                    new File(WarPlaces.getGameDir(), name + Assets.Extensions.TERRAIN).delete();

                    ((Texture) button.getUserObject()).dispose();
                    generateContent();
                }
            }
        });

        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                back();
            }
        });
    }

    private void generateContent() {
        mTerrainsTable.clear();
        mButtonGroup.clear();

        File[] files = Utils.listGameFiles(Assets.Extensions.TERRAIN);
        if (files != null) {
            for (File file : files) {
                String name = file.getName().replaceAll(Assets.Extensions.TERRAIN, "");
                VisTextButton button = new VisTextButton(name);
                Texture texture = Utils.loadTexture(name);
                button.setUserObject(texture);
                button.addListener(mButtonListener);

                mTerrainsTable.add(button).row();
                mButtonGroup.add(button);
            }

            if (mButtonGroup.getButtons().size > 0) {
                Button button = mButtonGroup.getButtons().first();

                InputEvent down = new InputEvent();
                down.setType(InputEvent.Type.touchDown);
                InputEvent up = new InputEvent();
                up.setType(InputEvent.Type.touchUp);

                button.fire(down);
                button.fire(up);
            }
        }
    }

    private void updateBackground() {
        if (mButtonGroup.getChecked() != null) {
            Drawable drawable = new SpriteDrawable(new Sprite((Texture) mButtonGroup.getChecked().getUserObject()));
            mPreviewTable.setBackground(drawable);
        } else {
            mPreviewTable.setBackground((Drawable) null);
        }
    }

    private final ClickListener mButtonListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            updateBackground();
        }
    };

    @Override
    public void dispose() {
        super.dispose();
        for (Actor actor : mTerrainsTable.getChildren()) {
            if (actor.getUserObject() != null && actor.getUserObject() instanceof Texture) {
                ((Texture) actor.getUserObject()).dispose();
            }
        }
    }
}
