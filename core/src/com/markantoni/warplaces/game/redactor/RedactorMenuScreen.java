package com.markantoni.warplaces.game.redactor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisRadioButton;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.terrain.TerrainSize;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.screens.menu.MenuScreen;
import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 18.01.16.
 */
public class RedactorMenuScreen extends MenuScreen {
    @Override
    protected void initUI(Stage stage) {
        setTitle(Strings.get("terrain_redactor"));

        Table table = generateTable();
        table.defaults().prefSize(SizeUnit.uiX(2f), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));

        VisLabel chooseSizeLabel = new VisLabel(Strings.get("choose_terrain_size"));
        chooseSizeLabel.setAlignment(Align.bottom);

        VisRadioButton debug = new VisRadioButton(Strings.get("debug"));
        debug.setUserObject(TerrainSize.DEBUG);
        debug.align(Align.left);

        VisRadioButton small = new VisRadioButton(Strings.get("small"));
        small.setChecked(true);
        small.setUserObject(TerrainSize.SMALL);
        small.align(Align.left);

        VisRadioButton medium = new VisRadioButton(Strings.get("medium"));
        medium.setUserObject(TerrainSize.MEDIUM);
        medium.align(Align.left);

        VisRadioButton big = new VisRadioButton(Strings.get("big"));
        big.setUserObject(TerrainSize.BIG);
        big.align(Align.left);

        VisRadioButton enormous = new VisRadioButton(Strings.get("enormous"));
        enormous.setUserObject(TerrainSize.ENORMOUS);
        enormous.align(Align.left);

        VisTextButton open = new VisTextButton(Strings.get("open"));
        final ButtonGroup<TextButton> buttonGroup = new ButtonGroup<TextButton>(debug, small, medium, big, enormous);

        table.add(chooseSizeLabel).colspan(2).row();
        table.add(small).align(Align.center);
        table.add(medium).align(Align.center).row();
        table.add(big).align(Align.center);
        table.add(enormous).align(Align.center).row();
        table.add(debug).row();

        VisTextButton done = new VisTextButton(Strings.get("done"));
        Table doneTable = generateTable();
        doneTable.defaults().prefSize(SizeUnit.uiX(2), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));
        doneTable.bottom().right();
        doneTable.add(open);
        doneTable.add(done);

        stage.addActor(table);
        stage.addActor(doneTable);

        open.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(RedactorChooserScreen.class);
            }
        });

        done.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Bundle bundle = new Bundle();
                bundle.put(AbstractGameScreen.SIZE, buttonGroup.getChecked().getUserObject());
                ScreenStack.open(RedactorScreen.class, bundle);
            }
        });
    }
}