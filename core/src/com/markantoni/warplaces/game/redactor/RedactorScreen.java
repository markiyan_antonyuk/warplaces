package com.markantoni.warplaces.game.redactor;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.ColorDrawable;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 20.01.16.
 */
public class RedactorScreen extends AbstractGameScreen {
    private Terrain.Tile mSelectionTile = Terrain.GRASS;

    @Override
    protected void initUI(Stage stage) {
        float width = SizeUnit.uiX(1f);
        float height = SizeUnit.uiY(1f);
        Table table = new Table();
        table.align(Align.bottomLeft);
        table.background(new ColorDrawable(Color.BROWN));

        SpriteDrawable grassDrawable = new SpriteDrawable(Assets.Terrain.GRASS.SPRITE);
        VisImageButton grass = new VisImageButton(grassDrawable);

        SpriteDrawable waterDrawable = new SpriteDrawable(Assets.Terrain.WATER.SPRITE);
        VisImageButton water = new VisImageButton(waterDrawable);

        float padding = SizeUnit.uiUnit(.1f);
        table.defaults().size(width, height).pad(padding);
        table.setPosition(1, 1);
        table.setSize(width * 2 + padding * 4, height + padding * 2);
        table.add(grass);
        table.add(water);
        stage.addActor(table);

        water.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mSelectionTile = Terrain.WATER;
            }
        });

        grass.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mSelectionTile = Terrain.GRASS;
            }
        });

        new ButtonGroup<Button>(water, grass);
    }

    @Override
    protected void back() {
        ScreenStack.open(RedactorSettingsScreen.class, new Bundle().put(Terrain.TERRAIN, getTerrain()));
    }

    @Override
    protected void create() {
        super.create();
        addInputSource(mInputAdapter);
    }

    @Override
    protected void renderGame(Renderer renderer) {
    }

    private final InputAdapter mInputAdapter = new InputAdapter() {
        //todo merge into AbstractGameScreen
        private boolean mIsTouchedDown = false;
        private Vector2 mLastTouch = new Vector2();
        private Vector2 mCurrentTouch = new Vector2();

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if (getCamera().isPerformingAction()) {
                return false;
            }

            mIsTouchedDown = true;
            mLastTouch.set(screenX, screenY);

            if (!Settings.isInTouchmode()) {
                getTerrain().setTile(getCamera().unproject(screenX, screenY), mSelectionTile);
            }
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (getCamera().isPerformingAction() || (pointer > 0 && Utils.fingersTouched(2))) { // FIXME: 02.02.16 putting tiles when zoom (1 finger left)
                return false;
            }

            mCurrentTouch.set(screenX, screenY);

            if (mIsTouchedDown && !mLastTouch.equals(mCurrentTouch)) {
                getTerrain().setTile(getCamera().unproject(screenX, screenY), mSelectionTile);
            }

            mLastTouch.set(mCurrentTouch);
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            mIsTouchedDown = false;
            mLastTouch.setZero();
            mCurrentTouch.setZero();
            return false;
        }
    };
}
