package com.markantoni.warplaces.game.redactor;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.util.dialog.InputDialogAdapter;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.screens.menu.MainMenuScreen;
import com.markantoni.warplaces.screens.menu.MenuScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by Mark on 24.01.2016.
 */
public class RedactorSettingsScreen extends MenuScreen {
    private Terrain mTerrain;

    @Override
    protected void initUI(final Stage stage) {
        setTitle(Strings.get("terrain_redactor"));
        final Table table = generateTable();

        table.defaults().prefSize(SizeUnit.uiX(4), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));
        TextButton resumeButton = new VisTextButton(Strings.get("resume"));
        TextButton saveButton = new VisTextButton(Strings.get("save"));
        TextButton mainMenuButton = new VisTextButton(Strings.get("main_menu"));

        table.add(resumeButton).row();
        table.add(saveButton).row();
        table.add(mainMenuButton);

        mTerrain = getBundle().get(Terrain.TERRAIN, null);
        SpriteDrawable spriteDrawable = new SpriteDrawable(new Sprite(mTerrain.getTerrainPreview()));
        table.setBackground(spriteDrawable);

        stage.addActor(table);

        resumeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                back();
            }
        });

        saveButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Dialogs.showInputDialog(getStage(), Strings.get("save"), Strings.get("enter_name"), true, mInputDialogAdapter);
            }
        });

        mainMenuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(MainMenuScreen.class);
            }
        });
    }

    private final InputDialogAdapter mInputDialogAdapter = new InputDialogAdapter() {
        @Override
        public void finished(String input) {
            Utils.saveJson(input, Assets.Extensions.TERRAIN, mTerrain.toJSON());
            Utils.saveTexture(input, mTerrain.getTerrainPreview());
        }
    };
}
