package com.markantoni.warplaces.game.resources;

import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.utils.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mark on 02.03.16.
 */
public class ResourceSet {
    public final Map<Class<? extends Item>, Integer> mResources = new HashMap<Class<? extends Item>, Integer>();

    public ResourceSet(Pair<Class<? extends Item>, Integer>... resources) {
        add(resources);
    }

    public void add(ResourceSet resourceSet) {
        for (Map.Entry<Class<? extends Item>, Integer> entry : resourceSet.mResources.entrySet()) {
            add(entry.getKey(), entry.getValue());
        }
    }

    public void add(Pair<Class<? extends Item>, Integer>... resources) {
        for (Pair<Class<? extends Item>, Integer> pair : resources) {
            add(pair.first, pair.second);
        }
    }

    public void add(Class<? extends Item> type) {
        add(type, 1);
    }

    public void add(Class<? extends Item> type, int count) {
        mResources.put(type, get(type) + count);
    }

    public int get(Class<? extends Item> type) {
        if (!mResources.containsKey(type)) {
            return 0;
        }
        return mResources.get(type);
    }

    public void remove(Class<? extends Item> type) {
        remove(type, 1);
    }

    public void remove(Class<? extends Item> type, int count) {
        int oldCount = get(type);
        if (oldCount - count <= 0) {
            mResources.remove(type);
            return;
        }
        mResources.put(type, oldCount - count);
    }

    public Map<Class<? extends Item>, Integer> getResources() {
        return mResources;
    }

    public boolean isEmpty() {
        return mResources.isEmpty();
    }

    public void clearAll() {
        mResources.clear();
    }

    public int getResourceTypesCount() {
        return mResources.keySet().size();
    }

    public boolean isEnough(ResourceSet resourceSet) {
        for (Map.Entry<Class<? extends Item>, Integer> entry : resourceSet.mResources.entrySet()) {
            if (get(entry.getKey()) < entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    public int containsPercents(ResourceSet resourceSet) {
        int current = 0;
        int other = 0;

        for (Map.Entry<Class<? extends Item>, Integer> entry : resourceSet.mResources.entrySet()) {
            other += entry.getValue();
            current += get(entry.getKey());
        }

        return (int) ((float) current / other * 100);
    }
}
