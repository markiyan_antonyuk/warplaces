package com.markantoni.warplaces.game.resources;

import com.markantoni.warplaces.game.buildings.Building;
import com.markantoni.warplaces.game.buildings.BuildingsManager;
import com.markantoni.warplaces.game.buildings.storage.Storage;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObjectManager;

import java.util.List;

/**
 * Created by mark on 24.02.16.
 * Game resources which are stored only in storages
 */
public class Resources {
    private final ResourceSet mResourceSet;
    private final BuildingsManager mBuildingsManager;

    public Resources(Nation nation) {
        mResourceSet = new ResourceSet();
        mBuildingsManager = nation.getBuildingsManager();
        mBuildingsManager.addListener(mBuildingsListener);
    }

    public boolean isEnough(ResourceSet resourceSet) {
        return mResourceSet.isEnough(resourceSet);
    }

    public ResourceSet getResourceSet() {
        return mResourceSet;
    }

    private final Container.Observer mObserver = new Container.Observer() {
        @Override
        public void changed(Container container) {
            handleChanged();
        }
    };

    private final GameObjectManager.Listener<Building> mBuildingsListener = new GameObjectManager.Listener<Building>() {
        @Override
        public void added(final GameObjectManager manager, Building gameObject) {
            if (gameObject instanceof StorageBuilding) {
                ((StorageBuilding) gameObject).getStorage().addObserver(mObserver);
            }
        }

        @Override
        public void removed(GameObjectManager manager, Building gameObject) {
            if (gameObject instanceof StorageBuilding) {
                handleChanged();
                ((StorageBuilding) gameObject).getStorage().removeObserver(mObserver);
            }
        }
    };

    private void handleChanged() {
        mResourceSet.clearAll();
        for (Building building : mBuildingsManager.getAll()) {
            if (building instanceof StorageBuilding) {
                Storage storage = ((StorageBuilding) building).getStorage();
                addResources(storage);
            }
        }
    }

    private void addResources(Storage storage) {
        for (int i = 0; i < storage.getSize(); i++) {
            List<Item> list = storage.getItems(i);
            if (list != null) {
                Class<? extends Item> item = list.get(0).getClass();
                mResourceSet.add(item, list.size());
            }
        }
    }
}
