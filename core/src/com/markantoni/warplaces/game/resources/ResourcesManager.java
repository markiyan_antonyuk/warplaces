package com.markantoni.warplaces.game.resources;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.Updatable;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.resources.objects.Resource;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Log;
import com.markantoni.warplaces.utils.RenderableSelected;
import com.markantoni.warplaces.utils.Renderer;

/**
 * Created by Mark on 06.02.2016.
 */
public class ResourcesManager extends GameObjectManager<Resource> implements Updatable, RenderableSelected {
    private final World mWorld;

    public ResourcesManager(GameScreen gameScreen, World world) {
        super(gameScreen, null);
        mWorld = world;
    }

    public Resource create(Class<? extends Resource> type, int cellX, int cellY) {
        try {
            Resource resource = type.getConstructor(GameScreen.class, World.class, int.class, int.class)
                    .newInstance(getGameScreen(), mWorld, cellX, cellY);
            resource.setResourcesManager(this);
            add(resource);
            return resource;
        } catch (Exception e) {
            Log.d(ResourcesManager.class, "can't create " + type + " " + e.getCause().toString());
        }
        return null;
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        super.update(deltaTime, renderManager);
        for (Resource resource : mAll) {
            resource.update(deltaTime);
        }
    }

    @Override
    public void render(Renderer renderer) {
        for (Resource resource : mAll) {
            resource.render(renderer);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        for (Resource resource : mSelected) {
            resource.renderSelected(renderer);
        }
    }

    @Override
    protected boolean handleAction(float x, float y) {
        return false;
    }
}
