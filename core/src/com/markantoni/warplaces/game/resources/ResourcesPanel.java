package com.markantoni.warplaces.game.resources;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 24.02.16.
 */
public class ResourcesPanel extends UiPanel {
    private final Map<Class<? extends Item>, Item> mItems = new HashMap<Class<? extends Item>, Item>();
    private final List<VisLabel> mLabels = new ArrayList<VisLabel>();
    private final Resources mResources;
    private final Nation mNation;
    private Table mTable;

    public ResourcesPanel(GameScreen gameScreen, Nation nation) {
        super(gameScreen, ResourcesPanel.class.getName(), false, true, 0, true, true);
        mNation = nation;
        mResources = nation.getResources();
    }

    @Override
    protected Table create() {
        setTitle(Strings.get("resources"));
        mTable = new Table();
        mTable.defaults().minSize(SizeUnit.uiX(.75f), SizeUnit.uiY(.75f)).pad(SizeUnit.uiUnit(.1f));
        return mTable;
    }

    public void update() {
        if (mLabels.size() != mResources.getResourceSet().getResourceTypesCount()) {
            rebuildTable();
        }

        int i = 0;
        for (Map.Entry<Class<? extends Item>, Integer> entry : mResources.getResourceSet().getResources().entrySet()) {
            VisLabel visLabel = mLabels.get(i++);
            visLabel.setText(entry.getValue() + "");

            Label.LabelStyle labelStyle = new Label.LabelStyle(visLabel.getStyle().font, visLabel.getStyle().fontColor);
            labelStyle.background = new SpriteDrawable(mItems.get(entry.getKey()).getPreviewSprite());
            visLabel.setStyle(labelStyle);
        }
    }

    private void rebuildTable() {
        mTable.clearChildren();
        mLabels.clear();

        Map<Class<? extends Item>, Integer> resources = mResources.getResourceSet().getResources();
        for (Map.Entry<Class<? extends Item>, Integer> entry : resources.entrySet()) {
            if (!mItems.containsKey(entry.getKey())) {
                try {
                    mItems.put(entry.getKey(), entry.getKey().getConstructor(GameScreen.class).newInstance(mNation.getGameScreen()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            VisLabel visLabel = new VisLabel("", Align.center);
            visLabel.setFontScale(1.5f);
            mLabels.add(visLabel);
            mTable.add(visLabel).row();
        }

        getUi().pack();
    }
}
