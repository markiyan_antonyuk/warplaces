package com.markantoni.warplaces.game.resources.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.items.resources.WheatItem;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 07.02.2016.
 */
public class Farm extends Resource {
    public Farm(GameScreen gameScreen, World world, int cellX, int cellY) {
        super(gameScreen, world, Strings.get("farm"), Assets.Resources.FARM.SPRITE, cellX, cellY, 1, 25);
    }

    @Override
    public Color getColor() {
        return Color.YELLOW;
    }

    @Override
    protected ResourceItem getResourceItem() {
        return (ResourceItem) getItemsManager().createItem(WheatItem.class);
    }

    @Override
    public float getPassingSpeedMultiplier() {
        return .9f;
    }
}
