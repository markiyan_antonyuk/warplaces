package com.markantoni.warplaces.game.resources.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.items.resources.WoodItem;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 19.02.16.
 */
public class Forest extends Resource {
    public Forest(GameScreen gameScreen, World world, int cellX, int cellY) {
        super(gameScreen, world, Strings.get("forest"), Assets.Resources.FOREST.SPRITE, cellX, cellY, 2, 50);
    }

    @Override
    public Color getColor() {
        return Color.GREEN;
    }

    @Override
    protected ResourceItem getResourceItem() {
        return (ResourceItem) getItemsManager().createItem(WoodItem.class);
    }

    @Override
    public float getPassingSpeedMultiplier() {
        return .5f;
    }
}
