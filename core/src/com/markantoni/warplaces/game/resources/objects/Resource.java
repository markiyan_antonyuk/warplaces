package com.markantoni.warplaces.game.resources.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.resources.ResourcesManager;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by Mark on 06.02.2016.
 */
public abstract class Resource extends GameObject {
    private final float CAPACITY_PER_ITEM;
    private final int ITEMS_IN_RESOURCE;
    private final Sprite mSprite;
    private final Rect mTempRect = Rect.empty();
    private final String mName;

    private ResourcesManager mResourcesManager;
    private int mCurrentItems;
    private float mCapacity;

    public Resource(GameScreen gameScreen, World world, String name, Sprite sprite, int cellX, int cellY, float capacityPerItem, int itemsInResource) {
        super(gameScreen, null);
        mName = name;
        mSprite = sprite;
        setPhysicObject(new ResourceObject(world, cellX * AbstractGameScreen.DEFAULT_TILE_SIZE.width, cellY * AbstractGameScreen.DEFAULT_TILE_SIZE.height,
                AbstractGameScreen.DEFAULT_TILE_SIZE.width, AbstractGameScreen.DEFAULT_TILE_SIZE.height));
        mBounds.setSize(AbstractGameScreen.DEFAULT_TILE_SIZE.width, AbstractGameScreen.DEFAULT_TILE_SIZE.height);
        mBounds.set(getPhysicObject().getBody().getPosition());

        CAPACITY_PER_ITEM = capacityPerItem;
        ITEMS_IN_RESOURCE = mCurrentItems = itemsInResource;
    }

    @Override
    public final String getName() {
        return mName;
    }

    @Override
    public GameObjectManager getManager() {
        return getResourcesManager();
    }

    /**
     * @return false if unit has no more space in inventory or resource was destroyed
     */
    public boolean handleCollect(Unit unit, float speed) {
        mCapacity += speed;

        if (mCapacity > CAPACITY_PER_ITEM) {
            mCapacity -= CAPACITY_PER_ITEM;
            mCurrentItems--;

            if (mCurrentItems >= 0) {
                ResourceItem item = getResourceItem();
                if (!unit.getInventory().addItem(item)) {
                    getItemsManager().createItem(item, Utils.randomRange(getBounds().x, getBounds().width),
                            Utils.randomRange(getBounds().y, getBounds().height));
                    return false;
                } else {
                    return true;
                }
            } else {
                mResourcesManager.remove(this);
                return false;
            }
        }

        return true;
    }

    public void setResourcesManager(ResourcesManager resourcesManager) {
        mResourcesManager = resourcesManager;
    }

    protected abstract ResourceItem getResourceItem();

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }

    public abstract float getPassingSpeedMultiplier();

    @Override
    public Vector2 getIsometricPosition() {
        return mBounds.getPosition(mPosition);
    }

    @Override
    public Rect getBounds() {
        return mBounds;
    }

    @Override
    public void update(float deltaTime) {
        mBounds.set(getPhysicObject().getBody().getPosition());
    }

    @Override
    public void render(Renderer renderer) {
        if (renderer.needToRender(mBounds)) {
            renderer.renderSprite(mSprite, mBounds);
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        if (!renderer.needToRender(mBounds)) {
            return;
        }

        mTempRect.set(mBounds);
        int divider = SizeUnit.unit(.5f);
        mTempRect.x -= mTempRect.width / divider;
        mTempRect.width += mTempRect.width / divider * 2;
        mTempRect.y -= mTempRect.height / divider;
        mTempRect.height += mTempRect.height / divider * 2;
        renderer.renderEllipse(mIsMainSelected ? Color.GOLD : Color.RED, mTempRect.x, mTempRect.y, mTempRect.width, mTempRect.height);
    }

    @Override
    public String type() {
        return Strings.get("resource");
    }

    @Override
    public String capacity() {
        return mCurrentItems + "/" + ITEMS_IN_RESOURCE;
    }
}
