package com.markantoni.warplaces.game.resources.objects;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.objects.PhysicObject;
import com.markantoni.warplaces.utils.PhysicUtils;

/**
 * Created by Mark on 06.02.2016.
 */
public class ResourceObject extends PhysicObject {
    ResourceObject(World world, float x, float y, float width, float height) {
        mBody = PhysicUtils.createStaticBody(world, x, y, PhysicUtils.rectWithoutEdges(width / 4 * 3, height / 4 * 3, 3, width / 8, height / 8));
    }
}
