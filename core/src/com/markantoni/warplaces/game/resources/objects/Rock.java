package com.markantoni.warplaces.game.resources.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.items.resources.StoneItem;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 24.02.16.
 */
public class Rock extends Resource {
    public Rock(GameScreen gameScreen, World world, int cellX, int cellY) {
        super(gameScreen, world, Strings.get("rock"), Assets.Resources.ROCK.SPRITE, cellX, cellY, 3, 150);
    }

    @Override
    public Color getColor() {
        return Color.LIGHT_GRAY;
    }

    @Override
    protected ResourceItem getResourceItem() {
        return (ResourceItem) getItemsManager().createItem(StoneItem.class);
    }

    @Override
    public float getPassingSpeedMultiplier() {
        return .1f;
    }
}
