package com.markantoni.warplaces.game.sandbox;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.DebugPanel;
import com.markantoni.warplaces.game.items.resources.WoodItem;
import com.markantoni.warplaces.game.items.tools.twohand.Axe;
import com.markantoni.warplaces.game.items.tools.twohand.Hammer;
import com.markantoni.warplaces.game.items.tools.twohand.Pickaxe;
import com.markantoni.warplaces.game.minimap.Minimap;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObjectFinder;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.items.ItemsManager;
import com.markantoni.warplaces.game.items.resources.WheatItem;
import com.markantoni.warplaces.game.items.tools.twohand.Scythe;
import com.markantoni.warplaces.game.nations.PlayerNation;
import com.markantoni.warplaces.game.pathfinding.Pathfinder;
import com.markantoni.warplaces.game.resources.ResourcesManager;
import com.markantoni.warplaces.game.resources.objects.Rock;
import com.markantoni.warplaces.game.resources.objects.Farm;
import com.markantoni.warplaces.game.resources.objects.Forest;
import com.markantoni.warplaces.game.terrain.Terrain;
import com.markantoni.warplaces.game.ui.StickersManager;
import com.markantoni.warplaces.utils.PhysicUtils;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 25.01.16.
 */
//todo merge into AbstractGameScreen
public class GameScreen extends AbstractGameScreen {
    public static final int SELECTION_BUTTON = Input.Buttons.LEFT;
    public static final int ACTION_BUTTON = Input.Buttons.RIGHT;

    private final List<Nation> mAllNations = new ArrayList<Nation>();
    private final StickersManager mStickersManager = new StickersManager(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    private World mWorld;
    private Box2DDebugRenderer mBox2DDebugRenderer;
    private ResourcesManager mResourcesManager;
    private PlayerNation mPlayerNation;
    private ItemsManager mItemsManager;
    private RenderManager mRenderManager;
    private GameObjectFinder mGameObjectFinder;
    private Pathfinder mPathfinder;
    private Minimap mMinimap;
    private DebugPanel mDebugPanel;

    @Override
    protected void initUI(Stage stage) {
        mDebugPanel = new DebugPanel(this);
        mStickersManager.setLayoutCallback(mLayoutStickersCallback);
    }

    @Override
    protected void create() {
        super.create();
        mWorld = new World(new Vector2(0, 0), false);
        PhysicUtils.registerContactListener(mWorld);
        PhysicUtils.createTerrainBody(mWorld, getTerrain());

        GestureDetector gestureDetector = new GestureDetector(mGestureAdapter);
        gestureDetector.setLongPressSeconds(0.5f);
        addInputSource(gestureDetector);

        getTerrain().setWorld(mWorld);
        mMinimap = new Minimap(this);
        mGameObjectFinder = new GameObjectFinder();
        mRenderManager = new RenderManager();
        mItemsManager = new ItemsManager(this, mWorld);
        mResourcesManager = new ResourcesManager(this, mWorld);
        mPlayerNation = new PlayerNation(this, mWorld, Color.RED);
        addInputSource(mPlayerNation.getChooserRect().getInputSource());
        mAllNations.add(mPlayerNation);
        mBox2DDebugRenderer = new Box2DDebugRenderer();

        /************************/
        mItemsManager.createItem(WheatItem.class, SizeUnit.x(1), SizeUnit.y(1));
        mItemsManager.createItem(WheatItem.class, SizeUnit.x(1), SizeUnit.y(1.25f));
        mItemsManager.createItem(Pickaxe.class, SizeUnit.x(1.5f), SizeUnit.y(1.25f));
        mItemsManager.createItem(Pickaxe.class, SizeUnit.x(1.2f), SizeUnit.y(1.25f));
        mItemsManager.createItem(Axe.class, SizeUnit.x(1), SizeUnit.y(1.5f));
        mItemsManager.createItem(WoodItem.class, SizeUnit.x(1), SizeUnit.y(2f));
        mItemsManager.createItem(Scythe.class, SizeUnit.x(1), SizeUnit.y(2.5f));
        mItemsManager.createItem(Hammer.class, SizeUnit.x(2), SizeUnit.y(1));
        getTerrain().setTileCell(3, 6, Terrain.WATER);
        getTerrain().setTileCell(4, 6, Terrain.WATER);
        getTerrain().setTileCell(5, 6, Terrain.WATER);
        getTerrain().setTileCell(3, 7, Terrain.WATER);
        getTerrain().setTileCell(4, 7, Terrain.WATER);
        getTerrain().setTileCell(5, 7, Terrain.WATER);
        getTerrain().setTileCell(6, 7, Terrain.WATER);
        getTerrain().setTileCell(5, 8, Terrain.WATER);
        getTerrain().setTileCell(6, 8, Terrain.WATER);
        getTerrain().setTileCell(6, 9, Terrain.WATER);
        getTerrain().setTileCell(6, 10, Terrain.WATER);
        getTerrain().setTileCell(6, 11, Terrain.WATER);
        getTerrain().setTileCell(6, 12, Terrain.WATER);
        getTerrain().setTileCell(7, 8, Terrain.WATER);
        getTerrain().setTileCell(8, 8, Terrain.WATER);
        getTerrain().setTileCell(9, 8, Terrain.WATER);
        getTerrain().setTileCell(9, 9, Terrain.WATER);
        getTerrain().setTileCell(9, 10, Terrain.WATER);
        getTerrain().setTileCell(10, 8, Terrain.WATER);
        getTerrain().setTileCell(11, 8, Terrain.WATER);
        getTerrain().setTileCell(12, 8, Terrain.WATER);
        mResourcesManager.create(Farm.class, 9, 3);
        mResourcesManager.create(Farm.class, 10, 3);
        mResourcesManager.create(Farm.class, 10, 2);
        mResourcesManager.create(Farm.class, 10, 1);
        mResourcesManager.create(Forest.class, 12, 1);
        mResourcesManager.create(Forest.class, 12, 2);
        mResourcesManager.create(Forest.class, 12, 3);
        mResourcesManager.create(Forest.class, 12, 4);
        mResourcesManager.create(Forest.class, 12, 5);
        mResourcesManager.create(Forest.class, 12, 6);
        mResourcesManager.create(Forest.class, 12, 7);
        mResourcesManager.create(Forest.class, 12, 7);
        mResourcesManager.create(Rock.class, 8, 1);
        mResourcesManager.create(Rock.class, 7, 1);
        /************************/

        mPathfinder = new Pathfinder(this);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        mRenderManager.clear();
        mGameObjectFinder.clear();

        mResourcesManager.updateFinder(mGameObjectFinder);
        mItemsManager.updateFinder(mGameObjectFinder);
        for (Nation nation : mAllNations) {
            nation.updateFinder(mGameObjectFinder);
        }

        mResourcesManager.update(deltaTime, mRenderManager);
        mItemsManager.update(deltaTime, mRenderManager);
        for (Nation nation : mAllNations) {
            nation.update(deltaTime, mRenderManager);
        }

        mMinimap.update();
        mWorld.step(1 / 60f, 7, 4);

        mDebugPanel.update(this);
    }

    @Override
    protected boolean shouldTranslate(float x, float y) {
        return super.shouldTranslate(x, y) && !mPlayerNation.getChooserRect().isActive();
    }

    @Override
    protected void renderGame(Renderer renderer) {
        renderer.end();

        mResourcesManager.renderSelected(renderer);
        mItemsManager.renderSelected(renderer);
        mRenderManager.renderSelected(renderer);

        mResourcesManager.render(renderer);
        mItemsManager.render(renderer);
        mRenderManager.render(renderer);
        mPlayerNation.render(renderer);
    }

    @Override
    public void render(Renderer renderer) {
        super.render(renderer);
        if (Settings.isDebug()) {
            mBox2DDebugRenderer.render(mWorld, getCamera().matrix());
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        mWorld.dispose();
    }

    @Override
    public Stage getStage() {
        return super.getStage();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        mStickersManager.updateViewPort(width, height);
    }

    public StickersManager getStickerManager() {
        return mStickersManager;
    }

    public World getWorld() {
        return mWorld;
    }

    public GameObjectFinder getGameObjectFinder() {
        return mGameObjectFinder;
    }

    public ItemsManager getItemsManager() {
        return mItemsManager;
    }

    public ResourcesManager getResourcesManager() {
        return mResourcesManager;
    }

    public Pathfinder getPathfinder() {
        return mPathfinder;
    }

    public List<Nation> getAllNations() {
        return mAllNations;
    }

    private final StickersManager.LayoutCallback mLayoutStickersCallback = new StickersManager.LayoutCallback() {
        @Override
        public void layoutFinished(float topHeight, float bottomHeight) {
            getCamera().setMargins(topHeight, bottomHeight);
        }
    };

    private final GestureDetector.GestureAdapter mGestureAdapter = new GestureDetector.GestureAdapter() {
        @Override
        public boolean tap(float x, float y, int count, int button) {
            Vector2 tap = getCamera().unproject(x, y);
            mPlayerNation.handleTap(tap.x, tap.y, button);
            return false;
        }

        @Override
        public boolean longPress(float x, float y) {
            mPlayerNation.handleLongPress();
            return false;
        }
    };
}
