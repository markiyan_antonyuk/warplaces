package com.markantoni.warplaces.game.terrain;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.camera.Camera;
import com.markantoni.warplaces.utils.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by mark on 30.12.15.
 */
public class Terrain implements Disposable, com.markantoni.warplaces.utils.Renderable {
    public static final String TERRAIN = "TERRAIN";
    public static final Tile GRASS = new Tile(Assets.Terrain.GRASS.TEXTURE, 0, true);
    public static final Tile WATER = new Tile(Assets.Terrain.WATER.TEXTURE, 1, false, .2f, Assets.Terrain.WATER_ANIMATED.TEXTURE_REGION[0]);

    private final int DEFAULT_PREVIEW_TILE_SIZE = 2;
    public final Tile DEFAULT_TILE;

    private final Rect mRealSize = Rect.empty();
    private final Rect mTileBounds = new Rect(AbstractGameScreen.DEFAULT_TILE_SIZE);
    private Map<Tile, Map<Point, Point>> mTileMap = new TreeMap<Tile, Map<Point, Point>>();

    {
        mTileMap.put(GRASS, new HashMap<Point, Point>());
        mTileMap.put(WATER, new HashMap<Point, Point>());
    }

    private int mSize;
    private boolean mHasChanged;
    private Texture mPreviewTexture;
    private Camera mCamera;
    private TerrainObject mTerrainObject;

    public Terrain(TerrainSize terrainSize) {
        DEFAULT_TILE = GRASS;
        mSize = terrainSize.get();
        mRealSize.setSize(mTileBounds.width * mSize, mTileBounds.height * mSize);
    }

    public Terrain(JSONObject jsonObject) {
        mSize = jsonObject.optInt(JSON.SIZE);
        mRealSize.setSize(mTileBounds.width * mSize, mTileBounds.height * mSize);

        int defaultTile = jsonObject.optInt(JSON.DEFAULT);
        DEFAULT_TILE = parseTile(defaultTile);

        JSONArray tilesJson = jsonObject.optJSONArray(JSON.TILES);

        for (int i = 0; i < tilesJson.length(); i++) {
            JSONObject jsonTile = tilesJson.optJSONObject(i);
            Tile tile = parseTile(jsonTile.optInt(JSON.TYPE));

            JSONArray positions = jsonTile.optJSONArray(JSON.POSITIONS);
            for (int j = 0; j < positions.length(); j++) {
                JSONObject jsonPoint = positions.optJSONObject(j);
                Point point = new Point(jsonPoint.optInt(JSON.X), jsonPoint.optInt(JSON.Y));
                mTileMap.get(tile).put(point, point);
            }
        }
    }

    public int getSize() {
        return mSize;
    }

    public void setWorld(World world) {
        mTerrainObject = new TerrainObject(world, DEFAULT_TILE, mTileMap);
    }

    public Map<Tile, Map<Point, Point>> getTileMap() {
        return mTileMap;
    }

    public void setCamera(Camera camera) {
        mCamera = camera;
    }

    private Tile parseTile(int symbol) {
        if (symbol == GRASS.symbol) {
            return GRASS;
        }

        if (symbol == WATER.symbol) {
            return WATER;
        }

        return DEFAULT_TILE;
    }

    public Rect getRealSize() {
        return mRealSize;
    }

    public void setTileCell(int cellX, int cellY, Tile tile) {
        setTile(cellX * AbstractGameScreen.DEFAULT_TILE_SIZE.width, cellY * AbstractGameScreen.DEFAULT_TILE_SIZE.height, tile);
    }

    public void setTile(Vector2 vector2, Tile tile) {
        setTile(vector2.x, vector2.y, tile);
    }

    public void setTile(float x, float y, Tile tile) {
        if (!mRealSize.contains(x, y)) {
            return;
        }
        mHasChanged = true;
        Point point = new Point((int) (x / mTileBounds.width), (int) (y / mTileBounds.height));

        for (Tile t : mTileMap.keySet()) {
            if (tile == DEFAULT_TILE) {
                mTileMap.get(t).remove(point);
            }

            if (mTileMap.get(t).containsKey(point)) {
                mTileMap.get(t).remove(point);
            }
        }

        mTileMap.get(tile).put(point, point);

        if (mTerrainObject != null) {
            mTerrainObject.changed(point, tile);
        }
    }

    public JSONObject toJSON() {
        JSONObject terrainJson = new JSONObject();
        terrainJson.put(JSON.SIZE, mSize);
        terrainJson.put(JSON.DEFAULT, DEFAULT_TILE.symbol);

        JSONArray tilesJson = new JSONArray();

        for (Tile tile : mTileMap.keySet()) {
            JSONObject jsonTile = new JSONObject();
            JSONArray positions = new JSONArray();
            for (Point point : mTileMap.get(tile).keySet()) {
                JSONObject pointJson = new JSONObject();
                pointJson.put(JSON.X, point.x);
                pointJson.put(JSON.Y, point.y);

                positions.put(pointJson);
            }

            jsonTile.put(JSON.TYPE, tile.symbol);
            jsonTile.put(JSON.POSITIONS, positions);

            tilesJson.put(jsonTile);
        }

        terrainJson.put(JSON.TILES, tilesJson);

        JSONObject result = new JSONObject();
        result.put(JSON.TERRAIN, terrainJson);

        return result;
    }

    public Texture getTerrainPreview() {
        if (mPreviewTexture != null && !mHasChanged) {
            return mPreviewTexture;
        }

        Pixmap result = new Pixmap(mSize * DEFAULT_PREVIEW_TILE_SIZE, mSize * DEFAULT_PREVIEW_TILE_SIZE, Pixmap.Format.RGB888);

        DEFAULT_TILE.texture.getTextureData().prepare();
        //consume pixmap for first time only to draw it into another pixmap
        Pixmap defaultTilePixmap = DEFAULT_TILE.texture.getTextureData().consumePixmap();
        //draw this pixmap into temporary scaled pixmap, to save time (177 ms -> 66ms)
        Pixmap tempPixmap = new Pixmap(DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE, Pixmap.Format.RGB888);
        tempPixmap.drawPixmap(defaultTilePixmap, 0, 0, defaultTilePixmap.getWidth(), defaultTilePixmap.getHeight(),
                0, 0, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE);

        for (int y = 0; y < mSize; y++) {
            for (int x = 0; x < mSize; x++) {
                int xCoord = x * DEFAULT_PREVIEW_TILE_SIZE;
                int yCoord = y * DEFAULT_PREVIEW_TILE_SIZE;

                result.drawPixmap(tempPixmap, 0, 0, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE,
                        xCoord, yCoord, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE);
            }
        }

        for (Tile tile : mTileMap.keySet()) {
            tile.texture.getTextureData().prepare();
            tempPixmap.drawPixmap(tile.texture.getTextureData().consumePixmap(), 0, 0, tile.texture.getWidth(), tile.texture.getHeight(),
                    0, 0, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE);

            for (Point point : mTileMap.get(tile).keySet()) {
                int xCoord = point.x * DEFAULT_PREVIEW_TILE_SIZE;
                int yCoord = (mSize - point.y) * DEFAULT_PREVIEW_TILE_SIZE;

                result.drawPixmap(tempPixmap, 0, 0, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE,
                        xCoord, yCoord, DEFAULT_PREVIEW_TILE_SIZE, DEFAULT_PREVIEW_TILE_SIZE);
            }
        }

        if (mPreviewTexture != null) {
            mPreviewTexture.dispose();
        }
        mPreviewTexture = new Texture(result);
        mHasChanged = false;
        return mPreviewTexture;
    }

    public void update(float deltaTime) {
        WATER.animation.update(deltaTime);
    }

    @Override
    public void render(final Renderer renderer) {
        if (mCamera == null) {
            return;
        }
        Rect bounds = new Rect(mCamera.getVisibleBounds());
        int translationX = (int) (bounds.x / mTileBounds.width);
        int translationY = (int) (bounds.y / mTileBounds.height);
        //max is counted as startTranlation + tranlation.size / tile.size + tile.size as extra tile
        int maxX = (int) (translationX + bounds.width / mTileBounds.width + mTileBounds.width);
        int maxY = (int) (translationY + bounds.height / mTileBounds.height + mTileBounds.height);

        if (maxX > mSize) {
            maxX = mSize;
        }

        if (maxY > mSize) {
            maxY = mSize;
        }

        for (int y = translationY > 0 ? translationY : 0; y < maxY; y++) {
            for (int x = translationX > 0 ? translationX : 0; x < maxX; x++) {
                mTileBounds.set(x * mTileBounds.width, y * mTileBounds.height);
                if (renderer.needToRender(mTileBounds)) {
                    renderer.renderTexture(DEFAULT_TILE.texture, mTileBounds);
                }
            }
        }

        Point tempPoint = new Point();
        for (Tile tile : mTileMap.keySet()) {
            for (Point point : mTileMap.get(tile).keySet()) {
                mTileBounds.set(point.x * mTileBounds.width, point.y * mTileBounds.height);
                if (renderer.needToRender(mTileBounds)) {
                    if (tile.equals(WATER)) { //water rendering animation and generating beach
                        renderer.renderTextureRegion(tile.animation.getFrame(), mTileBounds);
                        drawBeach(renderer, mTileMap.get(tile).keySet(), tempPoint.set(point), mTileBounds);
                    } else {
                        renderer.renderTexture(tile.texture, mTileBounds);
                    }
                }
            }
        }
    }

    private void drawBeach(Renderer renderer, Set<Point> points, Point point, Rect bounds) {
        point.x++;
        if (!points.contains(point)) {
            renderer.renderTexture(Assets.Terrain.BEACH.TEXTURE, bounds);
        }

        point.x -= 2;
        if (!points.contains(point)) {
            renderer.renderTexture(Assets.Terrain.BEACH.TEXTURE, bounds, 180);
        }

        point.x++;
        point.y++;
        if (!points.contains(point)) {
            renderer.renderTexture(Assets.Terrain.BEACH.TEXTURE, bounds, 90);
        }

        point.y -= 2;
        if (!points.contains(point)) {
            renderer.renderTexture(Assets.Terrain.BEACH.TEXTURE, bounds, 270);
        }
    }

    @Override
    public void dispose() {
        if (mPreviewTexture != null) {
            mPreviewTexture.dispose();
        }
    }

    public interface JSON {
        String TERRAIN = "terrain";
        String SIZE = "size";
        String TILES = "tiles";
        String X = "x";
        String Y = "y";
        String TYPE = "type";
        String DEFAULT = "default";
        String POSITIONS = "positions";
    }

    public static class Tile implements Comparable<Tile> {
        private static int order; //overall count of calling Tile constructors
        private final int thisOrder; //order in which tiles should be handled as hashcode
        final Texture texture;
        final Animation animation;
        final int symbol;
        final boolean isPassable;

        private Tile(Texture texture, int symbol, boolean isPassable) {
            this(texture, symbol, isPassable, 0, null);
        }

        private Tile(Texture texture, int symbol, boolean isPassable, float frameDuration, TextureRegion[] frames) {
            thisOrder = order++;
            this.texture = texture;
            this.symbol = symbol;
            this.isPassable = isPassable;
            if (frames != null) {
                animation = new Animation(frameDuration, frames);
            } else {
                animation = null;
            }
        }

        @Override
        public int compareTo(Tile o) {
            return thisOrder - o.thisOrder;
        }

        public boolean isPassable() {
            return isPassable;
        }

        @Override
        public int hashCode() {
            return thisOrder;
        }
    }
}
