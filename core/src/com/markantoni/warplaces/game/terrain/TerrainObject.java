package com.markantoni.warplaces.game.terrain;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.utils.PhysicUtils;
import com.markantoni.warplaces.utils.Point;
import com.markantoni.warplaces.utils.Rect;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mark on 02.02.16.
 */
public class TerrainObject {
    private final Terrain.Tile DEFAULT_TILE;
    private final Map<Point, Body> mBodies = new HashMap<Point, Body>();
    private final World mWorld;
    private final Rect mTempRect = Rect.empty();

    TerrainObject(World world, Terrain.Tile defaultTile, Map<Terrain.Tile, Map<Point, Point>> tiles) {
        DEFAULT_TILE = defaultTile;
        mWorld = world;
        mTempRect.setSize(AbstractGameScreen.DEFAULT_TILE_SIZE.width, AbstractGameScreen.DEFAULT_TILE_SIZE.height);

        for (Terrain.Tile tile : tiles.keySet()) {
            if (tile == DEFAULT_TILE) {
                continue;
            }

            for (Point point : tiles.get(tile).keySet()) {
                mBodies.put(point, createBody(point, tile));
            }
        }
    }

    void changed(Point point, Terrain.Tile tile) {
        Body body = mBodies.remove(point);
        if (body != null) {
            mWorld.destroyBody(body);
        }

        if (tile != DEFAULT_TILE) {
            mBodies.put(point, createBody(point, tile));
        }
    }

    Body createBody(Point point, Terrain.Tile tile) {
        mTempRect.set(point.x * AbstractGameScreen.DEFAULT_TILE_SIZE.width,
                point.y * AbstractGameScreen.DEFAULT_TILE_SIZE.height);
        Body body = PhysicUtils.createStaticBody(mWorld, mTempRect.x, mTempRect.y, PhysicUtils.rectWithoutEdges(mTempRect.width, mTempRect.height, 6));
        body.getFixtureList().first().setUserData(tile);

        return body;
    }
}
