package com.markantoni.warplaces.game.terrain;

/**
 * Created by mark on 21.01.16.
 */
public enum TerrainSize {
    SMALL(100), MEDIUM(250), BIG(500), ENORMOUS(750), DEBUG(25);

    private int mSize;

    TerrainSize(int size) {
        mSize = size;
    }

    public int get() {
        return mSize;
    }

    public static TerrainSize get(int size) {
        switch (size) {
            case 25:
                return DEBUG;

            case 250:
                return MEDIUM;

            case 500:
                return BIG;

            case 750:
                return ENORMOUS;

            case 100:
            default:
                return SMALL;
        }
    }

}
