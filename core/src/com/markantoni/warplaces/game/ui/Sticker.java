package com.markantoni.warplaces.game.ui;

import com.badlogic.gdx.math.MathUtils;
import com.markantoni.warplaces.game.ui.settings.HUDSettings;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 17.03.16.
 */
public abstract class Sticker implements Comparable<Sticker> {
    private final String mName;
    private int mPriority;
    private boolean mTop;
    private boolean mLeft;
    private boolean mIsSticky;

    public Sticker(String name, boolean sticky) {
        this(name, sticky, 0, true, true);
    }

    public Sticker(String name, boolean sticky, int priority, boolean top, boolean left) {
        mName = name;
        mPriority = HUDSettings.getPriority(this, priority);
        mTop = HUDSettings.getTop(this, top);
        mLeft = HUDSettings.getLeft(this, left);
        mIsSticky = sticky;
    }

    public String getName() {
        return mName;
    }

    public void setLeft(boolean left) {
        mLeft = left;
    }

    public void setRight(boolean right) {
        mLeft = !right;
    }

    public void setTop(boolean top) {
        mTop = top;
    }

    public void setBottom(boolean bottom) {
        mTop = !bottom;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public int getPriority() {
        return mPriority;
    }

    public boolean isLeft() {
        return mLeft;
    }

    public boolean isRight() {
        return !mLeft;
    }

    public boolean isTop() {
        return mTop;
    }

    public boolean isBottom() {
        return !mTop;
    }

    public boolean isSticky() {
        return mIsSticky;
    }

    @Override
    public int compareTo(Sticker o) {
        int result = mPriority - o.mPriority;
        return result == 0 ? MathUtils.random(100) : result;
    }

    public abstract boolean isVisible();

    public abstract void setPosition(float x, float y);

    public abstract Rect getBounds();
}
