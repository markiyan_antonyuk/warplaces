package com.markantoni.warplaces.game.ui;

import com.badlogic.gdx.Gdx;
import com.markantoni.warplaces.utils.Rect;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by mark on 16.03.16.
 */
// TODO: 16.03.16 Extend to handle centered stickers
public class StickersManager {
    private final List<Sticker> mAllStickers = new ArrayList<Sticker>();
    private final List<Sticker> mTopStickers = new ArrayList<Sticker>();
    private final List<Sticker> mBottomStickers = new ArrayList<Sticker>();
    private final TreeSet<Sticker> mLeftTree = new TreeSet<Sticker>();
    private final TreeSet<Sticker> mRightTree = new TreeSet<Sticker>();
    private final Rect mViewPort = Rect.empty();
    private LayoutCallback mLayoutCallback;

    public StickersManager(float x, float y, float width, float height) {
        mViewPort.set(x, y, width, height);
    }

    public void setLayoutCallback(LayoutCallback layoutCallback) {
        mLayoutCallback = layoutCallback;
    }

    public void updateViewPort(float width, float height) {
        mViewPort.setSize(width, height);
        layoutStickers();
    }

    public void updateViewPortPosition(float x, float y) {
        mViewPort.set(x, y);
        layoutStickers();
    }

    public void registerSticker(Sticker sticker) {
        if (!sticker.isSticky()) {
            return;
        }

        mAllStickers.add(sticker);
    }

    /*
    *|       <--  ^  -->
    *|            |
    *|        layouting (first top|bottom, then left|right
    *|            |
    *|       <--  v  -->
    */
    public void layoutStickers() {
        separateStickers(mAllStickers, mTopStickers, mBottomStickers);
        float topHeight = layoutTopStickers();
        float bottomHeight = layoutBottomStickers();
        if (mLayoutCallback != null) {
            mLayoutCallback.layoutFinished(topHeight, bottomHeight);
        }
    }

    private void separateStickers(List<Sticker> stickers, List<Sticker> top, List<Sticker> bottom) {
        top.clear();
        bottom.clear();

        for (Sticker sticker : stickers) {
            if (sticker.isTop()) {
                top.add(sticker);
            } else {
                bottom.add(sticker);
            }
        }
    }

    private void separateStickers(List<Sticker> stickers, TreeSet<Sticker> leftTree, TreeSet<Sticker> rightTree) {
        leftTree.clear();
        rightTree.clear();

        for (Sticker sticker : stickers) {
            if (sticker.isVisible()) {
                if (sticker.isLeft()) {
                    leftTree.add(sticker);
                } else {
                    rightTree.add(sticker);
                }
            }
        }
    }

    //returns biggest panel height
    private float layoutBottomStickers() {
        separateStickers(mBottomStickers, mLeftTree, mRightTree);
        float result = 0;

        Sticker last = null;
        for (Sticker sticker : mLeftTree) {
            float x = last == null ? mViewPort.x : last.getBounds().x + last.getBounds().width;
            sticker.setPosition(x, mViewPort.y);
            result = Math.max(result, sticker.getBounds().height);

            last = sticker;
        }

        last = null;
        for (Sticker sticker : mRightTree) {
            float x = last == null ? mViewPort.width - sticker.getBounds().width : last.getBounds().x - sticker.getBounds().width;
            sticker.setPosition(x, mViewPort.y);
            result = Math.max(result, sticker.getBounds().height);

            last = sticker;
        }

        return result;
    }

    //returns biggest panel height
    private float layoutTopStickers() {
        separateStickers(mTopStickers, mLeftTree, mRightTree);
        float result = 0;

        Sticker last = null;
        for (Sticker sticker : mLeftTree) {
            float x = last == null ? mViewPort.x : last.getBounds().x + last.getBounds().width;
            float y = mViewPort.height - sticker.getBounds().height;
            sticker.setPosition(x, y);
            result = Math.max(result, sticker.getBounds().height);

            last = sticker;
        }

        last = null;
        for (Sticker sticker : mRightTree) {
            float x = last == null ? mViewPort.width - sticker.getBounds().width : last.getBounds().x - sticker.getBounds().width;
            float y = mViewPort.height - sticker.getBounds().height;
            sticker.setPosition(x, y);
            result = Math.max(result, sticker.getBounds().height);

            last = sticker;
        }

        return result;
    }

    public static interface LayoutCallback {
        void layoutFinished(float topHeight, float bottomHeight);
    }
}
