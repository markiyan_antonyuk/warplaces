package com.markantoni.warplaces.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.kotcrab.vis.ui.widget.CollapsibleWidget;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.ui.settings.HUDSettings;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 11.02.16.
 */
public abstract class UiPanel extends Sticker {
    private final StickersManager mStickersManager;
    private final VisWindow mWindow;
    private final Rect mBounds = Rect.empty();
    private CollapsibleWidget mCollapsibleWidget;
    private VisTextButton mHideShowButton;

    public UiPanel(GameScreen gameScreen, String name, boolean collapsible, boolean isSticky, int stickerPriority, boolean top, boolean left) {
        super(name, isSticky, stickerPriority, top, left);
        mWindow = new VisWindow("", false);
        mWindow.setModal(false);
        mWindow.setMovable(!isSticky);
        if (!isSticky) {
            mWindow.centerWindow();
            HUDSettings.initialSave(this);
        }
        mWindow.padTop(SizeUnit.uiY(1f));
        Stage stage = gameScreen.getStage();
        mStickersManager = gameScreen.getStickerManager();

        if (!collapsible) {
            mWindow.add(create());
            mWindow.pack();
            stage.addActor(mWindow);
            mStickersManager.registerSticker(this);
            return;
        }

        mHideShowButton = new VisTextButton(Strings.get("show"));
        mHideShowButton.setHeight(mWindow.getTitleLabel().getHeight());
        mHideShowButton.setFocusBorderEnabled(false);
        mWindow.getTitleTable().add(mHideShowButton).prefSize(SizeUnit.uiX(1), SizeUnit.uiY(1f)).pad(SizeUnit.uiUnit(.1f));

        mCollapsibleWidget = new CollapsibleWidget(create(), true);
        mWindow.add(mCollapsibleWidget);
        mWindow.pack();
        stage.addActor(mWindow);
        mStickersManager.registerSticker(this);

        mHideShowButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mCollapsibleWidget.setCollapsed(!mCollapsibleWidget.isCollapsed(), false);
                mHideShowButton.setText(Strings.get(mCollapsibleWidget.isCollapsed() ? "show" : "hide"));
                mWindow.pack();
                mStickersManager.layoutStickers();
            }
        });
    }

    protected void updateButtonDrawable(VisTextButton button, Sprite sprite) {
        BitmapFont bitmapFont = button.getStyle().font;
        Drawable drawable = Assets.DARK_GRAY_DRAWABLE;
        Drawable down = Assets.DARK_GRAY_DRAWABLE;
        if (sprite != null) {
            drawable = new SpriteDrawable(sprite);
            down = ((SpriteDrawable) drawable).tint(Color.DARK_GRAY);
        }

        button.setStyle(new VisTextButton.VisTextButtonStyle(drawable, down, drawable, bitmapFont));
    }

    protected abstract Table create();

    protected void setTitle(String text) {
        mWindow.getTitleLabel().setText(text);
    }

    public VisWindow getUi() {
        return mWindow;
    }

    public void setVisible(boolean visible) {
        mWindow.setVisible(visible);
        mStickersManager.layoutStickers();
    }

    protected boolean isFullyVisible() {
        return mWindow.isVisible() && !mCollapsibleWidget.isCollapsed();
    }

    @Override
    public void setPosition(float x, float y) {
        mWindow.setPosition(x, y);
    }

    @Override
    public Rect getBounds() {
        mBounds.set(mWindow.getX(), mWindow.getY(), mWindow.getWidth(), mWindow.getHeight());
        return mBounds;
    }

    @Override
    public boolean isVisible() {
        return mWindow.isVisible();
    }
}
