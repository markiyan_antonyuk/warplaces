package com.markantoni.warplaces.game.ui.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.markantoni.warplaces.game.actions.ActionsPanel;
import com.markantoni.warplaces.game.buildings.storage.StoragePanel;
import com.markantoni.warplaces.game.minimap.MinimapPanel;
import com.markantoni.warplaces.game.objects.ChooserPanel;
import com.markantoni.warplaces.game.objects.GameObjectPanel;
import com.markantoni.warplaces.game.resources.ResourcesPanel;
import com.markantoni.warplaces.game.ui.Sticker;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.game.units.inventory.InventoryPanel;

/**
 * Created by mark on 18.03.16.
 */
public class HUDSettings {
    public static final Class<? extends UiPanel>[] UI_PANELS = new Class[]{
            InventoryPanel.class,
            StoragePanel.class,
            GameObjectPanel.class,
            ActionsPanel.class,
            ResourcesPanel.class,
            ChooserPanel.class,
            MinimapPanel.class
    };

    private static final String PREF_NAME = "hud_settings";
    private static final String LEFT = "left_";
    private static final String TOP = "top_";
    private static final String PRIORITY = "priority_";
    private static final Preferences preferences = Gdx.app.getPreferences(PREF_NAME);

    public static void initialSave(Sticker sticker) {
        if (!isSaved(sticker)) {
            save(sticker);
        }
    }

    public static boolean isSaved(Sticker sticker) {
        return preferences.contains(PRIORITY + sticker.getName()) && preferences.contains(LEFT + sticker.getName()) && preferences.contains(TOP + sticker.getName());
    }

    public static void save(Sticker sticker) {
        preferences.putBoolean(LEFT + sticker.getName(), sticker.isLeft());
        preferences.putBoolean(TOP + sticker.getName(), sticker.isTop());
        preferences.putInteger(PRIORITY + sticker.getName(), sticker.getPriority());
        preferences.flush();
    }

    public static boolean getLeft(Sticker sticker, boolean defaultValue) {
        return preferences.getBoolean(LEFT + sticker.getName(), defaultValue);
    }

    public static boolean getTop(Sticker sticker, boolean defaultValue) {
        return preferences.getBoolean(TOP + sticker.getName(), defaultValue);
    }

    public static int getPriority(Sticker sticker, int defaultValue) {
        return preferences.getInteger(PRIORITY + sticker.getName(), defaultValue);
    }
}
