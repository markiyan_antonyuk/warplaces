package com.markantoni.warplaces.game.ui.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.NumberSelector;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisRadioButton;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.ui.StickersManager;
import com.markantoni.warplaces.game.ui.UiPanel;
import com.markantoni.warplaces.screens.menu.MenuScreen;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 17.03.16.
 */
public class HUDSettingsScreen extends MenuScreen {
    private static final int MARGINS = SizeUnit.uiUnit(2);

    private List<TableSticker> mStickers;
    private StickersManager mStickersManager;
    private ButtonGroup<VisRadioButton> mRadioButtons;

    private ButtonGroup<VisRadioButton> mLeftRight;
    private ButtonGroup<VisRadioButton> mTopBottom;
    private NumberSelector mPrioritySelector;

    @Override
    protected void initUI(Stage stage) {
        mStickersManager = new StickersManager(MARGINS, MARGINS, Gdx.graphics.getWidth() - MARGINS * 2, Gdx.graphics.getHeight() - MARGINS * 2);
        mStickers = new ArrayList<TableSticker>();
        mRadioButtons = new ButtonGroup<VisRadioButton>();

        Table buttons = initAttributesTable();
        initStickers(stage);
        mRadioButtons.getButtons().first().setChecked(true);

        Table table = generateTable();
        table.add(buttons).top();

        stage.addActor(table);
        layoutStickers();
    }

    private void updateAttributesTable(TableSticker tableSticker) {
        mPrioritySelector.setValue(tableSticker.getPriority(), false);
        mLeftRight.getButtons().get(0).setChecked(tableSticker.isLeft());
        mLeftRight.getButtons().get(1).setChecked(tableSticker.isRight());
        mTopBottom.getButtons().get(0).setChecked(tableSticker.isTop());
        mTopBottom.getButtons().get(1).setChecked(tableSticker.isBottom());
    }

    private Table initAttributesTable() {
        final Table table = new Table();
        mLeftRight = new ButtonGroup<VisRadioButton>();
        mTopBottom = new ButtonGroup<VisRadioButton>();

        final VisRadioButton left = new VisRadioButton("");
        mLeftRight.add(left);

        VisRadioButton right = new VisRadioButton("");
        mLeftRight.add(right);

        final VisRadioButton top = new VisRadioButton("");
        mTopBottom.add(top);

        VisRadioButton bottom = new VisRadioButton("");
        mTopBottom.add(bottom);

        VisTextButton save = new VisTextButton(Strings.get("save"));

        mPrioritySelector = new NumberSelector(Strings.get("priority"), 0, 0, HUDSettings.UI_PANELS.length - 1);

        table.add(top).colspan(2).row();
        table.add(left, right);
        table.row();
        table.add(bottom).colspan(2).row();
        table.add(mPrioritySelector).colspan(2).padTop(SizeUnit.uiUnit(.1f)).row();
        table.add(save).colspan(2).padTop(SizeUnit.uiUnit(.1f)).row();

        mPrioritySelector.addChangeListener(new NumberSelector.NumberSelectorListener() {
            @Override
            public void changed(float number) {
                getSelectedSticker().setPriority((int) number);
                layoutStickers();
            }
        });

        left.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getSelectedSticker().setLeft(left.isChecked());
                layoutStickers();
            }
        });

        top.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getSelectedSticker().setTop(top.isChecked());
                layoutStickers();
            }
        });

        save.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                saveSettings();
            }
        });

        return table;
    }

    private TableSticker getSelectedSticker() {
        return (TableSticker) mRadioButtons.getChecked().getUserObject();
    }

    private void layoutStickers() {
        mStickersManager.layoutStickers();
    }

    private void initStickers(Stage stage) {
        for (Class<? extends UiPanel> panel : HUDSettings.UI_PANELS) {
            Table table = new Table();
            final VisRadioButton radioButton = new VisRadioButton("");
            VisLabel label = new VisLabel(panel.getSimpleName(), Align.center);

            table.setBackground(Assets.DARK_GRAY_DRAWABLE);
            table.padLeft(SizeUnit.uiUnit(.1f));
            table.padRight(SizeUnit.uiUnit(.1f));
            table.add(radioButton).row();
            table.add(label);
            table.pack();

            final TableSticker tableSticker = new TableSticker(table, panel.getName());
            mStickers.add(tableSticker);
            mStickersManager.registerSticker(tableSticker);
            mRadioButtons.add(radioButton);
            stage.addActor(table);

            table.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    radioButton.setChecked(true);
                }
            });

            radioButton.setUserObject(tableSticker);
            radioButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (radioButton.isChecked()) {
                        updateAttributesTable(tableSticker);
                    }
                }
            });
        }
    }

    private void saveSettings() {
        for (TableSticker sticker : mStickers) {
            HUDSettings.save(sticker);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        mStickersManager.updateViewPort(width - MARGINS * 2, height - MARGINS * 2);
    }
}
