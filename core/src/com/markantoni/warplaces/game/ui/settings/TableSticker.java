package com.markantoni.warplaces.game.ui.settings;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.markantoni.warplaces.game.ui.Sticker;
import com.markantoni.warplaces.utils.Rect;

/**
 * Created by mark on 17.03.16.
 */
class TableSticker extends Sticker {
    private Table mTable;
    private final Rect mBounds = Rect.empty();

    TableSticker(Table table, String name) {
        super(name, true);
        mTable = table;
    }

    Table getTable() {
        return mTable;
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public void setPosition(float x, float y) {
        mTable.setPosition(x, y);
    }

    @Override
    public Rect getBounds() {
        mBounds.set(mTable.getX(), mTable.getY(), mTable.getWidth(), mTable.getHeight());
        return mBounds;
    }
}
