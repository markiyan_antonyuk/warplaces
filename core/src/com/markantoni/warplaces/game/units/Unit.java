package com.markantoni.warplaces.game.units;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.pathfinding.Pathfinder;
import com.markantoni.warplaces.game.resources.objects.Resource;
import com.markantoni.warplaces.game.units.inventory.Inventory;
import com.markantoni.warplaces.game.units.inventory.InventorySlot;
import com.markantoni.warplaces.game.units.types.UnitType;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Collideable;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

import java.util.HashSet;

/**
 * Created by mark on 26.01.16.
 */
public class Unit extends GameObject implements Collideable, ActionsPerformer {
    public static final int SIGHT_RADIUS = 10;

    private final Inventory mInventory;
    private final TextureRegion[][] mTextureRegion = Assets.Units.UNIT.TEXTURE_REGION;
    private final TextureRegion[][] mClothes = Assets.Units.UNIT_CLOTHES.TEXTURE_REGION;
    private UnitType mUnitType;
    private GameObject mTarget;
    private State mState = State.IDLE;
    private HashSet<GameObject> mCollisionObjects = new HashSet<GameObject>();

    public Unit(Nation nation, World world, float x, float y) {
        super(nation.getGameScreen(), nation);
        setPhysicObject(new UnitObject(this, world, x, y));
        mInventory = new Inventory(this);
        mInventory.setChangedCallback(mChangedCallback);
        mUnitType = new UnitType(nation, this);
    }

    public void dropItem(Item item) {
        mCollisionObjects.add(item);
        getItemsManager().createItem(item, Utils.randomRange(getBounds().x, getBounds().width),
                Utils.randomRange(getBounds().y, getBounds().height / UnitObject.HEIGHT_DIVIDER));
    }

    public boolean pickUpItem(Item item) {
        if (!mUnitType.isPreferredType(item.getClass()) || item.getContainer() != null) {
            return false;
        }

        boolean result = mInventory.addItem(item);
        if (result) {
            getItemsManager().remove(item);
        }
        return result;
    }

    @Override
    public Color getColor() {
        return getUnitsManager().getNation().getColor();
    }

    @Override
    public String getName() {
        return mUnitType.getName();
    }

    @Override
    public GameObjectManager getManager() {
        return getUnitsManager();
    }

    public Class<? extends GameObject> getType() {
        return mUnitType.getClass();
    }

    @Override
    public Sprite getPreviewSprite() {
        return mUnitType.getPreviewSprite();
    }


    public Inventory getInventory() {
        return mInventory;
    }

    public void setState(State state) {
        if (state == State.WORKING && state != mState) {
            stopMovement();
        }
        mState = state;
    }

    public State getState() {
        return mState;
    }

    public void update(float deltaTime) {
        getPhysicObject().update(deltaTime);

        switch (mState) {
            case IDLE:
                //can be moved to unit type
                mUnitType.handleAction(this);
                break;
        }

        for (GameObject object : mCollisionObjects) {
            if (object == mTarget) {
                mUnitType.handleTarget(mTarget, this, deltaTime);
            }
        }
    }

    @Override
    public void freePhysicObject(World world) {
        mInventory.dropAll();
        super.freePhysicObject(world);
        mUnitType.dispose();
    }

    public StorageBuilding getCollidedStorage() {
        for (GameObject object : mCollisionObjects) {
            if (object instanceof StorageBuilding) {
                return (StorageBuilding) object;
            }
        }
        return null;
    }

    protected Pathfinder getPathfinder() {
        return getUnitsManager().getGameScreen().getPathfinder();
    }

    @Override
    public Vector2 getIsometricPosition() {
        return getBounds().getPosition(mPosition);
    }

    public Rect getBounds() {
        return getPhysicObject().getBounds();
    }

    public void setTarget(GameObject target) {
        if (target != null) {
            startMovement(target.getCenter().x, target.getCenter().y);
        }
        mTarget = target;
    }

    void startMovement(float x, float y) {
        mTarget = null;
        getPhysicObject().startMovement(x, y);
    }

    public void stopMovement() {
        getPhysicObject().stopMovement();
    }

    @Override
    public void render(Renderer renderer) {
        int[] angle = getPhysicObject().getAngle();
        mBounds.set(getPhysicObject().getBounds());

        if (!renderer.needToRender(mBounds)) {
            return;
        }

        renderer.renderTextureRegion(mTextureRegion[angle[0]][angle[1]], mBounds);
        renderer.renderTextureRegion(mClothes[angle[0]][angle[1]], mBounds, getColor());

        Tool tool = (Tool) mInventory.getItem(InventorySlot.MAINHAND_TOOL);
        if (tool != null) {
            renderer.renderTextureRegion(tool.getEquippedTexture()[angle[0]][angle[1]], mBounds);
        }

        tool = (Tool) mInventory.getItem(InventorySlot.OFFHAND_TOOL);
        if (tool != null) {
            renderer.renderTextureRegion(tool.getEquippedTexture()[angle[0]][angle[1]], mBounds);
        }

        if (Settings.isDebug()) {
            if (mIsSelected) {
                getPhysicObject().render(renderer);
            }
        }
    }

    @Override
    public void renderSelected(Renderer renderer) {
        mBounds.set(getPhysicObject().getBounds());

        if (renderer.needToRender(mBounds)) {
            renderer.renderEllipse(mIsMainSelected ? Color.GOLD : Color.RED, mBounds.x, mBounds.y, mBounds.width, mBounds.height / UnitObject.HEIGHT_DIVIDER);
        }
    }

    @Override
    public boolean willCollide(Object another) {
        return !(another instanceof Item ||
                another instanceof Resource);
    }

    @Override
    public void startCollision(Object another) {
        if (another instanceof Item) {
            if (!mCollisionObjects.contains(another)) {
                pickUpItem((Item) another);
            }
        }

        if (another instanceof GameObject) {
            mCollisionObjects.add((GameObject) another);
        }

        if (another == mTarget) {
            mUnitType.handleTarget(mTarget, this, 0);
        }
    }

    @Override
    public void endCollision(Object another) {
        if (another instanceof GameObject) {
            mCollisionObjects.remove(another);
        }

        if (another == mTarget) {
            mTarget = null;
            if (mState == State.WORKING) {
                setState(State.IDLE);
            }
        }
    }

    protected float getSpeedMultiplier() {
        float multiplier = 1;

        for (GameObject object : mCollisionObjects) {
            if (object instanceof Resource) {
                multiplier = ((Resource) object).getPassingSpeedMultiplier();
                break;
            }
        }

        return multiplier;
    }

    @Override
    public GameObject getHolder() {
        return this;
    }

    @Override
    public Action getAction(int actionIndex) {
        return mUnitType.getAction(actionIndex);
    }

    public enum State {
        IDLE, MOVING, WORKING, ATTACKING
    }

    private final Container.ChangedCallback<InventorySlot> mChangedCallback = new Container.ChangedCallback<InventorySlot>() {
        @Override
        public void changed(Item item, InventorySlot oldSlot, InventorySlot newSlot) {
            if (newSlot == InventorySlot.MAINHAND_TOOL || oldSlot == InventorySlot.MAINHAND_TOOL) {
                mUnitType.reinitialize();
                mTarget = null;
            }

            if (newSlot == InventorySlot.OFFHAND_TOOL || oldSlot == InventorySlot.OFFHAND_TOOL) {
                mUnitType.reinitialize();
            }
        }
    };

    @Override
    protected UnitObject getPhysicObject() {
        return (UnitObject) super.getPhysicObject();
    }

    @Override
    public String type() {
        return Strings.get("unit");
    }

    @Override
    public String inventory() {
        return mInventory.getBackpack().size() + "/" + Inventory.BACKPACK_SIZE;
    }
}
