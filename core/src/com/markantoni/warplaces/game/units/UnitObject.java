package com.markantoni.warplaces.game.units;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.objects.PhysicObject;
import com.markantoni.warplaces.game.pathfinding.Pathfinder;
import com.markantoni.warplaces.utils.PhysicUtils;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderable;
import com.markantoni.warplaces.utils.Renderer;
import com.markantoni.warplaces.utils.SizeUnit;

import java.util.LinkedList;

/**
 * Created by mark on 27.01.16.
 */
public class UnitObject extends PhysicObject implements Renderable {
    static final float HEIGHT_DIVIDER = 3;
    private static final Rect SIZE = new Rect(SizeUnit.x(.3f), SizeUnit.y(.5f));
    private final float SPEED = SizeUnit.unit(3);

    private static final Vector2 sTempVector = new Vector2();
    private final LinkedList<Vector2> mPoints = new LinkedList<Vector2>();
    private final Rect mDebugRect = new Rect(10, 10);
    private final Rect mBounds = new Rect(SIZE);
    private final Vector2 mDestinationPoint = new Vector2(); //todo rect
    private final int[] mAngle = new int[2];
    private final Unit mUnit;

    UnitObject(Unit unit, World world, float x, float y) {
        mUnit = unit;
        mBody = PhysicUtils.createBody(world, x, y, PhysicUtils.rectWithoutEdges(SIZE.width, SIZE.height / HEIGHT_DIVIDER, 3));
        mBounds.set(mBody.getPosition());
    }

    protected void update(float deltaTime) {
        if (mBounds.getCenter(sTempVector).equals(mBody.getPosition())) {
            stopMovement();
        }

        mBounds.setCenter(mBody.getPosition());

        if (mBounds.contains(mDestinationPoint)) {
            if (!mPoints.isEmpty()) {
                mDestinationPoint.set(mPoints.pop());
                recalculateDirection();
            } else {
                stopMovement();
            }
        } else {
            recalculateDirection();
        }
    }

    @Override
    public void render(Renderer renderer) {
        mDebugRect.set(mDestinationPoint);
        renderer.renderColor(Color.CYAN, mDebugRect, ShapeRenderer.ShapeType.Filled);
        for (Vector2 vector2 : mPoints) {
            mDebugRect.set(vector2);
            renderer.renderColor(Color.BLUE, mDebugRect, ShapeRenderer.ShapeType.Filled);
        }
    }

    Rect getBounds() {
        mBounds.set(mBody.getPosition());
        return mBounds;
    }

    protected void startMovement(float x, float y) {
        mUnit.setState(Unit.State.MOVING);
        mDestinationPoint.set(x, y);
        recalculateDirection();
        mUnit.getPathfinder().findPathAsync(mBody.getPosition(), mDestinationPoint, mPoints, mFinderCallback);
    }

    protected void stopMovement() {
        mUnit.setState(Unit.State.IDLE);
        mDestinationPoint.setZero();
        mBody.setLinearVelocity(mDestinationPoint);
        mPoints.clear();
    }

    private void recalculateDirection() {
        if (!mBounds.contains(mDestinationPoint) && !mDestinationPoint.isZero()) {
            mBody.setLinearVelocity(PhysicUtils.calculateDirection(mBody.getPosition(), mDestinationPoint, SPEED * mUnit.getSpeedMultiplier()));
            calculateAngle();
        }
    }

    private void calculateAngle() {
        //rewrite, make smaller
        float angle = mBody.getLinearVelocity().angle();
        float halfSector = 22.5f;
        int firstIndex = 0;
        int secondIndex = 0;

        calculation:
        {
            if (angle > 315 + halfSector || angle <= halfSector) {
                break calculation;
            }

            if (angle >= halfSector && angle < 45 + halfSector) {
                secondIndex = 1;
                break calculation;
            }

            if (angle >= 45 + halfSector && angle < 90 + halfSector) {
                secondIndex = 2;
                break calculation;
            }

            if (angle >= 90 + halfSector && angle < 135 + halfSector) {
                secondIndex = 3;
                break calculation;
            }

            firstIndex = 1;

            if (angle >= 135 + halfSector && angle < 180 + halfSector) {
                break calculation;
            }

            if (angle >= 180 + halfSector && angle < 225 + halfSector) {
                secondIndex = 1;
                break calculation;
            }

            if (angle >= 225 + halfSector && angle < 270 + halfSector) {
                secondIndex = 2;
                break calculation;
            }

            if (angle >= 270 + halfSector && angle < 315 + halfSector) {
                secondIndex = 3;
                break calculation;
            }
        }

        mAngle[0] = firstIndex;
        mAngle[1] = secondIndex;
    }

    protected int[] getAngle() {
        return mAngle;
    }

    private final Pathfinder.FinderCallback mFinderCallback = new Pathfinder.FinderCallback() {
        @Override
        public void pathFound(LinkedList<Vector2> path) {
            path.add(mDestinationPoint.cpy());
            mDestinationPoint.set(path.pop());
        }
    };
}
