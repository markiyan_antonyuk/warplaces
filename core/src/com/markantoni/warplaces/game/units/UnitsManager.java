package com.markantoni.warplaces.game.units;

import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectFinder;
import com.markantoni.warplaces.game.Updatable;
import com.markantoni.warplaces.game.RenderManager;
import com.markantoni.warplaces.game.objects.GameObjectManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 26.01.16.
 */
public class UnitsManager extends GameObjectManager<Unit> implements Updatable {
    private final World mWorld;

    public UnitsManager(Nation nation, World world) {
        super(nation.getGameScreen(), nation);
        mWorld = world;
    }

    public Unit createUnit(float x, float y) {
        Unit unit = new Unit(getNation(), mWorld, x, y);
        add(unit);
        return unit;
    }

    @Override
    protected void cutSelectionTo(Class<? extends GameObject> type) {
        List<Unit> objects = new ArrayList<Unit>();
        for (Unit unit : mSelected) {
            if (unit.getType().equals(type)) {
                objects.add(unit);
            }
        }

        clearSelection();
        for (Unit unit : objects) {
            select(unit);
        }
    }

    @Override
    public void update(float deltaTime, RenderManager renderManager) {
        super.update(deltaTime, renderManager);
        for (Unit unit : mAll) {
            unit.update(deltaTime);
            renderManager.put(unit);
        }
    }

    @Override
    protected boolean handleAction(float x, float y) {
        if (!mSelected.isEmpty()) {
            for (Unit unit : mSelected) {
                unit.startMovement(x, y);
            }
            return true;
        }
        return false;
    }
}
