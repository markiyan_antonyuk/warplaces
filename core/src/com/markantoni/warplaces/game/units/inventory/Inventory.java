package com.markantoni.warplaces.game.units.inventory;

import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.offhand.OffHandTool;
import com.markantoni.warplaces.game.items.tools.twohand.TwoHandTool;
import com.markantoni.warplaces.game.units.Unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 27.01.16.
 */
public class Inventory extends Container<InventorySlot> {
    public static final int BACKPACK_SIZE = 6;

    private final Map<InventorySlot, Item> mContainer = new HashMap<InventorySlot, Item>();
    private final List<Item> mBackpack = new ArrayList<Item>(BACKPACK_SIZE);
    private final Unit mUnit;

    public Inventory(Unit unit) {
        mUnit = unit;
    }

    @Override
    public boolean addItem(Item item) {
        for (int i = 0; i < InventorySlot.SLOTS_COUNT; i++) {
            InventorySlot inventorySlot = InventorySlot.getByIndex(i);
            if (canBePlacedInSlot(item, inventorySlot)) {
                if (getItem(inventorySlot) == null) {
                    addItem(item, inventorySlot);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean addItem(Item item, InventorySlot slot) {
        if (mContainer.containsKey(slot)) {
            return false;
        }
        mContainer.put(slot, item);
        notifyObserver();
        notifyChanged(item, null, slot);
        item.setContainer(this);
        return true;
    }

    @Override
    public Item getFirst(Class<? extends Item> type) {
        for (int i = 0; i < InventorySlot.SLOTS_COUNT; i++) {
            Item item = getItem(InventorySlot.getByIndex(i));
            if (item != null && item.getClass().equals(type)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public boolean canBeAdded(Item item) {
        for (int i = 0; i < InventorySlot.SLOTS_COUNT; i++) {
            InventorySlot inventorySlot = InventorySlot.getByIndex(i);
            if (canBePlacedInSlot(item, inventorySlot)) {
                if (getItem(inventorySlot) == null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Item dropItem(InventorySlot slot) {
        Item item = mContainer.remove(slot);
        if (item != null) {
            item.setContainer(null);
            notifyObserver();
            notifyChanged(item, slot, null);
            mUnit.dropItem(item);
        }

        return item;
    }

    @Override
    public void dropAll() {
        for (int i = 0; i < InventorySlot.SLOTS_COUNT; i++) {
            dropItem(InventorySlot.getByIndex(i));
        }
    }

    @Override
    public boolean canRemoveItem(Item item) {
        InventorySlot slot = getSlot(item);
        if (slot != null) {
            Item i = mContainer.get(slot);
            if (i == item) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(Item item) {
        InventorySlot slot = getSlot(item);
        item.setContainer(null);
        mContainer.remove(slot);
        notifyObserver();
        return true;
    }

    @Override
    public Item getItem(InventorySlot slot) {
        return mContainer.get(slot);
    }

    @Override
    public InventorySlot getSlot(Item item) {
        for (Map.Entry<InventorySlot, Item> entry : mContainer.entrySet()) {
            if (entry.getValue() == item) {
                return entry.getKey();
            }
        }
        return null;
    }

    public boolean backpackContains(Class<? extends Item> type) {
        for (Item item : mBackpack) {
            if (type.isAssignableFrom(item.getClass())) {
                return true;
            }
        }
        return false;
    }

    public List<Item> getBackpack() {
        return mBackpack;
    }

    private void rebuildBackpack() {
        mBackpack.clear();
        for (int i = InventorySlot.BACKPACK_1.getIndex();
             i <= InventorySlot.BACKPACK_6.getIndex(); i++) {
            Item item = mContainer.get(InventorySlot.getByIndex(i));
            if (item != null) {
                mBackpack.add(item);
            }
        }
    }

    public boolean isBackpackFull() {
        return mBackpack.size() == BACKPACK_SIZE;

    }

    //todo implement auto changing
    public boolean switchSlots(InventorySlot source, InventorySlot destination) {
        Item sourceItem = mContainer.get(source);
        Item destinationItem = mContainer.get(destination);
        boolean result = false;

        if (destinationItem == null && canBePlacedInSlot(sourceItem, destination)) {
            mContainer.remove(source);
            mContainer.put(destination, sourceItem);
            notifyChanged(sourceItem, source, destination);
            result = true;
        } else if (sourceItem == null && canBePlacedInSlot(destinationItem, source)) {
            mContainer.remove(destination);
            mContainer.put(source, destinationItem);
            notifyChanged(destinationItem, destination, source);
            result = true;
        } else if (canBePlacedInSlot(sourceItem, destination) && canBePlacedInSlot(destinationItem, source)) {
            mContainer.remove(source);
            mContainer.remove(destination);
            mContainer.put(source, destinationItem);
            mContainer.put(destination, sourceItem);
            notifyChanged(sourceItem, source, destination);
            notifyChanged(destinationItem, destination, source);
            result = true;
        }

        if (result) {
            notifyObserver();
        }
        return result;
    }

    @Override
    public boolean canBePlacedInSlot(Item item, InventorySlot inventorySlot) {
        if (item == null || inventorySlot == null) {
            return false;
        }

        //handle two hand tool not placing in offhand and when offhand slot is taken
        if (item instanceof TwoHandTool) {
            if ((inventorySlot == InventorySlot.OFFHAND_TOOL) ||
                    (inventorySlot == InventorySlot.MAINHAND_TOOL && getItem(InventorySlot.OFFHAND_TOOL) != null)) {
                return false;
            }
        }

        //handle offhand tool not placing in main hand
        if (item instanceof OffHandTool) {
            if (inventorySlot == InventorySlot.MAINHAND_TOOL) {
                return false;
            }
        }

        //handle offhand slot can't be taken when two hand tool is equipped
        if (inventorySlot == InventorySlot.OFFHAND_TOOL) {
            if (getItem(InventorySlot.MAINHAND_TOOL) != null && getItem(InventorySlot.MAINHAND_TOOL) instanceof TwoHandTool) {
                return false;
            }
        }

        return inventorySlot.getClassType().isAssignableFrom(item.getClass());
    }

    @Override
    protected void notifyObserver() {
        super.notifyObserver();
        rebuildBackpack();
    }
}
