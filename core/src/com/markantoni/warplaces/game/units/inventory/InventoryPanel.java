package com.markantoni.warplaces.game.units.inventory;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.Draggable;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.game.buildings.storage.Storage;
import com.markantoni.warplaces.game.container.Container;
import com.markantoni.warplaces.game.container.ContainerPanel;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 27.01.16.
 */
public class InventoryPanel extends ContainerPanel<Inventory, Unit> {
    private static final int WIDTH = SizeUnit.uiX(1);
    private static final int HEIGHT = SizeUnit.uiY(1);
    private static final int PADDING = SizeUnit.uiUnit(.1f);

    private Table mTable;
    private Table mCharacterTable;
    private Table mBackpackTable;
    private List<VisTextButton> mButtons;

    public InventoryPanel(GameScreen gameScreen) {
        super(gameScreen, InventoryPanel.class.getName(), 0, false, false);

        //init dragging
        for (VisTextButton but : mButtons) {
            but.setFocusBorderEnabled(false);
            Draggable draggable = new Draggable(this);
            draggable.attachTo(but);
            draggable.setBlockInput(true);
        }
    }

    @Override
    protected Table createTable() {
        setTitle(Strings.get("inventory"));
        mTable = new Table();
        mButtons = new ArrayList<VisTextButton>();

        mCharacterTable = new Table();
        mCharacterTable.defaults().pad(PADDING);

        VisTextButton button;
        button = new VisTextButton("");
        button.setUserObject(InventorySlot.HELMET);
        VisLabel helmetLabel = new VisLabel(Strings.get("helmet"), Align.center);
        mCharacterTable.add(helmetLabel).colspan(2).pad(0).row();
        mCharacterTable.add(button).colspan(2).size(WIDTH, HEIGHT).row();
        mButtons.add(button);

        button = new VisTextButton("");
        button.setUserObject(InventorySlot.ARMOR);
        VisLabel armorLabel = new VisLabel(Strings.get("armor"), Align.center);
        mButtons.add(button);
        mCharacterTable.add(armorLabel).colspan(2).pad(0).row();
        mCharacterTable.add(button).colspan(2).size(WIDTH, HEIGHT).row();

        VisLabel mainToolLabel = new VisLabel(Strings.get("main_tool"), Align.center);
        VisLabel offhandToolLabel = new VisLabel(Strings.get("secondary_tool"), Align.center);
        mCharacterTable.add(mainToolLabel).pad(0);
        mCharacterTable.add(offhandToolLabel).pad(0).row();

        button = new VisTextButton("");
        button.setUserObject(InventorySlot.MAINHAND_TOOL);
        mButtons.add(button);
        mCharacterTable.add(button).size(WIDTH, HEIGHT);

        button = new VisTextButton("");
        button.setUserObject(InventorySlot.OFFHAND_TOOL);
        mButtons.add(button);
        mCharacterTable.add(button).size(WIDTH, HEIGHT);

        mBackpackTable = new Table();
        mBackpackTable.defaults().pad(PADDING);
        VisLabel backpackLabel = new VisLabel(Strings.get("backpack"), Align.center);
        int columnSize = 2;

        mBackpackTable.add(backpackLabel).colspan(2).pad(0).row();
        for (int i = 0, index = InventorySlot.BACKPACK_1.getIndex();
             i < Inventory.BACKPACK_SIZE; i++, index++) {

            button = new VisTextButton("");
            button.setUserObject(InventorySlot.getByIndex(index));
            mButtons.add(button);
            mBackpackTable.add(button).size(WIDTH, HEIGHT);

            if (i > 0 && i % columnSize != 0) {
                mBackpackTable.row();
            }
        }

        handleContainer(null);
        mTable.add(mCharacterTable);
        mTable.add(mBackpackTable).align(Align.bottom).row();

        return mTable;
    }

    @Override
    public void showContainer(Unit unit) {
        setVisible(true);
        setContainer(unit.getInventory());
    }

    @Override
    protected void handleContainer(Inventory container) {
        for (VisTextButton button : mButtons) {
            Item item = null;

            if (mContainer != null) {
                item = mContainer.getItem((InventorySlot) button.getUserObject());
            }

            Sprite sprite = null;
            if (item != null) {
                sprite = item.getPreviewSprite();
            }
            updateButtonDrawable(button, sprite);
        }
    }

    @Override
    protected boolean drag(Object object) {
        return mContainer.getItem((InventorySlot) object) != null;
    }

    @Override
    protected boolean drop(Object srcSlot, Object destSlot, Container container) {
        if (container instanceof Storage) {
            Storage storage = (Storage) container;
            Item src = storage.getItem((Integer) srcSlot);

            if (mContainer.canBePlacedInSlot(src, (InventorySlot) destSlot)) {
                Item dest = mContainer.getItem((InventorySlot) destSlot);

                if (dest == null) {
                    //if slot in inventory == null, just place item in it and remove 1 item from storage
                    storage.removeItem(src); //remove item from storage
                    if (!mContainer.addItem(src, (InventorySlot) destSlot)) { //place item
                        storage.addItem(src, (Integer) srcSlot); //if it doesn't place, return to it's position
                        return false;
                    }
                    return true;
                } else {
                    //remove items from containers
                    storage.removeItem(src);
                    mContainer.removeItem(dest);

                    //if storage can't add item to proper slot
                    if (!storage.addItem(dest, (Integer) srcSlot)) {
                        //just add it to first slot
                        if (!storage.addItem(dest)) {
                            //if eventually can't add item to any slot in storage, return items back
                            storage.addItem(src, (Integer) srcSlot);
                            mContainer.addItem(dest, (InventorySlot) destSlot);
                            return false;
                        }
                    }

                    mContainer.addItem(src, (InventorySlot) destSlot);
                    return true;
                }
            }
        } else {
            return mContainer.switchSlots((InventorySlot) srcSlot, (InventorySlot) destSlot);
        }
        return false;
    }
}
