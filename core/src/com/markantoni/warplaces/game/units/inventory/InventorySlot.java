package com.markantoni.warplaces.game.units.inventory;

import com.markantoni.warplaces.game.items.armor.Armor;
import com.markantoni.warplaces.game.items.helmets.Helmet;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.Tool;

/**
 * Created by Mark on 29.01.2016.
 */
public final class InventorySlot {
    public static final int SLOTS_COUNT = 10;
    public static final InventorySlot MAINHAND_TOOL = new InventorySlot(Tool.class, 0);
    public static final InventorySlot OFFHAND_TOOL = new InventorySlot(Tool.class, 1);
    public static final InventorySlot ARMOR = new InventorySlot(Armor.class, 2);
    public static final InventorySlot HELMET = new InventorySlot(Helmet.class, 3);
    public static final InventorySlot BACKPACK_1 = new InventorySlot(Item.class, 4);
    public static final InventorySlot BACKPACK_2 = new InventorySlot(Item.class, 5);
    public static final InventorySlot BACKPACK_3 = new InventorySlot(Item.class, 6);
    public static final InventorySlot BACKPACK_4 = new InventorySlot(Item.class, 7);
    public static final InventorySlot BACKPACK_5 = new InventorySlot(Item.class, 8);
    public static final InventorySlot BACKPACK_6 = new InventorySlot(Item.class, 9);

    private final Class<? extends Item> mClass;
    private final int mIndex;

    private InventorySlot(Class<? extends Item> mClass, int index) {
        this.mClass = mClass;
        mIndex = index;
    }

    public Class<? extends Item> getClassType() {
        return mClass;
    }

    public int getIndex() {
        return mIndex;
    }

    public static InventorySlot getByIndex(int index) {
        if (index == MAINHAND_TOOL.getIndex()) {
            return MAINHAND_TOOL;
        }
        if (index == OFFHAND_TOOL.getIndex()) {
            return OFFHAND_TOOL;
        }
        if (index == ARMOR.getIndex()) {
            return ARMOR;
        }
        if (index == HELMET.getIndex()) {
            return HELMET;
        }
        if (index == BACKPACK_1.getIndex()) {
            return BACKPACK_1;
        }
        if (index == BACKPACK_2.getIndex()) {
            return BACKPACK_2;
        }
        if (index == BACKPACK_3.getIndex()) {
            return BACKPACK_3;
        }
        if (index == BACKPACK_4.getIndex()) {
            return BACKPACK_4;
        }
        if (index == BACKPACK_5.getIndex()) {
            return BACKPACK_5;
        }
        if (index == BACKPACK_6.getIndex()) {
            return BACKPACK_6;
        }
        return null;
    }

    @Override
    public int hashCode() {
        return mIndex;
    }
}
