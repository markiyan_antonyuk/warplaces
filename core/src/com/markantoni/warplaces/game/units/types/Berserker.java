package com.markantoni.warplaces.game.units.types;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.items.tools.onehand.Sword;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 16.02.16.
 */
class Berserker extends UnitType {
    private Sprite mSprite = new Sprite(Utils.combineTextures(
            Assets.Items.TWO_SWORDS.TEXTURE,
            Assets.Units.UNIT_PREVIEW.TEXTURE));

    public Berserker(Nation nation) {
        super(nation, null);
    }

    @Override
    public String getName() {
        return Strings.get("berserker");
    }

    @Override
    protected boolean isValid(Class<? extends Tool> mainTool, Class<? extends Tool> offhandTool) {
        if (mainTool == null || offhandTool == null) {
            return false;
        }
        return mainTool.equals(Sword.class) && offhandTool.equals(Sword.class);
    }

    @Override
    public boolean isPreferredType(Class<? extends Item> type) {
        return true;
    }

    @Override
    public void handleAction(Unit unit) {
    }

    @Override
    public void handleTarget(GameObject target, Unit unit, float deltaTime) {
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }
}