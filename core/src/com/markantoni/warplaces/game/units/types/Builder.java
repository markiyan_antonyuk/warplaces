package com.markantoni.warplaces.game.units.types;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.buildings.Building;
import com.markantoni.warplaces.game.buildings.Construction;
import com.markantoni.warplaces.game.buildings.Warehouse;
import com.markantoni.warplaces.game.buildings.storage.Storage;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.items.tools.twohand.Hammer;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectFinder;
import com.markantoni.warplaces.game.resources.ResourceSet;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.inventory.Inventory;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Mark on 26.02.2016.
 */
class Builder extends UnitType {
    private Sprite mSprite = new Sprite(Utils.combineTextures(
            Assets.Items.HAMMER.TEXTURE,
            Assets.Units.UNIT_PREVIEW.TEXTURE
    ));
    private Construction mTargetConstruction;

    public Builder(Nation nation) {
        super(nation, null);
    }

    @Override
    protected boolean isValid(Class<? extends Tool> mainTool, Class<? extends Tool> offhandTool) {
        if (mainTool == null) {
            return false;
        }
        return mainTool.equals(Hammer.class);
    }

    @Override
    public boolean isPreferredType(Class<? extends Item> type) {
        return ResourceItem.class.isAssignableFrom(type);
    }

    @Override
    public void handleAction(Unit unit) {
        GameObjectFinder finder = unit.getGameObjectFinder();
        if (mTargetConstruction != null && !mTargetConstruction.getBuilding().isUnderConstruction()) {
            mTargetConstruction = null;
        }

        //check if there is target construction
        if (mTargetConstruction == null) {
            //find all constructions
            Set<GameObject> constructions = finder.findAll(unit, Unit.SIGHT_RADIUS, mConstructionFilter);
            for (GameObject gameObject : constructions) {
                Construction construction = (Construction) gameObject;
                ResourceSet resourceSet = construction.getNeededResources();

                //check if unit has items in inventory to complete construction
                for (Item item : unit.getInventory().getBackpack()) {
                    if (resourceSet.getResources().containsKey(item.getClass())) {
                        mTargetConstruction = construction;
                        break;
                    }
                }
            }

            //if there is no items, set closest construction as target
            if (mTargetConstruction == null) {
                if (constructions.iterator().hasNext()) {
                    mTargetConstruction = (Construction) constructions.iterator().next();
                }
            }
        }

        //if still no target construction, don't do anything
        if (mTargetConstruction == null) {
            return;
        }

        //if backpack is full, go to construction
        if (unit.getInventory().isBackpackFull()) {
            unit.setTarget(mTargetConstruction.getBuilding());
            return;
        }

        //filter needed items and storages which contains needed items
        final List<Class<? extends Item>> types = new ArrayList<Class<? extends Item>>(mTargetConstruction.getNeededResources().getResources().keySet());
        List<GameObjectFinder.Filter> filters = new ArrayList<GameObjectFinder.Filter>();
        //filter storage that contains needed items
        filters.add(new GameObjectFinder.Filter(StorageBuilding.class) {
            @Override
            public boolean apply(GameObject object) {
                if (((Building) object).isUnderConstruction()) {
                    return false;
                }

                Storage storage = ((StorageBuilding) object).getStorage();
                for (Class<? extends Item> type : types) {
                    if (storage.contains(type)) {
                        return true;
                    }
                }
                return false;
            }
        });
        //add filter for all needed items
        for (Class<? extends Item> type : types) {
            filters.add(new GameObjectFinder.Filter(type));
        }

        GameObject resources = finder.find(unit, Unit.SIGHT_RADIUS, filters.toArray(new GameObjectFinder.Filter[filters.size()]));
        if (resources != null) {
            unit.setTarget(resources);
        }
    }

    @Override
    public void handleTarget(GameObject target, Unit unit, float deltaTime) {
        if (mTargetConstruction == null) {
            return;
        }

        boolean isConstruction = false;

        if (target instanceof Building) {
            isConstruction = ((Building) target).isUnderConstruction();
        }

        Inventory inventory = unit.getInventory();

        if (isConstruction) {
            Construction construction = ((Building) target).getConstruction();
            if (mTargetConstruction != construction) {
                mTargetConstruction = construction;
            }

            for (Class<? extends Item> type : new ArrayList<Class<? extends Item>>(mTargetConstruction.getNeededResources().getResources().keySet())) {
                for (int i = mTargetConstruction.getNeededResources().getResources().get(type) - 1; i >= 0; i++) {
                    //get first needed item from inventory
                    Item item = inventory.getFirst(type);
                    if (item == null) {
                        break;
                    }

                    if (mTargetConstruction.addResource(item)) {
                        inventory.removeItem(item);
                    } else {
                        break;
                    }
                }
            }

            if (mTargetConstruction.isFinished()) {
                mTargetConstruction = null;
            }
            unit.setState(Unit.State.IDLE);
            return;
        }

        if (target instanceof StorageBuilding) {
            Storage storage = ((StorageBuilding) target).getStorage();
            //iterate through all needed resources
            for (Class<? extends Item> type : mTargetConstruction.getNeededResources().getResources().keySet()) {
                for (int i = 0; i < mTargetConstruction.getNeededResources().getResources().get(type); i++) {
                    //get first needed item from storage
                    Item item = storage.getFirst(type);
                    if (item == null) {
                        break;
                    }

                    //check if item can be added to inventory
                    if (inventory.canBeAdded(item)) {
                        storage.removeItem(item);
                        inventory.addItem(item);
                    } else if (inventory.isBackpackFull()) { //if cannot - means inventory is full, return
                        unit.setState(Unit.State.IDLE);
                        return;
                    }
                }
            }
        }
    }

    @Override
    public String getName() {
        return Strings.get("builder");
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }

    @Override
    public Action getAction(int actionIndex) {
        if (actionIndex == 0) {
            return mBuildWarehouseAction;
        }
        return super.getAction(actionIndex);
    }

    private final Action mBuildWarehouseAction = new Action(true) {
        @Override
        public boolean performDnd(ActionsPerformer actionsPerformer, float x, float y) {
            Construction construction = getBuildingsManager().createConstruction(Warehouse.class, x, y);
            mTargetConstruction = construction;
            return construction != null;
        }

        @Override
        public Sprite getPreview() {
            return Assets.Buildings.WAREHOUSE.SPRITE;
        }
    };

    private final GameObjectFinder.Filter mConstructionFilter = new GameObjectFinder.Filter(Construction.class);
}
