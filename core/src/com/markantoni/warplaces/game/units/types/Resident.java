package com.markantoni.warplaces.game.units.types;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 16.02.16.
 */
class Resident extends UnitType {
    public Resident(Nation nation) {
        super(nation, null);
    }

    private Sprite mSprite = new Sprite(Utils.combineTextures(Assets.Units.UNIT_PREVIEW.TEXTURE));

    @Override
    public String getName() {
        return Strings.get("resident");
    }

    @Override
    protected boolean isValid(Class<? extends Tool> mainTool, Class<? extends Tool> offhandTool) {
        return mainTool == null;
    }

    @Override
    public boolean isPreferredType(Class<? extends Item> type) {
        return true;
    }

    @Override
    public void handleAction(Unit unit) {
    }

    @Override
    public void handleTarget(GameObject target, Unit unit, float deltaTime) {
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }

    @Override
    public Action getAction(int actionIndex) {
        //remove action to pick up only preferred items, residents will pick all items
        if (actionIndex == 10) {
            return null;
        }
        return super.getAction(actionIndex);
    }
}