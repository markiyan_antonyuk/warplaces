package com.markantoni.warplaces.game.units.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectManager;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.inventory.Inventory;
import com.markantoni.warplaces.game.units.inventory.InventorySlot;
import com.markantoni.warplaces.game.units.types.collector.Farmer;
import com.markantoni.warplaces.game.units.types.collector.Forester;
import com.markantoni.warplaces.game.units.types.collector.Stoneminer;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Rect;
import com.markantoni.warplaces.utils.Renderer;

/**
 * Created by mark on 11.02.16.
 */
//todo in future, may check if tools are assignable from classes such as (BronzeSword.class is assignable from Sword.class)
public class UnitType extends GameObject implements Disposable, ActionsPerformer {
    private final UnitType[] mTypes;
    private final Unit mUnit;
    private UnitType mType = null;
    private boolean mIsPickingOnlyPreferred = true;

    public UnitType(Nation nation, Unit unit) {
        super(nation.getGameScreen(), nation);
        if (unit != null) {
            mUnit = unit;
            mTypes = new UnitType[]{
                    new Knight(nation),
                    new Berserker(nation),
                    new Swordsman(nation),
                    new Builder(nation),
                    new Farmer(nation),
                    new Forester(nation),
                    new Stoneminer(nation),
                    new Resident(nation)
            };
            reinitialize();
        } else {
            //initing lower level
            mUnit = null;
            mTypes = null;
        }
    }

    public final void reinitialize() {
        Inventory inventory = mUnit.getInventory();
        Class<? extends Tool> mainTool = null;
        Class<? extends Tool> offhandTool = null;

        if (inventory.getItem(InventorySlot.MAINHAND_TOOL) != null) {
            mainTool = (Class<? extends Tool>) inventory.getItem(InventorySlot.MAINHAND_TOOL).getClass();
        }

        if (inventory.getItem(InventorySlot.OFFHAND_TOOL) != null) {
            offhandTool = (Class<? extends Tool>) inventory.getItem(InventorySlot.OFFHAND_TOOL).getClass();
        }

        for (UnitType type : mTypes) {
            if (type.isValid(mainTool, offhandTool)) {
                mType = type;
                return;
            }
        }
        mType = mTypes[mTypes.length - 1];
    }

    @Override
    public final void dispose() {
        for (UnitType type : mTypes) {
            type.getPreviewSprite().getTexture().dispose();
        }
    }

    protected boolean isValid(Class<? extends Tool> mainTool, Class<? extends Tool> offhandTool) {
        return mType.isValid(mainTool, offhandTool);
    }

    public boolean isPreferredType(Class<? extends Item> type) {
        return !mIsPickingOnlyPreferred || mType.isPreferredType(type);
    }

    public void handleAction(Unit unit) {
        mType.handleAction(unit);
    }

    public void handleTarget(GameObject target, Unit unit, float deltaTime) {
        mType.handleTarget(target, unit, deltaTime);
    }

    @Override
    public String getName() {
        return mType.getName();
    }

    @Override
    public Sprite getPreviewSprite() {
        return mType.getPreviewSprite();
    }

    @Override
    public Action getAction(int actionIndex) {
        switch (actionIndex) {
            case 10:
                return mPickUpOnlyPreferredItems;
        }
        if (mType != null) {
            return mType.getAction(actionIndex);
        }
        return null;
    }

    //-------------------- actions --------------------//

    private final Action mPickUpOnlyPreferredItems = new Action(false) {
        @Override
        public void perform(ActionsPerformer actionsPerformer) {
            mIsPickingOnlyPreferred = !mIsPickingOnlyPreferred;
        }

        @Override
        public Sprite getPreview() {
            return mIsPickingOnlyPreferred ? Assets.Actions.ONLY_PREFERRED.SPRITE : Assets.Actions.ALL_ITEMS.SPRITE;
        }
    };

    //----------------- stub methods ------------------//

    @Override
    public Color getColor() {
        return null;
    }

    @Override
    public final void renderSelected(Renderer renderer) {
    }

    @Override
    public final Vector2 getIsometricPosition() {
        return null;
    }

    @Override
    public final Rect getBounds() {
        return null;
    }

    @Override
    public final void update(float deltaTime) {
    }

    @Override
    public final void render(Renderer renderer) {
    }

    @Override
    public final GameObjectManager getManager() {
        return null;
    }

    @Override
    public final GameObject getHolder() {
        return null;
    }
}
