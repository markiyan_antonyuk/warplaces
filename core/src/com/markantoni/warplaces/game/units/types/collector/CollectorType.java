package com.markantoni.warplaces.game.units.types.collector;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.actions.Action;
import com.markantoni.warplaces.game.actions.ActionsPerformer;
import com.markantoni.warplaces.game.buildings.storage.Storage;
import com.markantoni.warplaces.game.buildings.storage.StorageBuilding;
import com.markantoni.warplaces.game.items.Item;
import com.markantoni.warplaces.game.items.resources.ResourceItem;
import com.markantoni.warplaces.game.items.tools.Tool;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.objects.GameObject;
import com.markantoni.warplaces.game.objects.GameObjectFinder;
import com.markantoni.warplaces.game.resources.objects.Resource;
import com.markantoni.warplaces.game.units.Unit;
import com.markantoni.warplaces.game.units.inventory.Inventory;
import com.markantoni.warplaces.game.units.types.UnitType;
import com.markantoni.warplaces.utils.Assets;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 22.02.16.
 */
public abstract class CollectorType extends UnitType {
    private final Class<? extends StorageBuilding> mTargetStorage;
    private final Class<? extends Resource> mTargetResource;
    private final Class<? extends ResourceItem> mTargetResourceItem;
    private final Class<? extends Tool> mMainTool;
    private final GameObjectFinder.Filter mStorageFilter;
    private final GameObjectFinder.Filter mResourceFilter;
    private final GameObjectFinder.Filter mResourceItemFilter;
    private boolean mIsWorking = true;

    public CollectorType(Nation nation,
                         Class<? extends StorageBuilding> targetStorage,
                         Class<? extends Resource> targetResource,
                         Class<? extends ResourceItem> targetResourceItem,
                         Class<? extends Tool> mainTool) {
        super(nation, null);
        mTargetStorage = targetStorage;
        mTargetResource = targetResource;
        mTargetResourceItem = targetResourceItem;
        mMainTool = mainTool;

        mResourceFilter = new GameObjectFinder.Filter(mTargetResource);
        mResourceItemFilter = new GameObjectFinder.Filter(mTargetResourceItem);
        mStorageFilter = new GameObjectFinder.Filter(mTargetStorage) {
            @Override
            public boolean apply(GameObject object) {
                return !((StorageBuilding) object).getStorage().isFull();
            }
        };
    }

    @Override
    protected boolean isValid(Class<? extends Tool> mainTool, Class<? extends Tool> offhandTool) {
        if (mainTool == null) {
            return false;
        }
        return mainTool.equals(mMainTool);
    }

    @Override
    public boolean isPreferredType(Class<? extends Item> type) {
        return type.isAssignableFrom(mTargetResourceItem);
    }

    @Override
    public void handleAction(Unit unit) {
        if (!mIsWorking) {
            return;
        }

        StorageBuilding storageBuilding = (StorageBuilding) getGameObjectFinder().find(unit, Unit.SIGHT_RADIUS, mStorageFilter);
        GameObject resource = getGameObjectFinder().find(unit, Unit.SIGHT_RADIUS, mResourceItemFilter, mResourceFilter);

        if (unit.getInventory().backpackContains(mTargetResourceItem)) {
            if (storageBuilding != null && !storageBuilding.getStorage().isFull()) {

                if (unit.getInventory().isBackpackFull()) {
                    unit.setTarget(storageBuilding);
                    return;
                }

                if (resource != null) {
                    GameObject target = resource.getCenter().dst(unit.getCenter()) < storageBuilding.getCenter().dst(unit.getCenter()) ? resource : storageBuilding;
                    unit.setTarget(target);
                    return;
                }

            }

            if (!unit.getInventory().isBackpackFull() && resource != null) {
                unit.setTarget(resource);
                return;
            }

        } else if (!unit.getInventory().isBackpackFull()) {
            unit.setTarget(resource);
        }
    }

    @Override
    public void handleTarget(GameObject target, Unit unit, float deltaTime) {
        StorageBuilding storageBuilding = unit.getCollidedStorage();
        if (target == storageBuilding) {
            unit.stopMovement();

            List<Item> itemsToRemove = new ArrayList<Item>();
            Inventory inventory = unit.getInventory();
            Storage storage = storageBuilding.getStorage();

            for (Item item : inventory.getBackpack()) {
                if (!storage.isFull()) {
                    if (isPreferredType(item.getClass()) && inventory.canRemoveItem(item)) {
                        itemsToRemove.add(item);
                        storage.addItem(item);
                    }
                }
            }

            for (Item item : itemsToRemove) {
                inventory.removeItem(item);
            }
            return;
        }

        if (!mIsWorking) { //if have items add the to storage, but don't collect any
            return;
        }

        if (target.getClass().equals(mTargetResource)) {
            Resource resource = (Resource) target;
            if (!unit.getInventory().isBackpackFull() && resource.handleCollect(unit, deltaTime)) {
                unit.setState(Unit.State.WORKING);
            } else {
                unit.setState(Unit.State.IDLE);
            }
            return;
        }
    }

    @Override
    public Action getAction(int actionIndex) {
        if (actionIndex == 9) {
            return mIsWorkingAction;
        }
        return super.getAction(actionIndex);
    }

    private final Action mIsWorkingAction = new Action(false) {
        @Override
        public void perform(ActionsPerformer actionsPerformer) {
            mIsWorking = !mIsWorking;
        }

        @Override
        public Sprite getPreview() {
            return mIsWorking ? Assets.Actions.WORK.SPRITE : Assets.Actions.DO_NOT_WORK.SPRITE;
        }
    };
}
