package com.markantoni.warplaces.game.units.types.collector;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.buildings.Warehouse;
import com.markantoni.warplaces.game.items.resources.WheatItem;
import com.markantoni.warplaces.game.items.tools.twohand.Scythe;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.resources.objects.Farm;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 16.02.16.
 */
public class Farmer extends CollectorType {
    private Sprite mSprite = new Sprite(Utils.combineTextures(
            Assets.Items.SCYTHE.TEXTURE,
            Assets.Units.UNIT_PREVIEW.TEXTURE
    ));

    public Farmer(Nation nation) {
        super(nation, Warehouse.class, Farm.class, WheatItem.class, Scythe.class);
    }

    @Override
    public String getName() {
        return Strings.get("farmer");
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }
}
