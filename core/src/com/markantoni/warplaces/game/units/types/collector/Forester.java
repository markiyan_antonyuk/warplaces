package com.markantoni.warplaces.game.units.types.collector;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.buildings.Warehouse;
import com.markantoni.warplaces.game.items.resources.WoodItem;
import com.markantoni.warplaces.game.items.tools.twohand.Axe;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.resources.objects.Forest;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 22.02.16.
 */
public class Forester extends CollectorType {
    private Sprite mSprite = new Sprite(Utils.combineTextures(
            Assets.Items.AXE.TEXTURE,
            Assets.Units.UNIT_PREVIEW.TEXTURE
    ));

    public Forester(Nation nation) {
        super(nation, Warehouse.class, Forest.class, WoodItem.class, Axe.class);
    }

    @Override
    public String getName() {
        return Strings.get("forester");
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }
}
