package com.markantoni.warplaces.game.units.types.collector;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.markantoni.warplaces.game.buildings.Warehouse;
import com.markantoni.warplaces.game.items.resources.StoneItem;
import com.markantoni.warplaces.game.items.tools.twohand.Pickaxe;
import com.markantoni.warplaces.game.nations.Nation;
import com.markantoni.warplaces.game.resources.objects.Rock;
import com.markantoni.warplaces.utils.Assets;
import com.markantoni.warplaces.utils.Strings;
import com.markantoni.warplaces.utils.Utils;

/**
 * Created by mark on 24.02.16.
 */
public class Stoneminer extends CollectorType {
    private Sprite mSprite = new Sprite(Utils.combineTextures(
            Assets.Items.PICKAXE.TEXTURE,
            Assets.Units.UNIT_PREVIEW.TEXTURE
    ));

    public Stoneminer(Nation nation) {
        super(nation, Warehouse.class, Rock.class, StoneItem.class, Pickaxe.class);
    }

    @Override
    public String getName() {
        return Strings.get("stoneminer");
    }

    @Override
    public Sprite getPreviewSprite() {
        return mSprite;
    }
}
