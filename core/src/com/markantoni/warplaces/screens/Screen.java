package com.markantoni.warplaces.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.Renderable;
import com.markantoni.warplaces.utils.Renderer;

/**
 * Created by mark on 29.12.15.
 */
public abstract class Screen implements Disposable, Renderable {
    private InputMultiplexer mInputMultiplexer;
    private Stage mStage;
    private Bundle mBundle;

    public void init(Bundle bundle) {
        mBundle = bundle;
        mInputMultiplexer = new InputMultiplexer();
        mStage = new Stage(new ScreenViewport());
        addInputSource(mStage);

        create();
        resume();

        mStage.addListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE) {
                    back();
                    return true;
                }
                return false;
            }
        });
    }

    protected void back() {
        ScreenStack.back();
    }

    protected Bundle getBundle() {
        if (mBundle == null) {
            mBundle = new Bundle();
        }
        return mBundle;
    }

    protected Table generateTable() {
        Table table = new Table();
        table.setFillParent(true);
        return table;
    }

    protected abstract void initUI(Stage stage);

    protected Stage getStage() {
        return mStage;
    }

    protected void addInputSource(InputProcessor inputProcessor) {
        mInputMultiplexer.addProcessor(inputProcessor);
    }

    protected abstract void create();

    public void resume() {
        Gdx.input.setInputProcessor(mInputMultiplexer);
    }

    public void resize(int width, int height) {
        mStage.getViewport().update(width, height, true);
    }

    public void update(float deltaTime) {
        mStage.act(deltaTime);
    }

    public void pause() {
    }

    @Override
    public void dispose() {
        mStage.dispose();
    }

    @Override
    public void render(Renderer renderer) {
        mStage.getBatch().enableBlending();
        mStage.draw();
    }
}
