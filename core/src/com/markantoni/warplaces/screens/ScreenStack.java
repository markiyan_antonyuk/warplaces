package com.markantoni.warplaces.screens;

import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.Pair;

import java.util.LinkedList;

/**
 * Created by mark on 30.12.15.
 */
public class ScreenStack {
    private static final ScreenStack sInstance = new ScreenStack();

    private LinkedList<Screen> mScreenStack = new LinkedList<Screen>();
    private LinkedList<Pair<Class<? extends Screen>, Bundle>> mScreenQueue = new LinkedList<Pair<Class<? extends Screen>, Bundle>>();
    private int mGoBackCounter;
    private boolean mClearStack;

    private ScreenStack() {
    }

    public static void open(Class<? extends Screen> screen, Bundle bundle) {
        sInstance.mScreenQueue.push(new Pair<Class<? extends Screen>, Bundle>(screen, bundle));
    }

    public static void open(Class<? extends Screen> screen) {
        open(screen, null);
    }

    public static void clearStack() {
        sInstance.mClearStack = true;
    }

    public static void back() {
        sInstance.mGoBackCounter++;
    }

    public static void process() {
        while (!sInstance.mScreenQueue.isEmpty()) {
            Screen screen = getTop();
            if (screen != null) {
                screen.pause();
            }

            Pair<Class<? extends Screen>, Bundle> pair = sInstance.mScreenQueue.pop();
            Class<? extends Screen> screenClass = pair.first;
            try {
                screen = screenClass.newInstance();
            } catch (Exception e) {
            }
            screen.init(pair.second);
            sInstance.mScreenStack.push(screen);
        }

        while (sInstance.mGoBackCounter > 0) {
            if (sInstance.mScreenStack.size() <= 1) {
                break;
            }
            Screen screen = sInstance.mScreenStack.pop();
            screen.pause();
            screen.dispose();
            resume();
            sInstance.mGoBackCounter--;
        }
        sInstance.mGoBackCounter = 0;

        if (sInstance.mClearStack) {
            Screen top = sInstance.mScreenStack.pop();
            for (Screen screen : sInstance.mScreenStack) {
                screen.dispose();
            }
            sInstance.mScreenStack.clear();
            sInstance.mScreenStack.push(top);
            sInstance.mClearStack = false;
        }
    }

    public static void pause() {
        Screen screen = getTop();
        if (screen != null) {
            screen.pause();
        }
    }

    public static void resume() {
        Screen screen = getTop();
        if (screen != null) {
            screen.resume();
        }
    }

    public static Screen getTop() {
        if (!sInstance.mScreenStack.isEmpty()) {
            return sInstance.mScreenStack.getFirst();
        }
        return null;
    }


    public static void dispose() {
        for (Screen screen : sInstance.mScreenStack) {
            screen.dispose();
        }
    }

    public static void resize(int width, int height) {
        for (Screen screen : sInstance.mScreenStack) {
            screen.resize(width, height);
        }
    }
}
