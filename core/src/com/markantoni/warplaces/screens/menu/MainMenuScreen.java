package com.markantoni.warplaces.screens.menu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.WarPlaces;
import com.markantoni.warplaces.game.AbstractGameScreen;
import com.markantoni.warplaces.game.redactor.RedactorMenuScreen;
import com.markantoni.warplaces.game.sandbox.GameScreen;
import com.markantoni.warplaces.game.terrain.TerrainSize;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.utils.Bundle;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 15.01.16.
 */
public class MainMenuScreen extends MenuScreen {

    protected void initUI(Stage stage) {
        setBackButtonEnabled(false);

        Table table = generateTable();
        table.defaults().prefSize(SizeUnit.uiX(4), SizeUnit.uiY(1)).pad(SizeUnit.uiY(.1f));

        VisTextButton startGameButton = new VisTextButton(Strings.get("start_game"));
        VisTextButton redactorButton = new VisTextButton(Strings.get("terrain_redactor"));
        VisTextButton settingsButton = new VisTextButton(Strings.get("settings"));
        VisTextButton exitGameButton = new VisTextButton(Strings.get("exit"));

        table.center();
        table.add(startGameButton).row();
        table.add(redactorButton).row();
        table.add(settingsButton).row();
        if (WarPlaces.isDesktop()) {
            table.add(exitGameButton).row();
        }

        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(GameScreen.class, new Bundle().put(AbstractGameScreen.SIZE, TerrainSize.SMALL));
            }
        });

        redactorButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(RedactorMenuScreen.class);
            }
        });

        settingsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(SettingsScreen.class);
            }
        });

        exitGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                WarPlaces.exitGame();
            }
        });

        stage.addActor(table);
    }

    @Override
    public void resume() {
        super.resume();
        ScreenStack.clearStack();
    }
}