package com.markantoni.warplaces.screens.menu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.screens.Screen;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by mark on 15.01.16.
 */
public abstract class MenuScreen extends Screen {
    private VisLabel mTitle;
    private VisTextButton mBackButton;

    @Override
    protected void create() {
        Stage stage = getStage();
        Table table = generateTable();
        Table backButtonTable = generateTable();

        mBackButton = new VisTextButton(Strings.get("back"));
        mTitle = new VisLabel(Strings.get("war_places"));

        mTitle.setAlignment(Align.center, Align.center);
        mBackButton.align(Align.center).pad(SizeUnit.uiUnit(.1f));

        backButtonTable.top().left().add(mBackButton).prefWidth(SizeUnit.uiX(1)).prefHeight(SizeUnit.uiY(1));
        table.top().add(mTitle).prefWidth(SizeUnit.max()).prefHeight(SizeUnit.uiY(1));

        initUI(stage);
        stage.addActor(table);
        stage.addActor(backButtonTable);

        mBackButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.back();
            }
        });
    }

    protected void setTitle(String title) {
        mTitle.setText(title);
    }

    protected void setBackButtonEnabled(boolean enable) {
        mBackButton.setVisible(enable);
    }
}
