package com.markantoni.warplaces.screens.menu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.WarPlaces;
import com.markantoni.warplaces.game.ui.settings.HUDSettingsScreen;
import com.markantoni.warplaces.screens.ScreenStack;
import com.markantoni.warplaces.utils.SizeUnit;
import com.markantoni.warplaces.utils.Strings;

/**
 * Created by Mark on 10.02.2016.
 */
public class SettingsScreen extends MenuScreen {
    @Override
    protected void initUI(Stage stage) {
        setTitle(Strings.get("settings"));
        Table table = generateTable();
        table.defaults().prefSize(SizeUnit.uiX(4), SizeUnit.uiY(1)).pad(SizeUnit.uiUnit(.1f));

        //todo experimental
        final VisCheckBox fullscreen = new VisCheckBox(Strings.get("fullscreen"));
        fullscreen.setChecked(Settings.isFullscreen());
        fullscreen.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Settings.setFullscreen(fullscreen.isChecked());
            }
        });
        fullscreen.setFocusBorderEnabled(false);

        final VisCheckBox touchmode = new VisCheckBox(Strings.get("touch"));
        touchmode.setChecked(Settings.isInTouchmode());
        touchmode.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Settings.setTouchmode(touchmode.isChecked());
            }
        });
        touchmode.setFocusBorderEnabled(false);

        final VisCheckBox debug = new VisCheckBox(Strings.get("debug"));
        debug.setChecked(Settings.isDebug());
        debug.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Settings.setDebug(debug.isChecked());
            }
        });
        debug.setFocusBorderEnabled(false);

        VisTextButton hudSettings = new VisTextButton(Strings.get("hud_settings"));
        hudSettings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenStack.open(HUDSettingsScreen.class);
            }
        });
        hudSettings.setFocusBorderEnabled(false);

        table.add(hudSettings).row();
        if (WarPlaces.isDesktop()) {
            table.add(fullscreen).row();
        }
        table.add(touchmode).row();
        table.add(debug).row();

        stage.addActor(table);
    }
}
