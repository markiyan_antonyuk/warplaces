package com.markantoni.warplaces.utils;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Mark on 24.02.2016.
 */
public class Animation {
    private final TextureRegion[] mFrames;
    private final float mFrameDuration;
    private int mCurrentIndex;
    private float mTime;

    public Animation(float frameDuration, TextureRegion... frames) {
        mFrames = frames;
        mFrameDuration = frameDuration;
    }

    public void update(float deltaTime) {
        mTime += deltaTime;

        if (mTime > mFrameDuration) {
            mCurrentIndex++;
            mTime = 0;
        }

        if (mCurrentIndex >= mFrames.length) {
            mCurrentIndex = 0;
        }
    }

    public TextureRegion getFrame() {
        return mFrames[mCurrentIndex];
    }
}
