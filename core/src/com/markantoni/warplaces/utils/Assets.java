package com.markantoni.warplaces.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Disposable;

import java.lang.reflect.Field;

/**
 * Created by mark on 30.12.15.
 * Widget styles, Sprites, Textures, Fonts
 */
public final class Assets implements Disposable {
    private static Assets sInstance;
    public static final Drawable GRAY_DRAWABLE = new ColorDrawable(Color.GRAY);
    public static final Drawable DARK_GRAY_DRAWABLE = new ColorDrawable(Color.DARK_GRAY);
    public static final Drawable LIGHT_GRAY_DRAWABLE = new ColorDrawable(Color.LIGHT_GRAY);
    public static final Drawable WHITE_DRAWABLE = new ColorDrawable(Color.WHITE);
    public static final Drawable GREEN_DRAWABLE = new ColorDrawable(Color.GREEN);
    public static final Drawable BLACK_DRAWABLE = new ColorDrawable(Color.BLACK);
    public static final Drawable BROWN_DRAWABLE = new ColorDrawable(Color.BROWN);
    public static final Drawable DARK_BROWN_DRAWABLE = new ColorDrawable(Color.valueOf("893a04"));

    private Assets() {
    }

    public static Assets instance() {
        if (sInstance == null) {
            sInstance = new Assets();
        }
        return sInstance;
    }

    @Override
    public void dispose() {
        disposeHolder(Items.class);
        disposeHolder(Buildings.class);
        disposeHolder(Resources.class);
        disposeHolder(Terrain.class);
        disposeHolder(Units.class);
    }

    private void disposeHolder(Class<? extends AssetsHolder> assetsHolder) {
        for (Field field : assetsHolder.getFields()) {
            try {
                if (field.getClass().equals(Asset.class)) {
                    Object asset = field.get("this");
                    if (asset instanceof Asset) {
                        ((Asset) asset).dispose();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface Items extends AssetsHolder {
        String ITEMS_DIR = IMAGES_DIR + "items/";
        Asset ARMOR = new Asset(ITEMS_DIR + "armor.png");
        Asset AXE = new Asset(ITEMS_DIR + "axe.png");
        Asset BACKPACK = new Asset(ITEMS_DIR + "backpack.png");
        Asset HAMMER = new Asset(ITEMS_DIR + "hammer.png");
        Asset HELMET = new Asset(ITEMS_DIR + "helmet.png");
        Asset PICKAXE = new Asset(ITEMS_DIR + "pickaxe.png");
        Asset SCYTHE = new Asset(ITEMS_DIR + "scythe.png");
        Asset SHIELD = new Asset(ITEMS_DIR + "shield.png");
        Asset SWORD = new Asset(ITEMS_DIR + "sword.png");
        Asset STONE = new Asset(ITEMS_DIR + "stone.png");
        Asset TWO_SWORDS = new Asset(ITEMS_DIR + "two_swords.png");
        Asset WHEAT = new Asset(ITEMS_DIR + "wheat.png");
        Asset WOOD = new Asset(ITEMS_DIR + "wood.png");
    }

    public interface Buildings extends AssetsHolder {
        String BUILDINGS_DIR = IMAGES_DIR + "buildings/";
        Asset WAREHOUSE = new Asset(BUILDINGS_DIR + "warehouse.png");
        Asset WAREHOUSE_CONSTRUCTION = new Asset(BUILDINGS_DIR + "warehouse_construction.png", 4, 1);
        Asset CONSTRUCTION = new Asset(BUILDINGS_DIR + "construction.png");
    }

    public interface Resources extends AssetsHolder {
        String RESOURCES_DIR = IMAGES_DIR + "resources/";
        Asset FOREST = new Asset(RESOURCES_DIR + "forest.png");
        Asset FARM = new Asset(RESOURCES_DIR + "farm.png");
        Asset ROCK = new Asset(RESOURCES_DIR + "rock.png");
    }

    public interface Terrain extends AssetsHolder {
        String TERRAIN_DIR = IMAGES_DIR + "terrain/";
        Asset GRASS = new Asset(TERRAIN_DIR + "grass.png");
        Asset WATER = new Asset(TERRAIN_DIR + "water.png");
        Asset WATER_ANIMATED = new Asset(TERRAIN_DIR + "water_animated.png", 8, 1);
        Asset BEACH = new Asset(TERRAIN_DIR + "beach.png");
    }

    public interface Actions extends AssetsHolder {
        String ACTIONS_DIR = IMAGES_DIR + "actions/";
        Asset DESTROY = new Asset(ACTIONS_DIR + "destroy.png");
        Asset ONLY_PREFERRED = new Asset(ACTIONS_DIR + "only_preferred_debug.png");
        Asset ALL_ITEMS = new Asset(ACTIONS_DIR + "all_items_debug.png");
        Asset WORK = new Asset(ACTIONS_DIR + "work_debug.png");
        Asset DO_NOT_WORK = new Asset(ACTIONS_DIR + "do_not_work_debug.png");
    }

    public interface Units extends AssetsHolder {
        String UNITS_DIR = IMAGES_DIR + "units/";
        Asset AXE_EQUIPPED = new Asset(UNITS_DIR + "axe_equipped.png", true);
        Asset HAMMER_EQUIPPED = new Asset(UNITS_DIR + "hammer_equipped.png", true);
        Asset PICKAXE_EQUIPPED = new Asset(UNITS_DIR + "pickaxe_equipped.png", true);
        Asset SCYTHE_EQUIPPED = new Asset(UNITS_DIR + "scythe_equipped.png", true);
        Asset SHIELD_EQUIPPED = new Asset(UNITS_DIR + "shield_equipped.png", true);
        Asset SWORD_EQUIPPED_MAIN_HAND = new Asset(UNITS_DIR + "sword_equipped_main_hand.png", true);
        Asset SWORD_EQUIPPED_OFF_HAND = new Asset(UNITS_DIR + "sword_equipped_off_hand.png", true);
        Asset UNIT = new Asset(UNITS_DIR + "unit.png", true);
        Asset UNIT_CLOTHES = new Asset(UNITS_DIR + "unit_clothes.png", true);
        Asset UNIT_PREVIEW = new Asset(UNITS_DIR + "unit_preview.png");
    }

    private interface AssetsHolder {
        String IMAGES_DIR = "images/";
    }

    public interface Extensions {
        String TERRAIN = ".wpter";
    }

    public static class Asset implements Disposable {
        public final Texture TEXTURE;
        public final Sprite SPRITE;
        public final TextureRegion TEXTURE_REGION[][];

        private Asset(String path) {
            this(path, false);
        }

        private Asset(String path, int regionWidth, int regionHeight) {
            this(path, true, regionWidth, regionHeight);
        }

        private Asset(String path, boolean generateAsset) {
            this(path, generateAsset, 4, 2);
        }

        private Asset(String path, boolean generateRegion, int regionWidth, int regionHeight) {
            TEXTURE = new Texture(path);
            SPRITE = new Sprite(TEXTURE);
            if (generateRegion) {
                TEXTURE_REGION = TextureRegion.split(TEXTURE, TEXTURE.getWidth() / regionWidth, TEXTURE.getHeight() / regionHeight);
            } else {
                TEXTURE_REGION = null;
            }
        }

        @Override
        public void dispose() {
            TEXTURE.dispose();
        }
    }
}
