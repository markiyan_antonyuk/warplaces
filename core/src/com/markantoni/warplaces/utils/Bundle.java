package com.markantoni.warplaces.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mark on 22.01.16.
 */
public class Bundle {
    private Map<String, Object> mMap = new HashMap<String, Object>();

    public Bundle() {
    }

    public Bundle put(String key, Object value) {
        mMap.put(key, value);
        return this;
    }

    public <T extends Object> T get(String key, T defValue) {
        if(mMap.containsKey(key)) {
            Object object = mMap.get(key);
            try {
                return (T) object;
            } catch (Exception e) {
                Log.d(Bundle.class, "can't get value by key: " + key + ", wrong type");
            }
        }
        return defValue;
    }
}
