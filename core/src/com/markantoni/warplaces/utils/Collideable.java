package com.markantoni.warplaces.utils;

/**
 * Created by mark on 26.01.16.
 */
public interface Collideable {
    /**
     * @return false if collision will be handled by box2d
     */
    boolean willCollide(Object another);

    void startCollision(Object another);

    void endCollision(Object another);
}
