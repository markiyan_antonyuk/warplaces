package com.markantoni.warplaces.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by mark on 20.01.16.
 */
public class ColorDrawable extends BaseDrawable implements Disposable {
    public static final Color TRANSPARENT = Color.valueOf("000000FF");
    private Texture mTexture;

    public ColorDrawable(Color color) {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGB888);
        pixmap.setColor(color);
        pixmap.fill();
        mTexture = new Texture(pixmap);
    }

    @Override
    public void draw(Batch batch, float x, float y, float width, float height) {
        batch.draw(mTexture, x, y, width, height);
    }

    @Override
    public void dispose() {
        mTexture.dispose();
    }
}
