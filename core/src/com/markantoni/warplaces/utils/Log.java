package com.markantoni.warplaces.utils;

import com.badlogic.gdx.Gdx;
import com.markantoni.warplaces.WarPlaces;

/**
 * Created by mark on 30.12.15.
 */
public class Log {
    public static void d(Object instance, Object message) {
        d(WarPlaces.class, instance, message);
    }

    public static void d(Object message) {
        d(WarPlaces.class, message);
    }

    public static void d(String tag, Object message) {
        Gdx.app.log(tag, message + "");
    }

    public static void d(Class<?> classTag, Object message) {
        Gdx.app.log(classTag.getSimpleName(), message + "");
    }

    public static void d(String tag, Object instance, Object message) {
        Gdx.app.log(tag, instance + ": " + message + "");
    }

    public static void d(Class<?> classTag, Object instance, Object message) {
        Gdx.app.log(classTag.getSimpleName(), instance + ": " + message + "");
    }
}
