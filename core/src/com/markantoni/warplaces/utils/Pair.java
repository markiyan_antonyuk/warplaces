package com.markantoni.warplaces.utils;

/**
 * Created by mark on 30.12.15.
 */
public class Pair<F, S> {
    public final F first;
    public final S second;

    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }
}
