package com.markantoni.warplaces.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.markantoni.warplaces.game.terrain.Terrain;

/**
 * Created by mark on 26.01.16.
 */
public class PhysicUtils {
    private final static Vector2 tempVector = new Vector2();

    public static void createTerrainBody(World world, Terrain terrain) {
        Rect bounds = terrain.getRealSize();

        createStaticBody(world, bounds.x, bounds.y, 1, bounds.height);
        createStaticBody(world, bounds.x, bounds.y, bounds.width, 1);
        createStaticBody(world, bounds.x + bounds.width, bounds.y, 1, bounds.height);
        createStaticBody(world, bounds.x, bounds.y + bounds.height, bounds.width, 1);
    }

    public static Body createStaticBody(World world, float x, float y, float width, float height) {
        return createStaticBody(world, x, y, width, height, 0);
    }

    public static Body createStaticBody(World world, float x, float y, Vector2[] edges) {
        Body body;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x, y);

        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(edges);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);
        polygonShape.dispose();

        return body;
    }

    public static Body createStaticBody(World world, float x, float y, float width, float height, float angle) {
        Body body;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x, y);
        bodyDef.angle = angle;

        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width / 2, height / 2, new Vector2(width / 2, height / 2), 0);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);
        polygonShape.dispose();

        return body;
    }

    public static Body createBody(World world, float x, float y, float width, float height) {
        return createBody(world, x, y, width, height, 0);
    }

    public static Body createBody(World world, float x, float y, float width, float height, float angle) {
        Body body;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.fixedRotation = true;
        bodyDef.angle = angle;
        bodyDef.active = true;

        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width / 2, height / 2, new Vector2(width / 2, height / 2), 0);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.restitution = -1;
        body.createFixture(fixtureDef);
        polygonShape.dispose();

        return body;
    }

    public static Body createBody(World world, float x, float y, Vector2[] edges) {
        Body body;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.fixedRotation = true;

        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(edges);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.restitution = -1;
        body.createFixture(fixtureDef);
        polygonShape.dispose();

        return body;
    }

    public static Vector2 calculateDirection(Vector2 position, Vector2 destination, float speed) {
        return tempVector.set(destination).sub(position).nor().scl(speed);
    }

    public static void registerContactListener(World world) {
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Object first = contact.getFixtureA().getUserData();
                Object second = contact.getFixtureB().getUserData();

                if (first instanceof Collideable) {
                    ((Collideable) first).startCollision(second);
                }

                if (second instanceof Collideable) {
                    ((Collideable) second).startCollision(first);
                }
            }

            @Override
            public void endContact(Contact contact) {
                Object first = contact.getFixtureA().getUserData();
                Object second = contact.getFixtureB().getUserData();

                if (first instanceof Collideable) {
                    ((Collideable) first).endCollision(second);
                }

                if (second instanceof Collideable) {
                    ((Collideable) second).endCollision(first);
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
                Object first = contact.getFixtureA().getUserData();
                Object second = contact.getFixtureB().getUserData();

                if (first instanceof Collideable) {
                    contact.setEnabled(((Collideable) first).willCollide(second));
                }

                if (second instanceof Collideable) {
                    contact.setEnabled(((Collideable) second).willCollide(first));
                }
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });
    }

    /**
     * Create rect without edges
     *
     * @param edgeSize smaller size - bigger edges
     */
    public static Vector2[] rectWithoutEdges(float width, float height, int edgeSize) {
        return rectWithoutEdges(width, height, edgeSize, 0, 0);
    }

    public static Vector2[] rectWithoutEdges(float width, float height, int edgeSize, float marginLeft, float marginBottom) {
        return new Vector2[]{
                new Vector2(marginLeft + width / edgeSize,
                        marginBottom),
                new Vector2(marginLeft + width / edgeSize * (edgeSize - 1),
                        marginBottom),
                new Vector2(marginLeft + width,
                        marginBottom + height / edgeSize),
                new Vector2(marginLeft + width,
                        marginBottom + height / edgeSize * (edgeSize - 1)),
                new Vector2(marginLeft + width / edgeSize * (edgeSize - 1),
                        marginBottom + height),
                new Vector2(marginLeft + width / edgeSize,
                        marginBottom + height),
                new Vector2(marginLeft,
                        marginBottom + height / edgeSize * (edgeSize - 1)),
                new Vector2(marginLeft,
                        marginBottom + height / edgeSize),
        };
    }
}
