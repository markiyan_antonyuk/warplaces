package com.markantoni.warplaces.utils;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by mark on 15.01.16.
 */
public class Rect extends Rectangle {
    public Rect(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public Rect(float width, float height) {
        super(0, 0, width, height);
    }

    public Rect(Rect rect) {
        super(rect);
    }

    public Rect set(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Rect set(Vector2 vector2) {
        set(vector2.x, vector2.y);
        return this;
    }

    public Rect normalize() {
        if (width < 0) {
            x += width;
            width = -width;
        }

        if (height < 0) {
            y += height;
            height = -height;
        }

        return this;
    }

    public Rect setEmpty() {
        set(0, 0, 0, 0);
        return this;
    }

    public boolean isEmpty() {
        return x == 0 && y == 0 && width == 0 && height == 0;
    }

    public boolean contains(Vector2 vector2) {
        return contains(vector2.x, vector2.y);
    }

    public Rect cpy() {
        return new Rect(this);
    }

    public static Rect empty() {
        return new Rect(0, 0, 0, 0);
    }

    @Override
    public String toString() {
        return "Rect x: " + x + " y: " + y + " width: " + width + " height: " + height;
    }
}
