package com.markantoni.warplaces.utils;


/**
 * Created by mark on 30.12.15.
 */
public interface Renderable {
    void render(Renderer renderer);
}
