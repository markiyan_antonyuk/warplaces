package com.markantoni.warplaces.utils;

/**
 * Created by mark on 24.02.16.
 */
public interface RenderableSelected extends Renderable {
    void renderSelected(Renderer renderer);
}
