package com.markantoni.warplaces.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.markantoni.warplaces.game.camera.Camera;

/**
 * Created by mark on 14.01.16.
 */
public class Renderer implements Disposable {
    private static Renderer sInstance;

    private final SpriteBatch mSpriteBatch;
    private final ShapeRenderer mShapeRenderer;
    private boolean mIsBatchOpened;
    private boolean mIsShapeRendererOpened;
    private ShapeRenderer.ShapeType mOpenedShapeType;
    private Camera mCamera;

    public Renderer(SpriteBatch spriteBatch, ShapeRenderer shapeRenderer, Rect bounds) {
        sInstance = this;
        mSpriteBatch = spriteBatch;
        mShapeRenderer = shapeRenderer;
        resize(bounds);
    }

    public static Renderer debug() {
        return sInstance;
    }

    public static Vector2 unproject(float x, float y) {
        if (sInstance.mCamera == null) {
            return new Vector2(-1, -1);
        }
        return sInstance.mCamera.unproject(x, y);
    }

    public static void setCamera(Camera camera) {
        sInstance.mCamera = camera;
    }

    public boolean needToRender(Rect bounds) {
        if (mCamera == null) {
            return false;
        }
        return mCamera.getVisibleBounds().overlaps(bounds);
    }

    public void resize(Rect bounds) {
        mShapeRenderer.getProjectionMatrix().setToOrtho2D(0, 0, bounds.width, bounds.height);
        mShapeRenderer.updateMatrices();
        mSpriteBatch.getProjectionMatrix().setToOrtho2D(0, 0, bounds.width, bounds.height);
    }

    private void beginBatch() {
        if (mIsShapeRendererOpened) {
            mIsShapeRendererOpened = false;
            mShapeRenderer.end();
            mOpenedShapeType = null;
        }
        if (!mIsBatchOpened) {
            mIsBatchOpened = true;
            if (mCamera != null) {
                mSpriteBatch.setProjectionMatrix(mCamera.matrix());
            }
            mSpriteBatch.enableBlending();
            mSpriteBatch.begin();
        }
    }

    private void beginShapeRenderer(ShapeRenderer.ShapeType shapeType) {
        if (mIsBatchOpened) {
            mIsBatchOpened = false;
            mSpriteBatch.end();
        }

        if (mOpenedShapeType != shapeType) {
            mShapeRenderer.end();
            mIsShapeRendererOpened = false;
        }

        if (!mIsShapeRendererOpened) {
            mIsShapeRendererOpened = true;
            mOpenedShapeType = shapeType;
            if (mCamera != null) {
                mShapeRenderer.setProjectionMatrix(mCamera.matrix());
            }
            mShapeRenderer.begin(shapeType);
        }
    }

    public void begin() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void end() {
        if (mIsBatchOpened) {
            mSpriteBatch.end();
        }
        if (mIsShapeRendererOpened) {
            mShapeRenderer.end();
        }

        mIsBatchOpened = mIsShapeRendererOpened = false;
    }

    @Override
    public void dispose() {
        mSpriteBatch.dispose();
        mShapeRenderer.dispose();
    }

    public void renderCross(Vector2 position, float size, Color color) {
        beginShapeRenderer(ShapeRenderer.ShapeType.Filled);
        Color oldColor = mShapeRenderer.getColor();
        mShapeRenderer.setColor(color);
        mShapeRenderer.x(position, size);
        mShapeRenderer.setColor(oldColor);
    }

    public void renderTexture(Texture texture, Rect bounds) {
        beginBatch();
        mSpriteBatch.draw(texture, bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public void renderTexture(Texture texture, Rect bounds, float angle) {
        beginBatch();
        mSpriteBatch.draw(texture, bounds.x, bounds.y,
                bounds.width / 2, bounds.height / 2,
                bounds.width, bounds.height,
                1, 1, angle, 0, 0,
                texture.getWidth(), texture.getHeight(), false, false);
    }

    public void renderTextureRegion(TextureRegion region, Rect bounds) {
        beginBatch();
        mSpriteBatch.draw(region, bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public void renderTextureRegion(TextureRegion region, Rect bounds, Color color) {
        beginBatch();
        Color batchColor = mSpriteBatch.getColor();
        mSpriteBatch.setColor(color);
        mSpriteBatch.draw(region, bounds.x, bounds.y, bounds.width, bounds.height);
        mSpriteBatch.setColor(batchColor);
    }

    public void renderSprite(Sprite sprite, Rect bounds) {
        renderSprite(sprite, bounds, true);
    }

    public void renderSprite(Sprite sprite, Rect bounds, boolean zoomed) {
        beginBatch();
        if (zoomed) {
            sprite.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
        } else {
            sprite.setBounds(bounds.x, bounds.y, bounds.width * mCamera.getZoom(), bounds.height * mCamera.getZoom());
        }
        sprite.draw(mSpriteBatch);
    }

    public void renderEllipse(Color color, float x, float y, float width, float height) {
        beginShapeRenderer(ShapeRenderer.ShapeType.Filled);
        Color c = mShapeRenderer.getColor();
        mShapeRenderer.setColor(color);
        mShapeRenderer.ellipse(x, y, width, height);
        mShapeRenderer.setColor(c);
    }

    public void renderColor(Color color, Rect bounds, ShapeRenderer.ShapeType shapeType) {
        beginShapeRenderer(shapeType);
        mShapeRenderer.rect(bounds.x, bounds.y, bounds.width, bounds.height, color, color, color, color);
    }

    public void renderColor(Color color, Rect bounds) {
        renderColor(color, bounds, ShapeRenderer.ShapeType.Line);
    }

    public void renderText(BitmapFont bitmapFont, String text, Rect bounds) {
        beginBatch();
        bitmapFont.draw(mSpriteBatch, text, bounds.x, bounds.y + bounds.height / 2 + bitmapFont.getXHeight() / 2, bounds.width, Align.center, false);
    }
}
