package com.markantoni.warplaces.utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by mark on 18.01.16.
 * Abstract coordinate units for adaptive layouts
 */
public class SizeUnit {
    public static float HORIZONTAL;
    public static float VERTICAL;
    public static float HORIZONTAL_UI;
    public static float VERTICAL_UI;

    public static void init() {
        HORIZONTAL = VERTICAL = 50;
        float density = Gdx.graphics.getDensity();

        if (density > 1 && density <= 2) {
            density *= 0.6f;
        }

        if (density > 2 && density <= 3) {
            density *= 0.5;
        }
        HORIZONTAL_UI = HORIZONTAL * density;
        VERTICAL_UI = VERTICAL * density;
    }

    public static int uiX(float horizontal) {
        return (int) (horizontal * HORIZONTAL_UI);
    }

    public static int uiY(float vertical) {
        return (int) (vertical * VERTICAL_UI);
    }

    public static int uiUnit(float units) {
        return (int) ((VERTICAL_UI + HORIZONTAL_UI) / 2 * units);
    }

    public static int max() {
        return unit(100);
    }

    public static int x(float horizontalUnits) {
        return (int) (horizontalUnits * HORIZONTAL);
    }

    public static int y(float verticalUnits) {
        return (int) (verticalUnits * VERTICAL);
    }

    public static int unit(float sizeUnits) {
        return (int) ((VERTICAL + HORIZONTAL) / 2 * sizeUnits);
    }
}
