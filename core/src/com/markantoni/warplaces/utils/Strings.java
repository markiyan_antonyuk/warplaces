package com.markantoni.warplaces.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.MissingResourceException;

/**
 * Created by mark on 19.01.16.
 */
public class Strings {
    private static I18NBundle mStrings;
    private static I18NBundle mSettingStrings;
    private static I18NBundle mGameStrings;

    static {
        mStrings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"));
        mSettingStrings = I18NBundle.createBundle(Gdx.files.internal("strings/settings_strings"));
        mGameStrings = I18NBundle.createBundle(Gdx.files.internal("strings/game_strings"));
    }

    public static String get(String key) {
        try {
            return mStrings.get(key);
        } catch (Exception e) {
            //ignore
        }

        try {
            return mSettingStrings.get(key);
        } catch (Exception e) {
            //ignore
        }

        try {
            return mGameStrings.get(key);
        } catch (Exception e) {
            //ignore
        }

        throw new MissingResourceException("No resource found for key: " + key, Strings.class.getName(), key);
    }
}
