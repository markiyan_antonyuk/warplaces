package com.markantoni.warplaces.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by mark on 29.01.16.
 */
public class UiUtils {
    private static Vector2 tempVector1 = new Vector2();
    private static Vector2 tempVector2 = new Vector2();

    public static boolean contains(Actor actor, float x, float y) {
        return x > actor.getX() && x < actor.getRight()
                && y > actor.getY() && y < actor.getTop() && actor.isVisible();
    }

}
