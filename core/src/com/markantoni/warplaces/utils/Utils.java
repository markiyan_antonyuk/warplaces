package com.markantoni.warplaces.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.markantoni.warplaces.WarPlaces;

import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by mark on 21.01.16.
 */
public class Utils {
    private static final Random sRandom = new Random(System.currentTimeMillis());

    public interface DoubleLoopAction {
        void action(int x, int y);
    }

    public interface LoopAction<T> {
        void action(T t);
    }

    public static void post(Runnable runnable) {
        Gdx.app.postRunnable(runnable);
    }

    public static float randomRange(float min, float range) {
        return min + sRandom.nextInt((int) range);
    }

    public static int flipY(float y) {
        return (int) (WarPlaces.getSize().height - y);
    }

    public static boolean fingersTouched(int amount) {
        int touchedCount = 0;
        for (int i = 0; i < amount; i++) {
            if (Gdx.input.isTouched(i)) {
                touchedCount++;
            }
        }
        return touchedCount == amount;
    }

    public static void loop(int loopCount, DoubleLoopAction doubleLoopAction) {
        for (int y = 0; y < loopCount; y++) {
            for (int x = 0; x < loopCount; x++) {
                doubleLoopAction.action(x, y);
            }
        }
    }

    public static <T> void loop(List<T> list, LoopAction<T> loopAction) {
        for (T t : list) {
            loopAction.action(t);
        }
    }

    public static void saveJson(String name, String extension, JSONObject jsonObject) {
        try {
            File file = new File(WarPlaces.getGameDir(), name + extension);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(jsonObject.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            Log.d(e.toString());
        }
    }

    public static void saveTexture(String name, Texture texture) {
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }
        PixmapIO.writeCIM(new FileHandle(new File(WarPlaces.getGameDir(), name)), texture.getTextureData().consumePixmap());
    }

    public static Texture loadTexture(String name) {
        Pixmap pixmap = PixmapIO.readCIM(new FileHandle(new File(WarPlaces.getGameDir(), name)));
        return new Texture(pixmap);
    }

    public static JSONObject readJson(File file) {
        JSONObject jsonObject = null;
        try {
            Scanner scanner = new Scanner(file);
            jsonObject = new JSONObject(scanner.nextLine());
        } catch (FileNotFoundException e) {
            Log.d(e.toString());
        }
        return jsonObject;
    }

    public static File[] listGameFiles(final String filter) {
        return WarPlaces.getGameDir().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(filter);
            }
        });
    }

    public static Texture combineTextures(final Texture... textures) {
        int maxWidth = 0;
        int maxHeight = 0;
        for (Texture texture : textures) {
            if (texture.getWidth() > maxWidth) {
                maxWidth = texture.getWidth();
            }

            if (texture.getHeight() > maxHeight) {
                maxHeight = texture.getHeight();
            }

            if (!texture.getTextureData().isPrepared()) {
                texture.getTextureData().prepare();
            }
        }

        Pixmap pixmap = new Pixmap(maxWidth, maxHeight, Pixmap.Format.RGBA8888);
        for (Texture texture : textures) {
            pixmap.drawPixmap(texture.getTextureData().consumePixmap(), 0, 0, texture.getWidth(), texture.getHeight(),
                    0, 0, pixmap.getWidth(), pixmap.getHeight());
        }

        return new Texture(pixmap);
    }

}
