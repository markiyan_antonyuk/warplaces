package com.markantoni.warplaces.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.markantoni.warplaces.Settings;
import com.markantoni.warplaces.WarPlaces;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setWindowedMode(Settings.DEFAULT_WIDTH, Settings.DEFAULT_HEIGHT);
		new Lwjgl3Application(new WarPlaces(), config);
	}
}
